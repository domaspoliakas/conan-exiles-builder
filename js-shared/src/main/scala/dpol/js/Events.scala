package dpol.js

import cats.effect._
import cats.implicits._
import cats.effect.concurrent.Deferred

import scala.scalajs.js

import org.scalajs.dom.raw.Event
import org.scalajs.dom.raw.EventTarget

class Events[F[_]](implicit F: ConcurrentEffect[F]) {

  def once[A <: Event](elem: EventTarget, event: String): F[A] = {
    for {
      eventValue <- Deferred[F, A]
      listener <- F.delay[js.Function1[A, Unit]] { e =>
        F.runAsync(eventValue.complete(e))(_ => IO.unit).unsafeRunSync()
      }
      _ <- F.delay(elem.addEventListener(event, listener))
      e <- eventValue.get
    } yield e
  }

}

object Events {
  def apply[F[_]: ConcurrentEffect]: Events[F] = new Events[F]
}
