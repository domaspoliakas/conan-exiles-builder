package dpol.math

trait MatrixSubtraction[M] {
  def subtract(m1: M, m2: M): M
}
