package dpol.math

trait MatrixMultiplication[M1, M2, M3] {
  def multiply(m1: M1, m2: M2): M3
}

object MatrixMultiplication {

  // implicit val v3scalar: MatrixMultiplication[Vec3Double, Double, Vec3Double]

  implicit val v4v4mul: MatrixMultiplication[Vec4Double, Vec4Double, Double] =
    new MatrixMultiplication[Vec4Double, Vec4Double, Double] {
      override def multiply(m1: Vec4Double, m2: Vec4Double): Double =
        m1.x * m2.x + m1.y * m2.y + m1.z * m2.z + m1.w * m2.w
    }

  implicit val v4m4mul: MatrixMultiplication[Vec4Double, M4x4Double, Vec4Double] = 
    new MatrixMultiplication[Vec4Double, M4x4Double, Vec4Double] {
      override def multiply(m1: Vec4Double, m2: M4x4Double): Vec4Double = {

        val a0 = m1.x
        val a1 = m1.y
        val a2 = m1.z
        val a3 = m1.w

        val b0 = m2.x
        val b1 = m2.y
        val b2 = m2.z
        val b3 = m2.w

        Vec4Double(
          a0 * b0.x + 
          a1 * b1.x +
          a2 * b2.x + 
          a3 * b3.x,

          a0 * b0.y + 
          a1 * b1.y +
          a2 * b2.y + 
          a3 * b3.y,

          a0 * b0.z + 
          a1 * b1.z +
          a2 * b2.z + 
          a3 * b3.z,

          a0 * b0.w + 
          a1 * b1.w +
          a2 * b2.w + 
          a3 * b3.w,
        )
    }
  }

  implicit val v3m3mul: MatrixMultiplication[Vec3Double, M3x3Double, Vec3Double] = new MatrixMultiplication[Vec3Double, M3x3Double, Vec3Double] {
    override def multiply(m1: Vec3Double, m2: M3x3Double): Vec3Double = {

      val a0 = m1.x
      val a1 = m1.y
      val a2 = m1.z

      val b0 = m2.x
      val b1 = m2.y
      val b2 = m2.z

      Vec3Double(
        a0 * b0.x +
        a1 * b1.x + 
        a2 * b2.x,

        a0 * b0.y +
        a1 * b1.y + 
        a2 * b2.y,

        a0 * b0.z +
        a1 * b1.z + 
        a2 * b2.z
      )

    }
  }

  implicit val m3m3mul: MatrixMultiplication[M3x3Double, M3x3Double, M3x3Double] = new MatrixMultiplication[M3x3Double, M3x3Double, M3x3Double] {
    override def multiply(m1: M3x3Double, m2: M3x3Double): M3x3Double = {

      val a00 = m1.x.x
      val a01 = m1.x.y
      val a02 = m1.x.z
      val a10 = m1.y.x
      val a11 = m1.y.y
      val a12 = m1.y.z
      val a20 = m1.z.x
      val a21 = m1.z.y
      val a22 = m1.z.z

      val b00 = m2.x.x
      val b01 = m2.x.y
      val b02 = m2.x.z
      val b10 = m2.y.x
      val b11 = m2.y.y
      val b12 = m2.y.z
      val b20 = m2.z.x
      val b21 = m2.z.y
      val b22 = m2.z.z

      M3x3Double(
        Vec3Double(
          a00 * b00 + a01 * b10 + a02 * b20,
          a10 * b00 + a11 * b10 + a12 * b20,
          a20 * b00 + a21 * b10 + a22 * b20
        ),
        Vec3Double(
          a00 * b01 + a01 * b11 + a02 * b21,
          a10 * b01 + a11 * b11 + a12 * b21,
          a20 * b01 + a21 * b11 + a22 * b21
        ),
        Vec3Double(
          a00 * b02 + a01 * b12 + a02 * b22,
          a10 * b02 + a11 * b12 + a12 * b22,
          a20 * b02 + a21 * b12 + a22 * b22
        )
      )


    }
  }

  implicit val m4m4mul: MatrixMultiplication[M4x4Double, M4x4Double, M4x4Double] =
    new MatrixMultiplication[M4x4Double, M4x4Double, M4x4Double] {
      override def multiply(m1: M4x4Double, m2: M4x4Double): M4x4Double = {

        val a00 = m1.x.x
        val a01 = m1.x.y
        val a02 = m1.x.z
        val a03 = m1.x.w
        val a10 = m1.y.x
        val a11 = m1.y.y
        val a12 = m1.y.z
        val a13 = m1.y.w
        val a20 = m1.z.x
        val a21 = m1.z.y
        val a22 = m1.z.z
        val a23 = m1.z.w
        val a30 = m1.z.x
        val a31 = m1.z.y
        val a32 = m1.z.z
        val a33 = m1.z.w

        val b00 = m2.x.x
        val b01 = m2.x.y
        val b02 = m2.x.z
        val b03 = m2.x.w
        val b10 = m2.y.x
        val b11 = m2.y.y
        val b12 = m2.y.z
        val b13 = m2.y.w
        val b20 = m2.z.x
        val b21 = m2.z.y
        val b22 = m2.z.z
        val b23 = m2.z.w
        val b30 = m2.z.x
        val b31 = m2.z.y
        val b32 = m2.z.z
        val b33 = m2.z.w

        val v4x = Vec4Double(
          b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30,
          b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31,
          b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32,
          b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33
        )

        val v4y = Vec4Double(
          b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30,
          b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31,
          b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32,
          b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33
        )

        val v4z = Vec4Double(
          b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30,
          b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31,
          b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32,
          b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33
        )

        val v4w = Vec4Double(
          b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30,
          b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31,
          b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32,
          b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33
        )

        M4x4Double(
          v4x,
          v4y,
          v4z,
          v4w
        )
      }
    }
}