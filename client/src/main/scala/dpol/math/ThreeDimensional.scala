package dpol.math

case class Vec3Double(x: Double, y: Double, z: Double) {
  def homogenous: Vec4Double = Vec4Double(x, y, z, 1)
  def magnitude: Double      = Math.sqrt(x * x + y * y + z * z)
  def normalised: Option[Vec3Double] = {
    val mag = magnitude
    if (mag > 0) {
      val magMul = 1 / magnitude
      Some(
        Vec3Double(
          x * magMul,
          y * magMul,
          z * magMul
        )
      )
    } else None
  }
  def *(num: Double): Vec3Double = copy(x = x * num, y * num, z * num)

  def dotProduct(v2: Vec3Double): Double = x * v2.x + y * v2.y + z * v2.z

  // def +(num: Vec3): Vec3Double = copy(x = x * num, y * num, z * num)
}

object Vec3Double {
  implicit val sub: MatrixSubtraction[Vec3Double] = new MatrixSubtraction[Vec3Double] {
    override def subtract(m1: Vec3Double, m2: Vec3Double): Vec3Double = 
      Vec3Double(m1.x - m2.x, m1.y - m2.y, m1.z - m2.z)
  }
}

case class M3x3Double(x: Vec3Double, y: Vec3Double, z: Vec3Double)

object M3x3Double {

  def identity: M3x3Double = M3x3Double(
    Vec3Double(1, 0, 0),
    Vec3Double(0, 1, 0),
    Vec3Double(0, 0, 1)
  )

  object RowMajor {

    def identity: M3x3Double = M3x3Double.identity

    def xRotation(rx: Double): M3x3Double = {
      val c = Math.cos(rx)
      val s = Math.sin(rx)
      M3x3Double(
        Vec3Double(1, 0, 0),
        Vec3Double(0, c, -s),
        Vec3Double(0, s, c)
      )
    }

    def yRotation(ry: Double): M3x3Double = {
      val c = Math.cos(ry)
      val s = Math.sin(ry)
      M3x3Double(
        Vec3Double(c, 0, s),
        Vec3Double(0, 1, 0),
        Vec3Double(-s, 0, c)
      )
    }

    def zRotation(rz: Double): M3x3Double = {
      val c = Math.cos(rz)
      val s = Math.sin(rz)
      M3x3Double(
        Vec3Double(c, -s, 0),
        Vec3Double(s, c, 0),
        Vec3Double(0, 0, 1)
      )
    }

    def scaling(sx: Double, sy: Double, sz: Double): M3x3Double = {
      M3x3Double(
        Vec3Double(sx, 0, 0),
        Vec3Double(0, sy, 0),
        Vec3Double(0, 0, sz)
      )
    }
  }

}
