package dpol.math

import scala.scalajs.js
import scala.annotation.tailrec

/**
  * Matrix math!
  */
object Matrix {

  def multiply[M1, M2, M3](m1: M1, m2: M2)(implicit mul: MatrixMultiplication[M1, M2, M3]): M3 =
    mul.multiply(m1, m2)

  @tailrec
  def multiply[M](m1: M, m2: M*)(implicit mul: MatrixMultiplication[M, M, M]): M =
    if (m2.isEmpty)
      m1
    else
      multiply[M](multiply[M, M, M](m1, m2.head), m2.tail:_*)

  def add[M](m1: M, m2: M)(implicit sum: MatrixSummation[M]): M = 
    sum.add(m1, m2)
  
  def subtract[M](m1: M, m2: M)(implicit subtraction: MatrixSubtraction[M]): M = subtraction.subtract(m1, m2)

  implicit class MatrixOps(a: js.Array[Double]) {
    def translate(tx: Double, ty: Double, tz: Double): js.Array[Double] =
      multiply(a, translation(tx, ty, tz))

    def xRotate(angleInRadians: Double): js.Array[Double] =
      multiply(a, xRotation(angleInRadians))

    def yRotate(angleInRadians: Double): js.Array[Double] =
      multiply(a, yRotation(angleInRadians))

    def zRotate(angleInRadians: Double): js.Array[Double] =
      multiply(a, zRotation(angleInRadians))

    def scale(sx: Double, sy: Double, sz: Double): js.Array[Double] =
      multiply(a, scaling(sx, sy, sz))

    def inverse: js.Array[Double] =
      Matrix.inverse(a)

  }
  def identity: js.Array[Double] = {
    js.Array(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)
  }

  def perspective(fov: Double, aspect: Double, zNear: Double, zFar: Double): js.Array[Double] = {

    var f        = Math.tan(Math.PI * 0.5 - 0.5 * fov);
    var rangeInv = 1.0 / (zNear - zFar);

    js.Array(
      f / aspect,
      0,
      0,
      0,
      0,
      f,
      0,
      0,
      0,
      0,
      (zNear + zFar) * rangeInv,
      -1,
      0,
      0,
      zNear * zFar * rangeInv * 2,
      0
    )
  }

  def inverse(input: js.Array[Double]): js.Array[Double] = {

    val m00    = input(0 * 4 + 0)
    val m01    = input(0 * 4 + 1)
    val m02    = input(0 * 4 + 2)
    val m03    = input(0 * 4 + 3)
    val m10    = input(1 * 4 + 0)
    val m11    = input(1 * 4 + 1)
    val m12    = input(1 * 4 + 2)
    val m13    = input(1 * 4 + 3)
    val m20    = input(2 * 4 + 0)
    val m21    = input(2 * 4 + 1)
    val m22    = input(2 * 4 + 2)
    val m23    = input(2 * 4 + 3)
    val m30    = input(3 * 4 + 0)
    val m31    = input(3 * 4 + 1)
    val m32    = input(3 * 4 + 2)
    val m33    = input(3 * 4 + 3)
    val tmp_0  = m22 * m33;
    val tmp_1  = m32 * m23;
    val tmp_2  = m12 * m33;
    val tmp_3  = m32 * m13;
    val tmp_4  = m12 * m23;
    val tmp_5  = m22 * m13;
    val tmp_6  = m02 * m33;
    val tmp_7  = m32 * m03;
    val tmp_8  = m02 * m23;
    val tmp_9  = m22 * m03;
    val tmp_10 = m02 * m13;
    val tmp_11 = m12 * m03;
    val tmp_12 = m20 * m31;
    val tmp_13 = m30 * m21;
    val tmp_14 = m10 * m31;
    val tmp_15 = m30 * m11;
    val tmp_16 = m10 * m21;
    val tmp_17 = m20 * m11;
    val tmp_18 = m00 * m31;
    val tmp_19 = m30 * m01;
    val tmp_20 = m00 * m21;
    val tmp_21 = m20 * m01;
    val tmp_22 = m00 * m11;
    val tmp_23 = m10 * m01;

    val t0 = (tmp_0 * m11 + tmp_3 * m21 + tmp_4 * m31) -
      (tmp_1 * m11 + tmp_2 * m21 + tmp_5 * m31);
    val t1 = (tmp_1 * m01 + tmp_6 * m21 + tmp_9 * m31) -
      (tmp_0 * m01 + tmp_7 * m21 + tmp_8 * m31);
    val t2 = (tmp_2 * m01 + tmp_7 * m11 + tmp_10 * m31) -
      (tmp_3 * m01 + tmp_6 * m11 + tmp_11 * m31);
    val t3 = (tmp_5 * m01 + tmp_8 * m11 + tmp_11 * m21) -
      (tmp_4 * m01 + tmp_9 * m11 + tmp_10 * m21);

    var d = 1.0 / (m00 * t0 + m10 * t1 + m20 * t2 + m30 * t3);

    js.Array(
      d * t0,
      d * t1,
      d * t2,
      d * t3,
      d * ((tmp_1 * m10 + tmp_2 * m20 + tmp_5 * m30) -
        (tmp_0 * m10 + tmp_3 * m20 + tmp_4 * m30)),
      d * ((tmp_0 * m00 + tmp_7 * m20 + tmp_8 * m30) -
        (tmp_1 * m00 + tmp_6 * m20 + tmp_9 * m30)),
      d * ((tmp_3 * m00 + tmp_6 * m10 + tmp_11 * m30) -
        (tmp_2 * m00 + tmp_7 * m10 + tmp_10 * m30)),
      d * ((tmp_4 * m00 + tmp_9 * m10 + tmp_10 * m20) -
        (tmp_5 * m00 + tmp_8 * m10 + tmp_11 * m20)),
      d * ((tmp_12 * m13 + tmp_15 * m23 + tmp_16 * m33) -
        (tmp_13 * m13 + tmp_14 * m23 + tmp_17 * m33)),
      d * ((tmp_13 * m03 + tmp_18 * m23 + tmp_21 * m33) -
        (tmp_12 * m03 + tmp_19 * m23 + tmp_20 * m33)),
      d * ((tmp_14 * m03 + tmp_19 * m13 + tmp_22 * m33) -
        (tmp_15 * m03 + tmp_18 * m13 + tmp_23 * m33)),
      d * ((tmp_17 * m03 + tmp_20 * m13 + tmp_23 * m23) -
        (tmp_16 * m03 + tmp_21 * m13 + tmp_22 * m23)),
      d * ((tmp_14 * m22 + tmp_17 * m32 + tmp_13 * m12) -
        (tmp_16 * m32 + tmp_12 * m12 + tmp_15 * m22)),
      d * ((tmp_20 * m32 + tmp_12 * m02 + tmp_19 * m22) -
        (tmp_18 * m22 + tmp_21 * m32 + tmp_13 * m02)),
      d * ((tmp_18 * m12 + tmp_23 * m32 + tmp_15 * m02) -
        (tmp_22 * m32 + tmp_14 * m02 + tmp_19 * m12)),
      d * ((tmp_22 * m22 + tmp_16 * m02 + tmp_21 * m12) -
        (tmp_20 * m12 + tmp_23 * m22 + tmp_17 * m02))
    )
  }

  def projection(width: Double, height: Double, depth: Double): js.Array[Double] = {
    // Note: This matrix flips the Y axis so 0 is at the top.
    js.Array(
      2 / width,
      0,
      0,
      0,
      0,
      -2 / height,
      0,
      0,
      0,
      0,
      2 / depth,
      0,
      -1,
      1,
      0,
      1
    )
  }

  def translation(tx: Double, ty: Double, tz: Double): js.Array[Double] = {
    js.Array(
      1,
      0,
      0,
      0,
      0,
      1,
      0,
      0,
      0,
      0,
      1,
      0,
      tx,
      ty,
      tz,
      1
    )
  }

  def xRotation(angleInRadians: Double): js.Array[Double] = {
    val c = Math.cos(angleInRadians);
    val s = Math.sin(angleInRadians);

    js.Array(
      1,
      0,
      0,
      0,
      0,
      c,
      s,
      0,
      0,
      -s,
      c,
      0,
      0,
      0,
      0,
      1
    )
  }

  def yRotation(angleInRadians: Double): js.Array[Double] = {
    val c = Math.cos(angleInRadians);
    val s = Math.sin(angleInRadians);

    js.Array(
      c,
      0,
      -s,
      0,
      0,
      1,
      0,
      0,
      s,
      0,
      c,
      0,
      0,
      0,
      0,
      1
    )
  }

  def zRotation(angleInRadians: Double): js.Array[Double] = {
    val c = Math.cos(angleInRadians);
    val s = Math.sin(angleInRadians);

    js.Array(
      c,
      s,
      0,
      0,
      -s,
      c,
      0,
      0,
      0,
      0,
      1,
      0,
      0,
      0,
      0,
      1
    )
  }

  def scaling(sx: Double, sy: Double, sz: Double): js.Array[Double] = {
    js.Array(
      sx,
      0,
      0,
      0,
      0,
      sy,
      0,
      0,
      0,
      0,
      sz,
      0,
      0,
      0,
      0,
      1
    )
  }

  def multiply(a: js.Array[Double], b: js.Array[Double]): js.Array[Double] = {
    val b00 = b(0 * 4 + 0)
    val b01 = b(0 * 4 + 1)
    val b02 = b(0 * 4 + 2)
    val b03 = b(0 * 4 + 3)
    val b10 = b(1 * 4 + 0)
    val b11 = b(1 * 4 + 1)
    val b12 = b(1 * 4 + 2)
    val b13 = b(1 * 4 + 3)
    val b20 = b(2 * 4 + 0)
    val b21 = b(2 * 4 + 1)
    val b22 = b(2 * 4 + 2)
    val b23 = b(2 * 4 + 3)
    val b30 = b(3 * 4 + 0)
    val b31 = b(3 * 4 + 1)
    val b32 = b(3 * 4 + 2)
    val b33 = b(3 * 4 + 3)
    val a00 = a(0 * 4 + 0)
    val a01 = a(0 * 4 + 1)
    val a02 = a(0 * 4 + 2)
    val a03 = a(0 * 4 + 3)
    val a10 = a(1 * 4 + 0)
    val a11 = a(1 * 4 + 1)
    val a12 = a(1 * 4 + 2)
    val a13 = a(1 * 4 + 3)
    val a20 = a(2 * 4 + 0)
    val a21 = a(2 * 4 + 1)
    val a22 = a(2 * 4 + 2)
    val a23 = a(2 * 4 + 3)
    val a30 = a(3 * 4 + 0)
    val a31 = a(3 * 4 + 1)
    val a32 = a(3 * 4 + 2)
    val a33 = a(3 * 4 + 3)
    val dst = new js.Array[Double](16)
    dst(0) = b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30
    dst(1) = b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31
    dst(2) = b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32
    dst(3) = b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33
    dst(4) = b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30
    dst(5) = b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31
    dst(6) = b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32
    dst(7) = b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33
    dst(8) = b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30
    dst(9) = b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31
    dst(10) = b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32
    dst(11) = b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33
    dst(12) = b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30
    dst(13) = b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31
    dst(14) = b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32
    dst(15) = b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33
    dst
  }

}
