package dpol.math

object Vector {

  type Vec3[A] = (A, A, A)

  implicit val Vec3Ops: VectorOps[Vec3] = new VectorOps[Vec3] {
    def norm(v: Vec3[Double]): Double = Math.sqrt(v._1 * v._1 + v._2 * v._2 + v._3 * v._3)
    def +[A](v1: Vec3[A], v2: Vec3[A])(implicit n: Numeric[A]): Vec3[A] = (n.plus(v1._1, v2._1), n.plus(v1._2, v2._2), n.plus(v1._3, v2._3))
  }

}

trait VectorOps[V[_]] {
  def norm(v: V[Double]): Double 
  def magnitude(v: V[Double]): Double = norm(v)
  def +[A](v1: V[A], v2: V[A])(implicit n: Numeric[A]): V[A]
}

object VectorOps {
  implicit class VectorDoubleOps[V[_]](v: V[Double]) {
    def norm(implicit ops: VectorOps[V]): Double = ops.norm(v)
  }

  implicit class VectorNumericOps[V[_], A](v: V[A]) {
    def +(v2: V[A])(implicit n: Numeric[A], vv: VectorOps[V]) = vv.+(v, v2)
  }

}