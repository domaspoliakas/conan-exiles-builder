package dpol.math

case class Vec4Double(x: Double, y: Double, z: Double, w: Double)
case class M4x4Double(x: Vec4Double, y: Vec4Double, z: Vec4Double, w: Vec4Double)

object M4x4Double {

  object ColumnMajor {
    def identity: M4x4Double                 = M4x4Double.identity
    def transpose(m: M4x4Double): M4x4Double = M4x4Double.transpose(m)

    def translation(tx: Double, ty: Double, tz: Double): M4x4Double =
      M4x4Double(
        Vec4Double(1, 0, 0, 0),
        Vec4Double(0, 1, 0, 0),
        Vec4Double(0, 0, 1, 0),
        Vec4Double(tx, ty, tz, 1)
      )

    def xRotation(rx: Double): M4x4Double = {
      val c = Math.cos(rx)
      val s = Math.sin(rx)
      M4x4Double(
        Vec4Double(1, 0, 0, 0),
        Vec4Double(0, c, s, 0),
        Vec4Double(0, -s, c, 0),
        Vec4Double(0, 0, 0, 1)
      )
    }

    def yRotation(ry: Double): M4x4Double = {
      val c = Math.cos(ry)
      val s = Math.sin(ry)
      M4x4Double(
        Vec4Double(c, 0, -s, 0),
        Vec4Double(0, 1, 0, 0),
        Vec4Double(s, 0, c, 0),
        Vec4Double(0, 0, 0, 1)
      )
    }

    def zRotation(rz: Double): M4x4Double = {
      val c = Math.cos(rz)
      val s = Math.sin(rz)
      M4x4Double(
        Vec4Double(c, s, 0, 0),
        Vec4Double(-s, c, 0, 0),
        Vec4Double(0, 0, 1, 0),
        Vec4Double(0, 0, 0, 1)
      )
    }
  }

  object RowMajor {
    def identity: M4x4Double                 = M4x4Double.identity
    def transpose(m: M4x4Double): M4x4Double = M4x4Double.transpose(m)
    def translation(tx: Double, ty: Double, tz: Double): M4x4Double = {
      M4x4Double(
        Vec4Double(1, 0, 0, tx),
        Vec4Double(0, 1, 0, ty),
        Vec4Double(0, 0, 1, tz),
        Vec4Double(0, 0, 0, 1)
      )
    }

    def xRotation(rx: Double): M4x4Double = {
      val c = Math.cos(rx)
      val s = Math.sin(rx)
      M4x4Double(
        Vec4Double(1, 0, 0, 0),
        Vec4Double(0, c, -s, 0),
        Vec4Double(0, s, c, 0),
        Vec4Double(0, 0, 0, 1)
      )
    }

    def yRotation(ry: Double): M4x4Double = {
      val c = Math.cos(ry)
      val s = Math.sin(ry)
      M4x4Double(
        Vec4Double(c, 0, s, 0),
        Vec4Double(0, 1, 0, 0),
        Vec4Double(-s, 0, c, 0),
        Vec4Double(0, 0, 0, 1)
      )
    }

    def zRotation(rz: Double): M4x4Double = {
      val c = Math.cos(rz)
      val s = Math.sin(rz)
      M4x4Double(
        Vec4Double(c, -s, 0, 0),
        Vec4Double(s, c, 0, 0),
        Vec4Double(0, 0, 1, 0),
        Vec4Double(0, 0, 0, 1)
      )
    }
  }

  def identity: M4x4Double = M4x4Double(
    Vec4Double(1, 0, 0, 0),
    Vec4Double(0, 1, 0, 0),
    Vec4Double(0, 0, 1, 0),
    Vec4Double(0, 0, 0, 1)
  )

  def transpose(m: M4x4Double): M4x4Double = {

    val r1 = m.x
    val r2 = m.y
    val r3 = m.z
    val r4 = m.w

    M4x4Double(
      Vec4Double(r1.x, r2.x, r3.x, r4.x),
      Vec4Double(r1.y, r2.y, r3.y, r4.y),
      Vec4Double(r1.z, r2.z, r3.z, r4.z),
      Vec4Double(r1.w, r2.w, r3.w, r4.w)
    )
  }

}