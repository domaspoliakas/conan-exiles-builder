package dpol.math

trait MatrixSummation[M] {
  def add(m1: M, m2: M): M
}

object MatrixSummation {

  implicit val v3sum = new MatrixSummation[Vec3Double] {
    override def add(m1: Vec3Double, m2: Vec3Double): Vec3Double = 
      Vec3Double(m1.x + m2.x, m1.y + m2.y, m1.z + m2.z)
  }
}