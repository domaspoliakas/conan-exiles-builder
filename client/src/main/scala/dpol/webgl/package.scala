package dpol

package object webgl {
    type MonadErrorT[F[_]] = cats.MonadError[F, Throwable]
}
