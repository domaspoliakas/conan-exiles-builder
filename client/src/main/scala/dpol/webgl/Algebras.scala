package dpol.webgl

import scalajs.js
import org.scalajs.dom.raw.WebGLBuffer
import org.scalajs.dom.raw.WebGLShader
import org.scalajs.dom.raw.WebGLProgram
import cats.effect.Sync
import org.scalajs.dom.raw.WebGLRenderingContext
import org.scalajs.dom.raw.WebGLUniformLocation
import org.scalajs.dom.raw.HTMLCanvasElement
import org.scalajs.dom.raw.WebGLContextAttributes
import org.scalajs.dom.raw.WebGLFramebuffer
import org.scalajs.dom.raw.WebGLRenderbuffer
import org.scalajs.dom.raw.WebGLTexture
import scala.scalajs.js.typedarray.ArrayBufferView
import scala.scalajs.js.typedarray.ArrayBuffer
import org.scalajs.dom.raw.WebGLActiveInfo
import org.scalajs.dom.raw.WebGLShaderPrecisionFormat
import org.scalajs.dom.raw.ImageData
import org.scalajs.dom.raw.HTMLImageElement
import org.scalajs.dom.raw.HTMLVideoElement
import scala.scalajs.js.typedarray.Float32Array
import scala.scalajs.js.typedarray.Int32Array

/**
 * A WebGL context algebra. Defines all the WebGL operations, but the effects of the operations are wrapped in F
 */
trait WebGLContextAlgebra[F[_]] {

  /**
   * The canvas object this WebGLRenderingContext is associated with.
   */
  def canvas: F[HTMLCanvasElement]

  /**
   * The actual width of the drawing buffer.
   * This may be different than the underlying HTMLCanvasElement width.
   */
  def drawingBufferWidth: F[Int]

  /**
   * The actual height of the drawing buffer.
   * This may be different than the underlying HTMLCanvasElement height.
   */
  def drawingBufferHeight: F[Int]

  /**
   * Returns `null` if [[isContextLost]] would return `false`, otherwise returns a copy of the context parameters.
   */
  def getContextAttributes(): F[WebGLContextAttributes]

  /**
   * Returns `true` if the context has been lost,  `false` otherwise.
   */
  def isContextLost(): F[Boolean]

  /**
   * Returns an array of strings naming supported WebGL extensions.
   */
  def getSupportedExtensions(): F[js.Array[String]]

  /**
   * Returns an object for the named extension, or `null` if no such extension exists.
   *
   * @param name  the name of the extension
   */
  def getExtension(name: String): F[js.Any]

  /**
   * Selects the active texture unit.
   *
   * @param texture  an integer specifying the texture unit to make active. Must be in 0 .. MAX_COMBINED_TEXTURE_IMAGE_UNITS-1
   */
  def activeTexture(texture: Int): F[Unit]

  /**
   * Attaches a shader (fragment or vertex) to a [[WebGLProgram]].
   */
  def attachShader(program: WebGLProgram, shader: WebGLShader): F[Unit]

  /**
   * Associates a vertex attribute index with a named attribute variable.
   */
  def bindAttribLocation(program: WebGLProgram, index: Int, name: String): F[Unit]

  /**
   * Loads a a target into a [[WebGLBuffer]].
   *
   * @param target the target to bind the buffer to.  May be [[WebGLRenderingContext.ARRAY_BUFFER]] or [[WebGLRenderingContext.ELEMENT_ARRAY_BUFFER]]
   */
  def bindBuffer(target: Int, buffer: WebGLBuffer): F[Unit]

  /**
   * Loads a a target into a [[WebGLFramebuffer]].
   *
   * @param target  the target to bind the framebuffer to.  Must be [[WebGLRenderingContext.FRAMEBUFFER]].
   * @param framebuffer  a framebuffer object, or null to bind the default framebuffer.
   */
  def bindFramebuffer(target: Int, framebuffer: WebGLFramebuffer): F[Unit]

  /**
   * Loads a a target into a [[WebGLRenderbuffer]].
   *
   * @param target  target to bind to, must be [[WebGLRenderingContext.RENDERBUFFER]]
   * @param renderbuffer the renderbuffer to bind.  If `null`, any object bound to `target` us unbound.
   */
  def bindRenderbuffer(target: Int, renderbuffer: WebGLRenderbuffer): F[Unit]

  /**
   * Loads a the active texture unit into a [[WebGLTexture]].
   *
   * @param target  the target to bind to.  Must be [[WebGLRenderingContext.TEXTURE_2D]] or [[WebGLRenderingContext.TEXTURE_CUBE_MAP]]
   * @param texture  the texture to bind.
   */
  def bindTexture(target: Int, texture: WebGLTexture): F[Unit]

  /**
   * Sets the blend color used in [[WebGLRenderingContext.BLEND_COLOR]].
   */
  def blendColor(red: Double, green: Double, blue: Double, alpha: Double): F[Unit]

  /**
   * Specifies the equation used for RGB and Alpha blending.
   *
   * @param mode  blend equation to use.  Can be one of [[WebGLRenderingContext.FUNC_ADD]], [[WebGLRenderingContext.FUNC_SUBTRACT]], or [[WebGLRenderingContext.FUNC_REVERSE_SUBTRACT]]
   */
  def blendEquation(mode: Int): F[Unit]

  /**
   * Specifies the equation used for RGB and Alpha blending separately.
   *
   * @param modeRGB  blend equation to use for RGB components.  Can be one of [[WebGLRenderingContext.FUNC_ADD]], [[WebGLRenderingContext.FUNC_SUBTRACT]], or [[WebGLRenderingContext.FUNC_REVERSE_SUBTRACT]]
   * @param modeAlpha  blend equation to use for alpha components.  Can be one of [[WebGLRenderingContext.FUNC_ADD]], [[WebGLRenderingContext.FUNC_SUBTRACT]], or [[WebGLRenderingContext.FUNC_REVERSE_SUBTRACT]]
   */
  def blendEquationSeparate(modeRGB: Int, modeAlpha: Int): F[Unit]

  /**
   * Specifies how the blending factors are computed for source and destination pixels.
   *
   * @param sfactor  The source blending factors. May be one of [[WebGLRenderingContext.ZERO]], [[WebGLRenderingContext.ONE]], [[WebGLRenderingContext.SRC_COLOR]],
   *                 [[WebGLRenderingContext.ONE_MINUS_SRC_COLOR]], [[WebGLRenderingContext.DST_COLOR]], [[WebGLRenderingContext.ONE_MINUS_DST_COLOR]], [[WebGLRenderingContext.SRC_ALPHA]],
   *                 [[WebGLRenderingContext.ONE_MINUS_SRC_ALPHA]], [[WebGLRenderingContext.DST_ALPHA]], [[WebGLRenderingContext.ONE_MINUS_DST_ALPHA]], [[WebGLRenderingContext.CONSTANT_COLOR]],
   *                 [[WebGLRenderingContext.ONE_MINUS_CONSTANT_COLOR]], [[WebGLRenderingContext.CONSTANT_ALPHA]], [[WebGLRenderingContext.ONE_MINUS_CONSTANT_ALPHA]],
   *                 or [[WebGLRenderingContext.SRC_ALPHA_SATURATE]]. Initially this value is [[WebGLRenderingContext.ONE]].
   * @param dfactor  The destination blending factors. May be one of [[WebGLRenderingContext.ZERO]], [[WebGLRenderingContext.ONE]], [[WebGLRenderingContext.SRC_COLOR]],
   *                  [[WebGLRenderingContext.ONE_MINUS_SRC_COLOR]], [[WebGLRenderingContext.DST_COLOR]], [[WebGLRenderingContext.ONE_MINUS_DST_COLOR]], [[WebGLRenderingContext.SRC_ALPHA]],
   *                 [[WebGLRenderingContext.ONE_MINUS_SRC_ALPHA]], [[WebGLRenderingContext.DST_ALPHA]], [[WebGLRenderingContext.ONE_MINUS_DST_ALPHA]], [[WebGLRenderingContext.CONSTANT_COLOR]],
   *                 ` ONE_MINUS_CONSTANT_COLOR`, [[WebGLRenderingContext.CONSTANT_ALPHA]], or [[WebGLRenderingContext.ONE_MINUS_CONSTANT_ALPHA]].
   *                 This value is initially [[WebGLRenderingContext.ZERO]].
   */
  def blendFunc(sfactor: Int, dfactor: Int): F[Unit]

  /**
   * Specifies how the blending factors are computed for source and destination pixels, separately for alpha and RGB.
   *
   * @param srcRGB  The source blending factor for RGB. May be one of [[WebGLRenderingContext.ZERO]], [[WebGLRenderingContext.ONE]], [[WebGLRenderingContext.SRC_COLOR]],
   *                 [[WebGLRenderingContext.ONE_MINUS_SRC_COLOR]], [[WebGLRenderingContext.DST_COLOR]], [[WebGLRenderingContext.ONE_MINUS_DST_COLOR]], [[WebGLRenderingContext.SRC_ALPHA]],
   *                 [[WebGLRenderingContext.ONE_MINUS_SRC_ALPHA]], [[WebGLRenderingContext.DST_ALPHA]], [[WebGLRenderingContext.ONE_MINUS_DST_ALPHA]], [[WebGLRenderingContext.CONSTANT_COLOR]],
   *                 [[WebGLRenderingContext.ONE_MINUS_CONSTANT_COLOR]], [[WebGLRenderingContext.CONSTANT_ALPHA]], [[WebGLRenderingContext.ONE_MINUS_CONSTANT_ALPHA]],
   *                 or [[WebGLRenderingContext.SRC_ALPHA_SATURATE]]. Initially this value is [[WebGLRenderingContext.ONE]].
   * @param dstRGB  The destination blending factor for RGB. May be one of [[WebGLRenderingContext.ZERO]], [[WebGLRenderingContext.ONE]], [[WebGLRenderingContext.SRC_COLOR]],
   *                  [[WebGLRenderingContext.ONE_MINUS_SRC_COLOR]], [[WebGLRenderingContext.DST_COLOR]], [[WebGLRenderingContext.ONE_MINUS_DST_COLOR]], [[WebGLRenderingContext.SRC_ALPHA]],
   *                 [[WebGLRenderingContext.ONE_MINUS_SRC_ALPHA]], [[WebGLRenderingContext.DST_ALPHA]], [[WebGLRenderingContext.ONE_MINUS_DST_ALPHA]], [[WebGLRenderingContext.CONSTANT_COLOR]],
   *                 ` ONE_MINUS_CONSTANT_COLOR`, [[WebGLRenderingContext.CONSTANT_ALPHA]], or [[WebGLRenderingContext.ONE_MINUS_CONSTANT_ALPHA]].
   *                 This value is initially [[WebGLRenderingContext.ZERO]].
   * @param srcAlpha The source blending factor for Alpha.  Accepted values are the same as srcRGB.
   *                 The initial value is [[WebGLRenderingContext.ONE]].
   * @param dstAlpha The destination blending factor for Alpha.  Accepted values are the same as srcRGB.
   *                 The initial value is [[WebGLRenderingContext.ZERO]].
   */
  def blendFuncSeparate(srcRGB: Int, dstRGB: Int, srcAlpha: Int, dstAlpha: Int): F[Unit]

  /**
   * Sets the size of the bound [[WebGLBuffer]] for the given `target`.  The contents of the buffer are cleared to 0.
   * @param target  The target to resize.  May be [[WebGLRenderingContext.ARRAY_BUFFER]] or [[WebGLRenderingContext.ELEMENT_ARRAY_BUFFER]].
   * @param size  The size of the new buffer
   * @param usage  The specified usage for this buffer.  May be [[WebGLRenderingContext.STREAM_DRAW]], [[WebGLRenderingContext.STATIC_DRAW]] or [[WebGLRenderingContext.DYNAMIC_DRAW]].
   */
  def bufferData(target: Int, size: Int, usage: Int): F[Unit]

  /**
   * Resizes the bound [[WebGLBuffer]] for the given `target` to the size of the passed buffer, and replaces its contents with the contents of the buffer.
   * @param target  The target to resize.  May be [[WebGLRenderingContext.ARRAY_BUFFER]] or [[WebGLRenderingContext.ELEMENT_ARRAY_BUFFER]].
   * @param data the source data for the new buffer.
   * @param usage  The specified usage for this buffer.  May be [[WebGLRenderingContext.STREAM_DRAW]], [[WebGLRenderingContext.STATIC_DRAW]] or [[WebGLRenderingContext.DYNAMIC_DRAW]].
   */
  def bufferData(target: Int, data: ArrayBufferView, usage: Int): F[Unit]

  /**
   * Resizes the bound [[WebGLBuffer]] for the given `target` to the size of the passed buffer, and replaces its contents with the contents of the buffer.
   *
   * @param target  The target to resize.  May be [[WebGLRenderingContext.ARRAY_BUFFER]] or [[WebGLRenderingContext.ELEMENT_ARRAY_BUFFER]].
   * @param data the source data for the new buffer.
   * @param usage  The specified usage for this buffer.  May be [[WebGLRenderingContext.STREAM_DRAW]], [[WebGLRenderingContext.STATIC_DRAW]] or [[WebGLRenderingContext.DYNAMIC_DRAW]].
   */
  def bufferData(target: Int, data: ArrayBuffer, usage: Int): F[Unit]

  def bufferSubData(target: Int, offset: Int, data: ArrayBufferView): F[Unit]

  def bufferSubData(target: Int, offset: Int, data: ArrayBuffer): F[Unit]

  /**
   * Returns the completeness status for the framebuffer.
   *
   * The possible results are:
   *
   *  - [[WebGLRenderingContext.FRAMEBUFFER_COMPLETE]] - the framebuffer is complete.
   *  - [[WebGLRenderingContext.FRAMEBUFFER_INCOMPLETE_ATTACHMENT]] - one or more attachment points are not complete in the framebuffer.
   *  - [[WebGLRenderingContext.FRAMEBUFFER_INCOMPLETE_DIMENSIONS]] - one or more attached images do not have a specified width and height.
   *  - [[WebGLRenderingContext.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT]] - there are no images attached to the framebuffer.
   *  - [[WebGLRenderingContext.FRAMEBUFFER_UNSUPPORTED]] - the attached image format combinations are not supported on this platform.
   *
   * @param target  the target framebuffer object, must be [[WebGLRenderingContext.FRAMEBUFFER]].
   * @return the framebuffer status.
   */
  def checkFramebufferStatus(target: Int): F[Int]

  /**
   * Clears the buffers specified in `mask` with the current [[WebGLRenderingContext#clearColor]], [[WebGLRenderingContext#clearDepth]] and [[WebGLRenderingContext#clearStencil]].
   *
   * @param mask  The buffers to clear, a bitmask of one or more of [[WebGLRenderingContext.COLOR_BUFFER_BIT]], [[WebGLRenderingContext.DEPTH_BUFFER_BIT]] and [[WebGLRenderingContext.STENCIL_BUFFER_BIT]].
   */
  def clear(mask: Int): F[Unit]

  /**
   * Sets the clear color to use with [[WebGLRenderingContext#clear]].
   */
  def clearColor(red: Double, green: Double, blue: Double, alpha: Double): F[Unit]

  /**
   * Sets the clear depth to use with [[WebGLRenderingContext#clear]].
   */
  def clearDepth(depth: Double): F[Unit]

  /**
   * Sets the stencil value to use with [[WebGLRenderingContext#clear]].
   */
  def clearStencil(s: Int): F[Unit]

  /**
   * Enable and disable writing to the given channels.  For each channel, `true` will allow writing, `false` will prevent it.
   */
  def colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean): F[Unit]

  /**
   * Compiles the provided shader.
   *
   * The [[WebGLRenderingContext#getShaderParameter]] can be used to determine if this operation succeeded.
   */
  def compileShader(shader: WebGLShader): F[Unit]

  /**
   * Loads a 2-dimensional texture into a texture unit, compressed with the specified algorithm.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param internalformat  the format of the compressed data.
   *  @param width  the width of the texture image.
   *  @param height the height of the texture image.
   *  @param border the border width.  Must be 0.
   *  @param data  the compressed image data.
   */
  def compressedTexImage2D(target: Int, level: Int, internalformat: Int,
      width: Int, height: Int, border: Int,
      data: ArrayBufferView): F[Unit]

  /**
   * Loads a 2-dimensional texture subimage into a texture unit, compressed with the specified algorithm.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param xoffset the x texel offset into the texture image.
   *  @param yoffset the y texel offset into the texture image.
   *  @param width  the width of the texture image.
   *  @param height the height of the texture image.
   *  @param format the format of the compressed image data
   *  @param data  the compressed image data.
   */
  def compressedTexSubImage2D(target: Int, level: Int, xoffset: Int,
      yoffset: Int, width: Int, height: Int, format: Int,
      data: ArrayBufferView): F[Unit]

  /**
   * Loads a 2-dimensional texture into a texture unit from the current framebuffer.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param internalformat  the format of the data.  May be [[WebGLRenderingContext.ALPHA]], [[WebGLRenderingContext.LUMINANCE]], [[WebGLRenderingContext.LUMINANCE_ALPHA]], [[WebGLRenderingContext.RGB]], or [[WebGLRenderingContext.RGBA]].
   *  @param x the window coordinates of the lower left corner of the framebuffer.
   *  @param y the window coordinates of the lower left corner of the framebuffer.
   *  @param width  the width of the texture image.
   *  @param height the height of the texture image.
   *  @param border the border width.  Must be 0.
   */
  def copyTexImage2D(target: Int, level: Int, internalformat: Int, x: Int, y: Int, width: Int, height: Int, border: Int): F[Unit]

  /**
   * Loads a 2-dimensional texture subimage into a texture unit from the current framebuffer.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param xoffset the x texel offset into the texture image.
   *  @param yoffset the y texel offset into the texture image.
   *  @param x the window coordinates of the lower left corner of the framebuffer.
   *  @param y the window coordinates of the lower left corner of the framebuffer.
   *  @param width  the width of the texture image.
   *  @param height the height of the texture image.
   */
  def copyTexSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, x: Int, y: Int, width: Int, height: Int): F[Unit]

  /**
   * Creates a new [[WebGLBuffer]].
   */
  def createBuffer(): F[WebGLBuffer]

  /**
   * Creates a new [[WebGLFramebuffer]].
   */
  def createFramebuffer(): F[WebGLFramebuffer]

  /**
   * Creates a new [[WebGLProgram]].
   */
  def createProgram(): F[WebGLProgram]

  /**
   * Creates a new [[WebGLRenderbuffer]].
   */
  def createRenderbuffer(): F[WebGLRenderbuffer]

  /**
   * Creates a new [[WebGLShader]].
   */
  def createShader(`type`: Int): F[WebGLShader]

  /**
   * Creates a new [[WebGLTexture]].
   */
  def createTexture(): F[WebGLTexture]

  /**
   * Set the culling mode for front and back facing polygons.
   *
   * @param mode the culling mode, may be [[WebGLRenderingContext.FRONT]], [[WebGLRenderingContext.BACK]] or [[WebGLRenderingContext.FRONT_AND_BACK]].
   *             When [[WebGLRenderingContext.FRONT_AND_BACK]] is set, no triangles are drawn, however lines and points will.
   */
  def cullFace(mode: Int): F[Unit]

  /**
   * Flags the specified [[WebGLBuffer]] for deletion.  When it is no longer used by the WebGL system it
   * will be deleted.
   *
   * ''Note'': garbage collection will also delete the buffer, it is not mandatory to call this method.
   */
  def deleteBuffer(buffer: WebGLBuffer): F[Unit]

  /**
   * Flags the specified [[WebGLFramebuffer]] for deletion.  When it is no longer used by the WebGL system it
   * will be deleted.
   *
   * ''Note'': garbage collection will also delete the framebuffer, it is not mandatory to call this method.
   */
  def deleteFramebuffer(framebuffer: WebGLFramebuffer): F[Unit]

  /**
   * Flags the specified [[WebGLProgram]] for deletion.  When it is no longer used by the WebGL system it
   * will be deleted.
   *
   * ''Note'': garbage collection will also delete the program, it is not mandatory to call this method.
   */
  def deleteProgram(program: WebGLProgram): F[Unit]

  /**
   * Flags the specified [[WebGLRenderbuffer]] for deletion.  When it is no longer used by the WebGL system it
   * will be deleted.
   *
   * ''Note'': garbage collection will also delete the renderbuffer, it is not mandatory to call this method.
   */
  def deleteRenderbuffer(renderbuffer: WebGLRenderbuffer): F[Unit]

  /**
   * Flags the specified [[WebGLShader]] for deletion.  When it is no longer used by the WebGL system it
   * will be deleted.
   *
   * ''Note'': garbage collection will also delete the shader, it is not mandatory to call this method.
   */
  def deleteShader(shader: WebGLShader): F[Unit]

  /**
   * Flags the specified [[WebGLTexture]] for deletion.  When it is no longer used by the WebGL system it
   * will be deleted.
   *
   * ''Note'': garbage collection will also delete the texture, it is not mandatory to call this method.
   */
  def deleteTexture(texture: WebGLTexture): F[Unit]

  /**
   * Set the function used to discard fragments.  When depth testing is enabled, the fragment depth is compared with
   * the current depth, and is allowed onto the framebuffer.
   *
   * @param func  the function to allow the fragment to be drawn.  Values are [[WebGLRenderingContext.NEVER]], [[WebGLRenderingContext.LESS]], [[WebGLRenderingContext.EQUAL]], [[WebGLRenderingContext.LEQUAL]]
   *              [[WebGLRenderingContext.GREATER]], [[WebGLRenderingContext.NOTEQUAL]], [[WebGLRenderingContext.GEQUAL]], and [[WebGLRenderingContext.ALWAYS]].
   */
  def depthFunc(func: Int): F[Unit]

  /**
   * Enables/disables writing to the depth buffer.
   *
   * @param flag  when `false`, depth writing is disabled, otherwise it is enabled.
   */
  def depthMask(flag: Boolean): F[Unit]

  /**
   * Sets the mapping from normalized device coordinates to window coordinates.
   * "normalized device coordinates" in this context really means "normalized depth map values".
   *
   * ''note'' there is no requirement that zNear &lt; zFar.
   *
   * Both parameters are clamped to -1 .. 1
   * @param zNear the near clipping plane, initially 0.
   * @param zFar the far clipping plane, initially 1
   */
  def depthRange(zNear: Double, zFar: Double): F[Unit]

  /**
   * Detaches a [[WebGLShader]] from a [[WebGLProgram]].
   *
   * If the shader has been flagged as deleted by a call to [[WebGLRenderingContext#deleteShader]], it will be deleted.
   */
  def detachShader(program: WebGLProgram, shader: WebGLShader): F[Unit]

  /**
   * Disables a GL capability.
   *
   * @param cap  the capability to disable.  May be [[WebGLRenderingContext.BLEND]], [[WebGLRenderingContext.CULL_FACE]],
   *             [[WebGLRenderingContext.DEPTH_TEST]], [[WebGLRenderingContext.DITHER]], [[WebGLRenderingContext.POLYGON_OFFSET_FILL]],
   *             [[WebGLRenderingContext.SAMPLE_ALPHA_TO_COVERAGE]], [[WebGLRenderingContext.SAMPLE_COVERAGE]],
   *             [[WebGLRenderingContext.SCISSOR_TEST]], or [[WebGLRenderingContext.STENCIL_TEST]].
   */
  def disable(cap: Int): F[Unit]

  /**
   * Disables the generic vertex attribute array specified by index.
   */
  def disableVertexAttribArray(index: Int): F[Unit]

  /**
   * Renders the primitives in the active arrays.
   *
   * @param mode  the kind of primitives to render.  May be [[WebGLRenderingContext.POINTS]], [[WebGLRenderingContext.LINES]], [[WebGLRenderingContext.LINE_STRIP]], [[WebGLRenderingContext.LINE_LOOP]], [[WebGLRenderingContext.TRIANGLES]], [[WebGLRenderingContext.TRIANGLE_STRIP]], [[WebGLRenderingContext.TRIANGLE_FAN]], or [[WebGLRenderingContext.TRIANGLES]]
   * @param first  the starting index into the arrays.
   * @param count  the number of indices to draw.
   */
  def drawArrays(mode: Int, first: Int, count: Int): F[Unit]

  /**
   * Renders the primitives in the active arrays using an [[WebGLRenderingContext.ELEMENT_ARRAY_BUFFER]] to index them.
   *
   * @param mode   the kind of primitives to render.  May be [[WebGLRenderingContext.POINTS]], [[WebGLRenderingContext.LINES]],
   *               [[WebGLRenderingContext.LINE_STRIP]], [[WebGLRenderingContext.LINE_LOOP]], [[WebGLRenderingContext.TRIANGLES]],
   *               [[WebGLRenderingContext.TRIANGLE_STRIP]], [[WebGLRenderingContext.TRIANGLE_FAN]],
   *                or [[WebGLRenderingContext.TRIANGLES]]
   * @param count  the number of elements to render.
   * @param type   the type of index value in the [[WebGLRenderingContext.ELEMENT_ARRAY_BUFFER]].  May be
   *               [[WebGLRenderingContext.UNSIGNED_BYTE]] or [[WebGLRenderingContext.UNSIGNED_SHORT]]
   * @param offset the offset into the [[WebGLRenderingContext.ELEMENT_ARRAY_BUFFER]] to begin drawing from.
   */
  def drawElements(mode: Int, count: Int, `type`: Int, offset: Int): F[Unit]

  /**
   * Enables a GL capability.
   *
   * @param cap  the capability to enable.  May be [[WebGLRenderingContext.BLEND]], [[WebGLRenderingContext.CULL_FACE]],
   *             [[WebGLRenderingContext.DEPTH_TEST]], [[WebGLRenderingContext.DITHER]], [[WebGLRenderingContext.POLYGON_OFFSET_FILL]],
   *             [[WebGLRenderingContext.SAMPLE_ALPHA_TO_COVERAGE]], [[WebGLRenderingContext.SAMPLE_COVERAGE]],
   *             [[WebGLRenderingContext.SCISSOR_TEST]], or [[WebGLRenderingContext.STENCIL_TEST]].
   */
  def enable(cap: Int): F[Unit]

  /**
   * Enables the generic vertex attribute array specified by index.
   */
  def enableVertexAttribArray(index: Int): F[Unit]

  /**
   * Block until all GL execution is complete.
   */
  def finish(): F[Unit]

  /**
   * Force all pending GL execution to complete as soon as possible.
   */
  def flush(): F[Unit]

  /**
   * Attach a [[WebGLRenderbuffer]] to a [[WebGLFramebuffer]].
   *
   * @param target  must be [[WebGLRenderingContext.FRAMEBUFFER]]
   * @param attachment  the attachment point on the framebuffer to attach the renderbuffer.  May be [[WebGLRenderingContext.COLOR_ATTACHMENT0]],
   *                    [[WebGLRenderingContext.DEPTH_ATTACHMENT]], [[WebGLRenderingContext.STENCIL_ATTACHMENT]], or
   *                    [[WebGLRenderingContext.DEPTH_STENCIL_ATTACHMENT]].
   * @param renderbuffertarget  must be [[WebGLRenderingContext.RENDERBUFFER]]
   * @param renderbuffer  the renderbuffer to attach.
   */
  def framebufferRenderbuffer(target: Int, attachment: Int,
      renderbuffertarget: Int,
      renderbuffer: WebGLRenderbuffer): F[Unit]

  /**
   * Attach a [[WebGLTexture]] to a [[WebGLFramebuffer]].
   *
   * @param target  must be [[WebGLRenderingContext.FRAMEBUFFER]]
   * @param attachment  the attachment point on the framebuffer to attach the texture.  May be [[WebGLRenderingContext.COLOR_ATTACHMENT0]], [[WebGLRenderingContext.DEPTH_ATTACHMENT]], [[WebGLRenderingContext.STENCIL_ATTACHMENT]], or [[WebGLRenderingContext.DEPTH_STENCIL_ATTACHMENT]].
   * @param textarget  the texture target.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]],
   *                   [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   * @param texture  the texture to be attached
   * @param level  the miplevel to be attached
   */

  /**
   * Specifies the winding that is considered front-facing for the purposes of CULL_FACE.
   *
   * @param mode  The winding to consider front-facing.  May be [[WebGLRenderingContext.CW]] or [[WebGLRenderingContext.CCW]]
   */
  def frontFace(mode: Int): F[Unit]

  /**
   * Generate the complete set of mipmaps for the active texture derived from level 0.
   *
   * @param target  the texture target, may be [[WebGLRenderingContext.TEXTURE_2D]] or [[WebGLRenderingContext.TEXTURE_CUBE_MAP]].
   */
  def generateMipmap(target: Int): F[Unit]

  /**
   * Returns a new [[WebGLActiveInfo]] object describing the given attribute at `index`.
   */
  def getActiveAttrib(program: WebGLProgram, index: Int): F[WebGLActiveInfo] 

  /**
   * Returns a new [[WebGLActiveInfo]] object describing the given uniform at `index`.
   */
  def getActiveUniform(program: WebGLProgram, index: Int): F[WebGLActiveInfo]

  /**
   * Returns a new array containing the shaders attached to the given program.
   */
  def getAttachedShaders(
      program: WebGLProgram): F[js.Array[WebGLShader]]

  /**
   * Returns the index of the named attribute, or -1 on error.
   */
  def getAttribLocation(program: WebGLProgram, name: String): F[Int]

  /**
   * Returns the value of the requested parameter for a buffer.
   *
   * @param target  must be [[WebGLRenderingContext.ARRAY_BUFFER]] or [[WebGLRenderingContext.ELEMENT_ARRAY_BUFFER]]
   * @param pname  the buffer parameter to retrieve, may be [[WebGLRenderingContext.BUFFER_SIZE]] or [[WebGLRenderingContext.BUFFER_USAGE]]
   *
   */
  def getBufferParameter(target: Int, pname: Int): F[Int]

  /**
   * Returns the value for the given `pname`.  Returns a value who's type depends on the requested parameter.
   *
   * @param pname  The parameter to query.  May be [[WebGLRenderingContext.ACTIVE_TEXTURE]], [[WebGLRenderingContext.ALIASED_LINE_WIDTH_RANGE]], [[WebGLRenderingContext.ALIASED_POINT_SIZE_RANGE]],
   *               [[WebGLRenderingContext.ALPHA_BITS]], [[WebGLRenderingContext.ARRAY_BUFFER_BINDING]], [[WebGLRenderingContext.BLEND]], [[WebGLRenderingContext.BLEND_COLOR]], [[WebGLRenderingContext.BLEND_DST_ALPHA]], [[WebGLRenderingContext.BLEND_DST_RGB]],
   *               [[WebGLRenderingContext.BLEND_EQUATION_ALPHA]], [[WebGLRenderingContext.BLEND_EQUATION_RGB]], [[WebGLRenderingContext.BLEND_SRC_ALPHA]], [[WebGLRenderingContext.BLEND_SRC_RGB]], [[WebGLRenderingContext.BLUE_BITS]], [[WebGLRenderingContext.COLOR_CLEAR_VALUE]],
   *               [[WebGLRenderingContext.COLOR_WRITEMASK]], [[WebGLRenderingContext.COMPRESSED_TEXTURE_FORMATS]], [[WebGLRenderingContext.CULL_FACE]], [[WebGLRenderingContext.CULL_FACE_MODE]], [[WebGLRenderingContext.CURRENT_PROGRAM]], [[WebGLRenderingContext.DEPTH_BITS]],
   *               [[WebGLRenderingContext.DEPTH_CLEAR_VALUE]], [[WebGLRenderingContext.DEPTH_FUNC]], `DEPTH-RANGE`, [[WebGLRenderingContext.DEPTH_TEST]], [[WebGLRenderingContext.DEPTH_WRITEMASK]], [[WebGLRenderingContext.DITHER]], [[WebGLRenderingContext.ELEMENT_ARRAY_BUFFER_BINDING]],
   *               [[WebGLRenderingContext.FRAMEBUFFER_BINDING]], [[WebGLRenderingContext.FRONT_FACE]], [[WebGLRenderingContext.GENERATE_MIPMAP_HINT]], [[WebGLRenderingContext.GREEN_BITS]], [[WebGLRenderingContext.LINE_WIDTH]], [[WebGLRenderingContext.MAX_COMBINED_TEXTURE_IMAGE_UNITS]],
   *               [[WebGLRenderingContext.MAX_CUBE_MAP_TEXTURE_SIZE]], [[WebGLRenderingContext.MAX_FRAGMENT_UNIFORM_VECTORS]], [[WebGLRenderingContext.MAX_RENDERBUFFER_SIZE]], [[WebGLRenderingContext.MAX_TEXTURE_IMAGE_UNITS]], [[WebGLRenderingContext.MAX_TEXTURE_SIZE]],
   *               [[WebGLRenderingContext.MAX_VARYING_VECTORS]], [[WebGLRenderingContext.MAX_VERTEX_ATTRIBS]], [[WebGLRenderingContext.MAX_VERTEX_TEXTURE_IMAGE_UNITS]], [[WebGLRenderingContext.MAX_VERTEX_UNIFORM_VECTORS]],
   *               [[WebGLRenderingContext.MAX_VIEWPORT_DIMS]], [[WebGLRenderingContext.PACK_ALIGNMENT]], [[WebGLRenderingContext.POLYGON_OFFSET_FACTOR]], [[WebGLRenderingContext.POLYGON_OFFSET_FILL]], [[WebGLRenderingContext.POLYGON_OFFSET_UNITS]],
   *               [[WebGLRenderingContext.RED_BITS]], [[WebGLRenderingContext.RENDERBUFFER_BINDING]], [[WebGLRenderingContext.RENDERER]], [[WebGLRenderingContext.SAMPLE_BUFFERS]], [[WebGLRenderingContext.SAMPLE_COVERAGE_INVERT]], [[WebGLRenderingContext.SAMPLE_COVERAGE_VALUE]],
   *               [[WebGLRenderingContext.SAMPLES]], [[WebGLRenderingContext.SCISSOR_BOX]], [[WebGLRenderingContext.SCISSOR_TEST]], [[WebGLRenderingContext.SHADING_LANGUAGE_VERSION]], [[WebGLRenderingContext.STENCIL_BACK_FAIL]], [[WebGLRenderingContext.STENCIL_BACK_FUNC]],
   *               [[WebGLRenderingContext.STENCIL_BACK_PASS_DEPTH_FAIL]], [[WebGLRenderingContext.STENCIL_BACK_PASS_DEPTH_PASS]], [[WebGLRenderingContext.STENCIL_BACK_REF]], [[WebGLRenderingContext.STENCIL_BACK_VALUE_MASK]],
   *               [[WebGLRenderingContext.STENCIL_BACK_WRITEMASK]], [[WebGLRenderingContext.STENCIL_BITS]], [[WebGLRenderingContext.STENCIL_CLEAR_VALUE]], [[WebGLRenderingContext.STENCIL_FAIL]], [[WebGLRenderingContext.STENCIL_FUNC]], [[WebGLRenderingContext.STENCIL_PASS_DEPTH_FAIL]],
   *               [[WebGLRenderingContext.STENCIL_PASS_DEPTH_PASS]], [[WebGLRenderingContext.STENCIL_REF]], [[WebGLRenderingContext.STENCIL_TEST]], [[WebGLRenderingContext.STENCIL_VALUE_MASK]], [[WebGLRenderingContext.STENCIL_WRITEMASK]], [[WebGLRenderingContext.SUBPIXEL_BITS]],
   *               [[WebGLRenderingContext.TEXTURE_BINDING_2D]], [[WebGLRenderingContext.TEXTURE_BINDING_CUBE_MAP]], [[WebGLRenderingContext.UNPACK_ALIGNMENT]], [[WebGLRenderingContext.UNPACK_COLORSPACE_CONVERSION_WEBGL]], [[WebGLRenderingContext.UNPACK_FLIP_Y_WEBGL]],
   *               [[WebGLRenderingContext.UNPACK_PREMULTIPLY_ALPHA_WEBGL]], [[WebGLRenderingContext.VENDOR]], [[WebGLRenderingContext.VERSION]] or [[WebGLRenderingContext.VIEWPORT]].
   */
  def getParameter(pname: Int): F[js.Any]

  /**
   * Returns the error value, and resets the error to [[WebGLRenderingContext.NO_ERROR]].
   *
   * Only the first error is recorded, new errors are not stored until the error value is reset
   * to [[WebGLRenderingContext.NO_ERROR]] by a call to this method.
   *
   * @return the error code.  One of [[WebGLRenderingContext.NO_ERROR]], [[WebGLRenderingContext.INVALID_ENUM]], [[WebGLRenderingContext.INVALID_VALUE]], [[WebGLRenderingContext.INVALID_OPERATION]], [[WebGLRenderingContext.INVALID_FRAMEBUFFER_OPERATION]], or [[WebGLRenderingContext.OUT_OF_MEMORY]].
   */
  def getError(): F[Int]

  /**
   * Returns the value for the given parameter name on for the target and attachment.
   * The return type is dependent on the requested parameter.
   *
   * @param target must be FRAMEBUFFER
   * @param attachment the attachment to examine.  May be [[WebGLRenderingContext.COLOR_ATTACHMENT0]], [[WebGLRenderingContext.DEPTH_ATTACHMENT]],
   *  				   [[WebGLRenderingContext.STENCIL_ATTACHMENT]], or [[WebGLRenderingContext.DEPTH_STENCIL_ATTACHMENT]].
   * @param pname the framebuffer attachment parameter.  May be [[WebGLRenderingContext.FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE]],
   *              [[WebGLRenderingContext.FRAMEBUFFER_ATTACHMENT_OBJECT_NAME]], [[WebGLRenderingContext.FRAMEBUFFER_ATTACHMENT_TEXTURE_LEVEL]], or
   *              [[WebGLRenderingContext.FRAMEBUFFER_ATTACHMENT_TEXTURE_CUBE_MAP_FACE]]
   */
  def getFramebufferAttachmentParameter(target: Int, attachment: Int, pname: Int): F[js.Any]

  /**
   * Returns the value for the given parameter name for the program.
   * The return type is dependent on the requested parameter.
   *
   * @param program the program to query.
   * @param pname  the parameter to get, may be one of [[WebGLRenderingContext.DELETE_STATUS]], [[WebGLRenderingContext.LINK_STATUS]],
   *               [[WebGLRenderingContext.VALIDATE_STATUS]], [[WebGLRenderingContext.ATTACHED_SHADERS]],
   *               [[WebGLRenderingContext.ACTIVE_ATTRIBUTES]], or [[WebGLRenderingContext.ACTIVE_UNIFORMS]].
   */
  def getProgramParameter(program: WebGLProgram, pname: Int): F[js.Any]

  /**
   * Returns a string containing information about the last link or validation operation for a program.
   */
  def getProgramInfoLog(program: WebGLProgram): F[String]

  /**
   * Returns the value of a parameter on the active renderbuffer.
   * The return type is dependent on the requested parameter.
   *
   * @param target  must be [[WebGLRenderingContext.RENDERBUFFER]]
   * @param pname  the parameter to query, may be [[WebGLRenderingContext.RENDERBUFFER_WIDTH]],
   *               [[WebGLRenderingContext.RENDERBUFFER_HEIGHT]], [[WebGLRenderingContext.RENDERBUFFER_INTERNAL_FORMAT]],
   *               [[WebGLRenderingContext.RENDERBUFFER_RED_SIZE]], [[WebGLRenderingContext.RENDERBUFFER_GREEN_SIZE]],
   *               [[WebGLRenderingContext.RENDERBUFFER_BLUE_SIZE]], [[WebGLRenderingContext.RENDERBUFFER_ALPHA_SIZE]],
   *               [[WebGLRenderingContext.RENDERBUFFER_STENCIL_SIZE]], or [[WebGLRenderingContext.RENDERBUFFER_DEPTH_SIZE]]
   */
  def getRenderbufferParameter(target: Int, pname: Int): F[js.Any]

  /**
   * Returns the value of a parameter on the specified [[WebGLShader]].
   * The return type is dependent on the requested parameter.
   *
   * @param shader  the shader to query
   * @param pname  the parameter to get, may be one of [[WebGLRenderingContext.SHADER_TYPE]],
   *               [[WebGLRenderingContext.DELETE_STATUS]] or [[WebGLRenderingContext.COMPILE_STATUS]]
   *
   */
  def getShaderParameter(shader: WebGLShader, pname: Int): F[js.Any]

  /**
   * Returns a new [[WebGLShaderPrecisionFormat]] for the given shader type and precision type.
   *
   * @param shadertype the type of shader, may be [[WebGLRenderingContext.FRAGMENT_SHADER]] or [[WebGLRenderingContext.VERTEX_SHADER]].
   * @param precisiontype the precision type to query, may be [[WebGLRenderingContext.LOW_FLOAT]],
   *                      [[WebGLRenderingContext.MEDIUM_FLOAT]], [[WebGLRenderingContext.HIGH_FLOAT]], [[WebGLRenderingContext.LOW_INT]],
   *                      [[WebGLRenderingContext.MEDIUM_INT]], or [[WebGLRenderingContext.HIGH_INT]].
   */
  def getShaderPrecisionFormat(shadertype: Int, precisiontype: Int): F[WebGLShaderPrecisionFormat]

  /**
   * Returns the information log from the last compile of the shader.
   */
  def getShaderInfoLog(shader: WebGLShader): F[String]

  /**
   * Returns the source of the given shader.
   */
  def getShaderSource(shader: WebGLShader): F[String]

  /**
   * Returns the value of the given texture parameter on the target of the active texture.
   *
   *  @param target  the target to query.  May be either [[WebGLRenderingContext.TEXTURE_2D]] or `TEXTURE_CUBE_MAP`.
   *  @param pname  the parameter to query.  May be either [[WebGLRenderingContext.TEXTURE_MAG_FILTER]],
   *                [[WebGLRenderingContext.TEXTURE_MIN_FILTER]], [[WebGLRenderingContext.TEXTURE_WRAP_S]],
   *                 or [[WebGLRenderingContext.TEXTURE_WRAP_T]].
   */
  def getTexParameter(target: Int, pname: Int): F[js.Any]

  /**
   * Returns the value of the uniform in the given program and location.  The return type is dependent
   * on the uniform type.
   */
  def getUniform(program: WebGLProgram, location: WebGLUniformLocation): F[js.Any]

  /**
   * Returns a new [[WebGLUniformLocation]] that represents the location of the given uniform in the specified program.
   * If the uniform does not exist, or another error occurs, returns `null`.
   */
  def getUniformLocation(program: WebGLProgram, name: String): F[WebGLUniformLocation]

  /**
   * Returns the value of the named parameter for a given vertex attribute index.
   *
   * @param index  the index of the vertex attribute to query.
   * @param pname  the requested parameter, may be [[WebGLRenderingContext.VERTEX_ATTRIB_ARRAY_BUFFER_BINDING]],
   * 			   [[WebGLRenderingContext.VERTEX_ATTRIB_ARRAY_ENABLED]], [[WebGLRenderingContext.VERTEX_ATTRIB_ARRAY_SIZE]],
   *               [[WebGLRenderingContext.VERTEX_ATTRIB_ARRAY_STRIDE]], [[WebGLRenderingContext.VERTEX_ATTRIB_ARRAY_TYPE]],
   *               [[WebGLRenderingContext.VERTEX_ATTRIB_ARRAY_NORMALIZED]], [[WebGLRenderingContext.CURRENT_VERTEX_ATTRIB]]
   *
   */
  def getVertexAttrib(index: Int, pname: Int): F[js.Any]

  /**
   * Returns the offset of the vertex attribute.
   *
   * @param index  the index of the vertex attribute to retrieve
   * @param pname  must be [[WebGLRenderingContext.VERTEX_ATTRIB_ARRAY_POINTER]]
   */
  def getVertexAttribOffset(index: Int, pname: Int): F[Int]

  /**
   * Specifies implementation specific hints.
   *
   * @param target  the hint to specify.  Must be [[WebGLRenderingContext.GENERATE_MIPMAP_HINT]]
   * @param mode  the desired mode.  Must be one of [[WebGLRenderingContext.FASTEST]],
   *              [[WebGLRenderingContext.NICEST]], or [[WebGLRenderingContext.DONT_CARE]].
   */
  def hint(target: Int, mode: Int): F[Unit]

  /**
   * Returns `true` if the `buffer` is valid, `false` otherwise.
   */
  def isBuffer(buffer: js.Any): F[Boolean]

  /**
   * Returns `true` if the specified capability is enabled, `false` otherwise.
   * @see [[WebGLRenderingContext#enable]]
   */
  def isEnabled(cap: Int): F[Boolean]

  /**
   * Returns `true` if the `framebuffer` is valid, `false` otherwise.
   */
  def isFramebuffer(framebuffer: js.Any): F[Boolean]

  /**
   * Returns `true` if the `program` is valid, `false` otherwise.
   */
  def isProgram(program: js.Any): F[Boolean]

  /**
   * Returns `true` if the `renderbuffer` is valid, `false` otherwise.
   */
  def isRenderbuffer(renderbuffer: js.Any): F[Boolean]

  /**
   * Returns `true` if the `shader` is valid, `false` otherwise.
   */
  def isShader(shader: js.Any): F[Boolean]

  /**
   * Returns `true` if the `texture` is valid, `false` otherwise.
   */
  def isTexture(texture: js.Any): F[Boolean]

  /**
   * Specifies the line width.
   */
  def lineWidth(width: Double): F[Unit]

  /**
   * Attempts to link the specified [[WebGLProgram]].
   */
  def linkProgram(program: WebGLProgram): F[Unit]

  /**
   * Sets the pixel store mode, used when copying image data such as framebuffers or textures.
   *
   * @param pname  the property to change.  May be one of [[WebGLRenderingContext.PACK_ALIGNMENT]],
   *               [[WebGLRenderingContext.UNPACK_ALIGNMENT]], [[WebGLRenderingContext.UNPACK_FLIP_Y_WEBGL]],
   *               [[WebGLRenderingContext.UNPACK_PREMULTIPLY_ALPHA_WEBGL]] or [[WebGLRenderingContext.UNPACK_COLORSPACE_CONVERSION_WEBGL]].
   */
  def pixelStorei(pname: Int, param: Int): F[Unit]

  /**
   * Specifies the polygon offset.  When [[WebGLRenderingContext.POLYGON_OFFSET_FILL]] is enabled, depth values for a fragment have an offset applied
   * to them, calculated as `factor`*DZ + r*`units`, where DZ is the change in z based on the polygon's screen area, and r is the minimum value that
   * is guaranteed produce a measurable offset.
   */
  def polygonOffset(factor: Double, units: Double): F[Unit]

  /**
   * Reads pixels from the framebuffer into `pixels`.
   *
   * @param x  the x coordinate of the bottom left of the area to read.
   * @param y  the y coordinate of the bottom left of the area to read.
   * @param width  the width of the area to read.
   * @param height  the height of the area to read.
   * @param format  the format of the desired output.  Must be one of [[WebGLRenderingContext.UNSIGNED_BYTE]],
   *             [[WebGLRenderingContext.UNSIGNED_SHORT_4_4_4_4]], [[WebGLRenderingContext.UNSIGNED_SHORT_5_5_5_1]],
   *             [[WebGLRenderingContext.UNSIGNED_SHORT_5_6_5]]
   */
  def readPixels(x: Int, y: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: ArrayBufferView): F[Unit] 

  /**
   * Create renderbuffer image storage.
   *
   * Initializes the renderbuffer to use the new storage format, replacing any previous store.
   *
   * @param target  must be [[WebGLRenderingContext.RENDERBUFFER]]
   * @param internalformat  specifies the format of the renderbuffer.  May be one of [[WebGLRenderingContext.RGBA4]], [[WebGLRenderingContext.RGB565]],
   *                        [[WebGLRenderingContext.RGB5_A1]], [[WebGLRenderingContext.DEPTH_COMPONENT16]], [[WebGLRenderingContext.STENCIL_INDEX8]] or
   *                        [[WebGLRenderingContext.DEPTH_STENCIL]].
   */
  def renderbufferStorage(target: Int, internalformat: Int, width: Int, height: Int): F[Unit]

  /**
   * Sets the sampling coverage parameters for primitive antialiasing.
   *
   * The OpenGL multisampling algorithm is too involved to concisely explain here.
   * Please consult [[http://www.opengl.org/registry/specs/SGIS/multisample.txt]].
   *
   * @param value  the sample coverage value, clamped to 0..1.
   * @param invert  if true, the mask will be bitwise-inverted.
   */
  def sampleCoverage(value: Int, invert: Boolean): F[Unit]

  /**
   * Sets the scissor rectangle.  When [[WebGLRenderingContext.SCISSOR_TEST]] is enabled, rendering will be restricted to this rectangle.
   */
  def scissor(x: Int, y: Int, width: Int, height: Int): F[Unit]

  /**
   * Sets the GLSL source for the given shader.
   */
  def shaderSource(shader: WebGLShader, source: String): F[Unit]

  /**
   * Sets the stencil test for front and back faces.
   * @param func the test function.  One of [[WebGLRenderingContext.NEVER]], [[WebGLRenderingContext.LESS]],
   * 			 [[WebGLRenderingContext.LEQUAL]], [[WebGLRenderingContext.GREATER]], [[WebGLRenderingContext.GEQUAL]],
   *             [[WebGLRenderingContext.EQUAL]], [[WebGLRenderingContext.NOTEQUAL]], and [[WebGLRenderingContext.ALWAYS]]
   * @param ref  the reference value to test against in the stencil buffer
   * @param mask mask that is ANDed with `ref` and the tested value and stored in the stencil buffer.
   */
  def stencilFunc(func: Int, ref: Int, mask: Int): F[Unit]

  /**
   * Sets the stencil test for the given face type.
   * @param face the face(s) to configure the test for.  May be [[WebGLRenderingContext.FRONT]], [[WebGLRenderingContext.BACK]]
   *             or [[WebGLRenderingContext.FRONT_AND_BACK]].
   * @param func the test function.  One of [[WebGLRenderingContext.NEVER]], [[WebGLRenderingContext.LESS]],
   * 			 [[WebGLRenderingContext.LEQUAL]], [[WebGLRenderingContext.GREATER]], [[WebGLRenderingContext.GEQUAL]],
   *             [[WebGLRenderingContext.EQUAL]], [[WebGLRenderingContext.NOTEQUAL]], and [[WebGLRenderingContext.ALWAYS]]
   * @param ref  the reference value to test against in the stencil buffer
   * @param mask mask that is ANDed with `ref` and the tested value and stored in the stencil buffer.
   */
  def stencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int): F[Unit]

  /**
   * Configure which bits in the stencil buffer may be written to by front or back faces.
   * @param mask  the write mask.  Set bits are allowed to be written to the corresponding stencil buffer bit.
   */
  def stencilMask(mask: Int): F[Unit]

  /**
   * Configure which bits in the stencil buffer may be written to by the given face type.
   * @param face the face(s) to configure the mask for.  May be [[WebGLRenderingContext.FRONT]], [[WebGLRenderingContext.BACK]]
   *             or [[WebGLRenderingContext.FRONT_AND_BACK]].
   * @param mask  the write mask.  Set bits are allowed to be written to the corresponding stencil buffer bit.
   */
  def stencilMaskSeparate(face: Int, mask: Int): F[Unit]

  /**
   * Configure the effect of a stencil or depth test failing for front or back faces.
   *
   * @param fail  the effect of the stencil test failing.  May be one of
   *        [[WebGLRenderingContext.KEEP]], [[WebGLRenderingContext.ZERO]], [[WebGLRenderingContext.REPLACE]],
   *        [[WebGLRenderingContext.INCR]], [[WebGLRenderingContext.INCR_WRAP]], [[WebGLRenderingContext.DECR]],
   *        [[WebGLRenderingContext.DECR_WRAP]], and [[WebGLRenderingContext.INVERT]]
   *
   * @param zfail  the effect of the stencil test passing but the depth test failing.  Parameters are as fail.
   * @param zpass  the effect of the stencil test failing but the depth test passing.  Parameters are as fail.
   */
  def stencilOp(fail: Int, zfail: Int, zpass: Int): F[Unit]

  /**
   * Configure the effect of a stencil or depth test failing for the specified faces.
   *
   * @param face the face(s) to configure the stencil operation for.  May be [[WebGLRenderingContext.FRONT]], [[WebGLRenderingContext.BACK]]
   *             or [[WebGLRenderingContext.FRONT_AND_BACK]].
   * @param fail  the effect of the stencil test failing.  May be one of
   *        [[WebGLRenderingContext.KEEP]], [[WebGLRenderingContext.ZERO]], [[WebGLRenderingContext.REPLACE]],
   *        [[WebGLRenderingContext.INCR]], [[WebGLRenderingContext.INCR_WRAP]], [[WebGLRenderingContext.DECR]],
   *        [[WebGLRenderingContext.DECR_WRAP]], and [[WebGLRenderingContext.INVERT]]
   *
   * @param zfail  the effect of the stencil test passing but the depth test failing.  Parameters are as fail.
   * @param zpass  the effect of the stencil test failing but the depth test passing.  Parameters are as fail.
   */
  def stencilOpSeparate(face: Int, fail: Int, zfail: Int, zpass: Int): F[Unit]

  /**
   * Loads a 2-dimensional texture into a texture unit from source data.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param internalformat  the format of the target pixel data.
   *  @param width  the width of the texture image.
   *  @param height the height of the texture image.
   *  @param border the border width.  Must be 0.
   *  @param pixels the source image data.
   */
  def texImage2D(target: Int, level: Int, internalformat: Int, width: Int,
      height: Int, border: Int, format: Int, `type`: Int,
      pixels: ArrayBufferView): F[Unit]

  /**
   * Loads a 2-dimensional texture into a texture unit from an ImageData object.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param internalformat  the format of the target pixel data.
   *  @param format  the format of the incoming pixel data.
   *  @param type the data type of the pixel data.
   *  @param pixels the source image data.
   */
  def texImage2D(target: Int, level: Int, internalformat: Int, format: Int, `type`: Int, pixels: ImageData): F[Unit]

  /**
   * Loads a 2-dimensional texture into a texture unit from an HTMLImageElement object.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param internalformat  the format of the target pixel data.
   *  @param format  the format of the incoming pixel data.
   *  @param type the data type of the pixel data.
   *  @param pixels the source image data.
   */
  def texImage2D(target: Int, level: Int, internalformat: Int, format: Int, `type`: Int, pixels: HTMLImageElement): F[Unit]

  /**
   * Loads a 2-dimensional texture into a texture unit from an HTMLCanvasElement object.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param internalformat  the format of the target pixel data.
   *  @param format  the format of the incoming pixel data.
   *  @param type the data type of the pixel data.
   *  @param pixels the source image data.
   */
  def texImage2D(target: Int, level: Int, internalformat: Int, format: Int, `type`: Int, pixels: HTMLCanvasElement): F[Unit]

  /**
   * Loads a 2-dimensional texture into a texture unit from an HTMLVideoElement object.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param internalformat  the format of the target pixel data.
   *  @param format  the format of the incoming pixel data.
   *  @param type the data type of the pixel data.
   *  @param pixels the source image data.
   */
  def texImage2D(target: Int, level: Int, internalformat: Int, format: Int, `type`: Int, pixels: HTMLVideoElement): F[Unit] 

  /**
   * Sets the texture parameter for the active texture unit.
   *
   * @param target  the texture target to configure.  May be [[WebGLRenderingContext.TEXTURE_2D]] or [[WebGLRenderingContext.TEXTURE_CUBE_MAP]]
   * @param pname  the parameter to change.  May be [[WebGLRenderingContext.TEXTURE_MIN_FILTER]], [[WebGLRenderingContext.TEXTURE_MAG_FILTER]]
   *               [[WebGLRenderingContext.TEXTURE_WRAP_S]], or [[WebGLRenderingContext.TEXTURE_WRAP_T]]
   * @param param  the value to set.  See the corresponding parameters for valid values.
   */
  def texParameterf(target: Int, pname: Int, param: Double): F[Unit]

  /**
   * Sets the texture parameter for the active texture unit.
   *
   * @param target  the texture target to configure.  May be [[WebGLRenderingContext.TEXTURE_2D]] or [[WebGLRenderingContext.TEXTURE_CUBE_MAP]]
   * @param pname  the parameter to change.  May be [[WebGLRenderingContext.TEXTURE_MIN_FILTER]], [[WebGLRenderingContext.TEXTURE_MAG_FILTER]]
   *               [[WebGLRenderingContext.TEXTURE_WRAP_S]], or [[WebGLRenderingContext.TEXTURE_WRAP_T]]
   * @param param  the value to set.  See the corresponding parameters for valid values.
   */
  def texParameteri(target: Int, pname: Int, param: Int): F[Unit]

  /**
   * Loads a 2-dimensional texture subimage into a texture unit from an `ArrayBufferView`.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param xoffset the x texel offset into the texture image.
   *  @param yoffset the y texel offset into the texture image.
   *  @param width  the width of the texture image.
   *  @param height the height of the texture image.
   *  @param format  the format of the incoming pixel data.
   *  @param type the data type of the pixel data.
   *  @param pixels the image data.
   */
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int,
      width: Int, height: Int, format: Int, `type`: Int,
      pixels: ArrayBufferView): F[Unit]

  /**
   * Loads a 2-dimensional texture subimage into a texture unit from an `ImageData` object.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param xoffset the x texel offset into the texture image.
   *  @param yoffset the y texel offset into the texture image.
   *  @param format  the format of the incoming pixel data.
   *  @param type the data type of the pixel data.
   *  @param pixels the image data.
   */
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, `type`: Int, pixels: ImageData): F[Unit] 

  /**
   * Loads a 2-dimensional texture subimage into a texture unit from an `HTMLImageElement`.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param xoffset the x texel offset into the texture image.
   *  @param yoffset the y texel offset into the texture image.
   *  @param format  the format of the incoming pixel data.
   *  @param type the data type of the pixel data.
   *  @param pixels the image data.
   */
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, `type`: Int, pixels: HTMLImageElement): F[Unit]

  /**
   * Loads a 2-dimensional texture subimage into a texture unit from an `HTMLCanvasElement`.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param xoffset the x texel offset into the texture image.
   *  @param yoffset the y texel offset into the texture image.
   *  @param format  the format of the incoming pixel data.
   *  @param type the data type of the pixel data.
   *  @param pixels the image data..
   */
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, `type`: Int, pixels: HTMLCanvasElement): F[Unit]

  /**
   * Loads a 2-dimensional texture subimage into a texture unit from an `HTMLVideoElement`.
   *
   *  @param target  the target on the active texture unit.  May be [[WebGLRenderingContext.TEXTURE_2D]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_X]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_X]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Y]], [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Y]],
   *                 [[WebGLRenderingContext.TEXTURE_CUBE_MAP_POSITIVE_Z]], or [[WebGLRenderingContext.TEXTURE_CUBE_MAP_NEGATIVE_Z]]
   *  @param level  the mipmap level of detail.  0 is the base image.
   *  @param xoffset the x texel offset into the texture image.
   *  @param yoffset the y texel offset into the texture image.
   *  @param format  the format of the incoming pixel data.
   *  @param type the data type of the pixel data.
   *  @param pixels the image data.
   */
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, `type`: Int, pixels: HTMLVideoElement): F[Unit]

  /**
   * Loads a a scalar float into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param x  the scalar to bind to.
   */
  def uniform1f(location: WebGLUniformLocation, x: Double): F[Unit]

  /**
   * Loads a a scalar float into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a `Float32Array` to bind to
   */
  def uniform1fv(location: WebGLUniformLocation, v: Float32Array): F[Unit] 

  /**
   * Loads a a scalar float into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a js.Array to bind to.
   */
  def uniform1fv(location: WebGLUniformLocation, v: js.Array[Double]): F[Unit]

  /**
   * Loads a a scalar integer into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param x  the scalar to bind to.
   */
  def uniform1i(location: WebGLUniformLocation, x: Int): F[Unit]

  /**
   * Loads a a scalar integer into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  an `Int32Array` to bind to
   */
  def uniform1iv(location: WebGLUniformLocation, v: Int32Array): F[Unit] 

  /**
   * Loads a a scalar integer into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a js.Array to bind to
   */
  def uniform1iv(location: WebGLUniformLocation, v: js.Array[Int]): F[Unit]

  /**
   * Loads a a 2-vector of floats into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param x  the first float component
   * @param y  the second float component
   */
  def uniform2f(location: WebGLUniformLocation, x: Double, y: Double): F[Unit] 

  /**
   * Loads a a 2-vector of floats into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a `Float32Array` to bind to
   */
  def uniform2fv(location: WebGLUniformLocation, v: Float32Array): F[Unit]

  /**
   * Loads a a 2-vector of floats
   *
   * @param location  the location to bind into a [[WebGLUniformLocation]].
   * @param v  a js.Array to bind to.
   */
  def uniform2fv(location: WebGLUniformLocation, v: js.Array[Double]): F[Unit] 

  /**
   * Loads a a 2-vector of integers into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param x  the first integer component
   * @param y  the second integer component
   */
  def uniform2i(location: WebGLUniformLocation, x: Int, y: Int): F[Unit] 

  /**
   * Loads a a 2-vector of integers into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  an `Int32Array` to bind to
   */
  def uniform2iv(location: WebGLUniformLocation, v: Int32Array): F[Unit]

  /**
   * Loads a a 2-vector of integers into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a js.Array to bind to
   */
  def uniform2iv(location: WebGLUniformLocation, v: js.Array[Int]): F[Unit] 

  /**
   * Loads a a 3-vector of floats into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param x  the first float component.
   * @param y  the second float component.
   * @param z  the third float component.
   */
  def uniform3f(location: WebGLUniformLocation, x: Double, y: Double, z: Double): F[Unit] 

  /**
   * Loads a a 3-vector of floats into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a `Float32Array` to bind to
   */
  def uniform3fv(location: WebGLUniformLocation, v: Float32Array): F[Unit]

  /**
   * Loads a a 3-vector of floats into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a js.Array to bind to.
   */
  def uniform3fv(location: WebGLUniformLocation, v: js.Array[Double]): F[Unit] 

  /**
   * Loads a a 3-vector of integers into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param x  the first integer component
   * @param y  the second integer component
   * @param z  the third integer component
   */
  def uniform3i(location: WebGLUniformLocation, x: Int, y: Int, z: Int): F[Unit] 

  /**
   * Loads a a 3-vector of integers into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  an `Int32Array` to bind to
   */
  def uniform3iv(location: WebGLUniformLocation, v: Int32Array): F[Unit]

  /**
   * Loads a a 3-vector of integers into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a js.Array to bind to
   */
  def uniform3iv(location: WebGLUniformLocation, v: js.Array[Int]): F[Unit] 

  /**
   * Loads a a 4-vector of floats into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param x  the first float component.
   * @param y  the second float component.
   * @param z  the third float component.
   * @param w  the fourth float component.
   */
  def uniform4f(location: WebGLUniformLocation, x: Double, y: Double, z: Double, w: Double): F[Unit] 

  /**
   * Loads a a 4-vector of floats into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a `Float32Array` to bind to
   */
  def uniform4fv(location: WebGLUniformLocation, v: Float32Array): F[Unit] 

  /**
   * Loads a a 4-vector of floats into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a js.Array to bind to.
   */
  def uniform4fv(location: WebGLUniformLocation, v: js.Array[Double]): F[Unit] 

  /**
   * Loads a a 4-vector of integers into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param x  the first integer component
   * @param y  the second integer component
   * @param z  the third integer component
   * @param w  the third integer component
   */
  def uniform4i(location: WebGLUniformLocation, x: Int, y: Int, z: Int, w: Int): F[Unit] 

  /**
   * Loads a a 4-vector of integers into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  an `Int32Array` to bind to
   */
  def uniform4iv(location: WebGLUniformLocation, v: Int32Array): F[Unit] 

  /**
   * Loads a a 4-vector of integers into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param v  a js.Array to bind to
   */
  def uniform4iv(location: WebGLUniformLocation, v: js.Array[Int]): F[Unit] 

  /**
   * Loads a a 4x2 matrix into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param transpose  if `true`, the matrix will loaded into the uniform transposed.
   * @param value  the source `Float32Array` containing the matrix data.
   */
  def uniformMatrix2fv(location: WebGLUniformLocation, transpose: Boolean, value: Float32Array): F[Unit]

  /**
   * Loads a a 4x2 matrix into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param transpose  if `true`, the matrix will loaded into the uniform transposed.
   * @param value  the source `js.Array` containing the matrix data.
   */
  def uniformMatrix2fv(location: WebGLUniformLocation, transpose: Boolean, value: js.Array[Double]): F[Unit]

  /**
   * Loads a a 4x3 matrix into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param transpose  if `true`, the matrix will loaded into the uniform transposed.
   * @param value  the source `Float32Array` containing the matrix data.
   */
  def uniformMatrix3fv(location: WebGLUniformLocation, transpose: Boolean, value: Float32Array): F[Unit] 

  /**
   * Loads a a 4x3 matrix into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param transpose  if `true`, the matrix will loaded into the uniform transposed.
   * @param value  the source `js.Array` containing the matrix data.
   */
  def uniformMatrix3fv(location: WebGLUniformLocation, transpose: Boolean, value: js.Array[Double]): F[Unit] 

  /**
   * Loads a a 4x4 matrix into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param transpose  if `true`, the matrix will loaded into the uniform transposed.
   * @param value  the source `Float32Array` containing the matrix data.
   */
  def uniformMatrix4fv(location: WebGLUniformLocation, transpose: Boolean, value: Float32Array): F[Unit] 

  /**
   * Loads a a 4x4 matrix into a [[WebGLUniformLocation]].
   *
   * @param location  the location to bind.
   * @param transpose  if `true`, the matrix will loaded into the uniform transposed.
   * @param value  the source `js.Array` containing the matrix data.
   */
  def uniformMatrix4fv(location: WebGLUniformLocation, transpose: Boolean, value: js.Array[Double]): F[Unit] 

  /**
   * Makes a [[WebGLProgram]] become the active program.
   */
  def useProgram(program: WebGLProgram): F[Unit]

  /**
   * Validates a [[WebGLProgram]].
   */
  def validateProgram(program: WebGLProgram): F[Unit]

  /**
   * Loads a scalar into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param x  the scalar to load.
   */
  def vertexAttrib1f(indx: Int, x: Double): F[Unit]

  /**
   * Loads a scalar into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param values the source array for the attribute.
   */
  def vertexAttrib1fv(indx: Int, values: Float32Array): F[Unit]

  /**
   * Loads a scalar into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param values the source array for the attribute.
   */
  def vertexAttrib1fv(indx: Int, values: js.Array[Double]): F[Unit]

  /**
   * Loads a 2-vector into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param x  the first component.
   * @param y  the second component.
   */
  def vertexAttrib2f(indx: Int, x: Double, y: Double): F[Unit]

  /**
   * Loads a 2-vector into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param values the source array for the attribute.
   */
  def vertexAttrib2fv(indx: Int, values: Float32Array): F[Unit]

  /**
   * Loads a 2-vector into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param values the source array for the attribute.
   */
  def vertexAttrib2fv(indx: Int, values: js.Array[Double]): F[Unit]

  /**
   * Loads a 3-vector into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param x  the first component.
   * @param y  the second component.
   * @param z  the third component.
   */
  def vertexAttrib3f(indx: Int, x: Double, y: Double, z: Double): F[Unit] 

  /**
   * Loads a 3-vector into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param values the source array for the attribute.
   */
  def vertexAttrib3fv(indx: Int, values: Float32Array): F[Unit]

  /**
   * Loads a 3-vector into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param values the source array for the attribute.
   */
  def vertexAttrib3fv(indx: Int, values: js.Array[Double]): F[Unit]

  /**
   * Loads a 4-vector into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param x  the first component.
   * @param y  the second component.
   * @param z  the third component.
   * @param w  the fourth component.
   */
  def vertexAttrib4f(indx: Int, x: Double, y: Double, z: Double, w: Double): F[Unit] 

  /**
   * Loads a 4-vector into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param values the source array for the attribute.
   */
  def vertexAttrib4fv(indx: Int, values: Float32Array): F[Unit]

  /**
   * Loads a 4-vector into a vertex attribute.
   *
   * @param indx the index of the attribute.
   * @param values the source array for the attribute.
   */
  def vertexAttrib4fv(indx: Int, values: js.Array[Double]): F[Unit]

  /**
   * Defines an array of generic vertex attribute data.
   *
   * @param indx the index of the attribute
   * @param size the number of components per attribute.  Must be 1..4
   * @param type the datatype for each component, may be [[WebGLRenderingContext.BYTE]],  [[WebGLRenderingContext.UNSIGNED_BYTE]],
   * 			  [[WebGLRenderingContext.SHORT]], [[WebGLRenderingContext.UNSIGNED_SHORT]], or
   *               [[WebGLRenderingContext.FLOAT]].
   * @param normalized if `true`, values are normalized on access, otherwise they are converted to fixed point values on access.
   * @param stride the gap between attributes.  0 would be packed together.
   * @param offset the offset to the first component in the array.
   */
  def vertexAttribPointer(indx: Int, size: Int, `type`: Int, normalized: Boolean, stride: Int, offset: Int): F[Unit] 

  /**
   * Sets the OpenGL viewport to render within.
   */
  def viewport(x: Double, y: Double, width: Double, height: Double): F[Unit] 

}

trait WebGL2ContextAlgebra[F[_]] extends WebGLContextAlgebra[F] {

    def createVertexArray: F[WebGLVertexArrayObject]
    def bindVertexArray(vertexArray: WebGLVertexArrayObject): F[Unit]
    def deleteVertexArray(vertexArray: WebGLVertexArrayObject): F[Unit]
    def isVertexArray(vertexArray: WebGLVertexArrayObject): F[Boolean]

}

class RealWebGLContext[F[_]: Sync](gl: WebGLRenderingContext) extends WebGLContextAlgebra[F] {

  def canvas: F[HTMLCanvasElement] = Sync[F].delay(gl.canvas)
  def drawingBufferWidth: F[Int] = Sync[F].delay(gl.drawingBufferWidth)
  def drawingBufferHeight: F[Int] = Sync[F].delay(gl.drawingBufferHeight)
  def getContextAttributes(): F[WebGLContextAttributes] = Sync[F].delay(gl.getContextAttributes())
  def isContextLost(): F[Boolean] = Sync[F].delay(gl.isContextLost())
  def getSupportedExtensions(): F[js.Array[String]] = Sync[F].delay(gl.getSupportedExtensions())
  def getExtension(name: String): F[js.Any] = Sync[F].delay(gl.getExtension(name))
  def activeTexture(texture: Int): F[Unit] = Sync[F].delay(gl.activeTexture(texture))
  def attachShader(program: WebGLProgram, shader: WebGLShader): F[Unit] = Sync[F].delay(gl.attachShader(program, shader))  
  def bindAttribLocation(program: WebGLProgram, index: Int, name: String): F[Unit] = Sync[F].delay(gl.bindAttribLocation(program, index, name))  
  def bindBuffer(target: Int, buffer: WebGLBuffer): F[Unit] = Sync[F].delay(gl.bindBuffer(target, buffer))
  def bindFramebuffer(target: Int, framebuffer: WebGLFramebuffer): F[Unit] = Sync[F].delay(gl.bindFramebuffer(target, framebuffer))  
  def bindRenderbuffer(target: Int, renderbuffer: WebGLRenderbuffer): F[Unit] = Sync[F].delay(gl.bindRenderbuffer(target, renderbuffer))  
  def bindTexture(target: Int, texture: WebGLTexture): F[Unit] = Sync[F].delay(gl.bindTexture(target, texture))
  def blendColor(red: Double, green: Double, blue: Double, alpha: Double): F[Unit] = Sync[F].delay(gl.blendColor(red, green, blue, alpha))  
  def blendEquation(mode: Int): F[Unit] = Sync[F].delay(gl.blendEquation(mode))
  def blendEquationSeparate(modeRGB: Int, modeAlpha: Int): F[Unit] = Sync[F].delay(gl.blendEquationSeparate(modeRGB, modeAlpha))
  def blendFunc(sfactor: Int, dfactor: Int): F[Unit] = Sync[F].delay(gl.blendFunc(sfactor, dfactor))
  def blendFuncSeparate(srcRGB: Int, dstRGB: Int, srcAlpha: Int, dstAlpha: Int): F[Unit] = Sync[F].delay(gl.blendFuncSeparate(srcRGB, dstRGB, srcAlpha, dstAlpha))  
  def bufferData(target: Int, size: Int, usage: Int): F[Unit] = Sync[F].delay(gl.bufferData(target, size, usage))
  def bufferData(target: Int, data: ArrayBufferView, usage: Int): F[Unit] = Sync[F].delay(gl.bufferData(target, data, usage))
  def bufferData(target: Int, data: ArrayBuffer, usage: Int): F[Unit] = Sync[F].delay(gl.bufferData(target, data, usage))
  def bufferSubData(target: Int, offset: Int, data: ArrayBufferView): F[Unit] = Sync[F].delay(gl.bufferSubData(target, offset, data))
  def bufferSubData(target: Int, offset: Int, data: ArrayBuffer): F[Unit] = Sync[F].delay(gl.bufferSubData(target, offset, data))
  def checkFramebufferStatus(target: Int): F[Int] = Sync[F].delay(gl.checkFramebufferStatus(target))
  def clear(mask: Int): F[Unit] = Sync[F].delay(gl.clear(mask))
  def clearColor(red: Double, green: Double, blue: Double, alpha: Double): F[Unit] = Sync[F].delay(gl.clearColor(red, green, blue, alpha))  
  def clearDepth(depth: Double): F[Unit] = Sync[F].delay(gl.clearDepth(depth))
  def colorMask(red: Boolean, green: Boolean, blue: Boolean, alpha: Boolean): F[Unit] = Sync[F].delay(gl.colorMask(red, green, blue, alpha))  
  def compileShader(shader: WebGLShader): F[Unit] = Sync[F].delay(gl.compileShader(shader))
  def compressedTexImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, data: ArrayBufferView): F[Unit] = Sync[F].delay(gl.compressedTexImage2D(target, level, internalformat, width, height, border, data))
  def compressedTexSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, width: Int, height: Int, format: Int, data: ArrayBufferView): F[Unit] = Sync[F].delay(gl.compressedTexSubImage2D(target, level, xoffset, yoffset, width, height, format, data))
  def copyTexImage2D(target: Int, level: Int, internalformat: Int, x: Int, y: Int, width: Int, height: Int, border: Int): F[Unit] = Sync[F].delay(gl.copyTexImage2D(target, level, internalformat, x, y, width, height, border))  
  def copyTexSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, x: Int, y: Int, width: Int, height: Int): F[Unit] = Sync[F].delay(gl.copyTexSubImage2D(target, level, xoffset, yoffset, x, y, width, height))  
  def createBuffer(): F[WebGLBuffer] = Sync[F].delay(gl.createBuffer())
  def createFramebuffer(): F[WebGLFramebuffer] = Sync[F].delay(gl.createFramebuffer())
  def createProgram(): F[WebGLProgram] = Sync[F].delay(gl.createProgram())
  def createRenderbuffer(): F[WebGLRenderbuffer] = Sync[F].delay(gl.createRenderbuffer())
  def createShader(`type`: Int): F[WebGLShader] = Sync[F].delay(gl.createShader(`type`: Int))
  def createTexture(): F[WebGLTexture] = Sync[F].delay(gl.createTexture())
  def cullFace(mode: Int): F[Unit] = Sync[F].delay(gl.cullFace(mode))
  def deleteBuffer(buffer: WebGLBuffer): F[Unit] = Sync[F].delay(gl.deleteBuffer(buffer))
  def deleteFramebuffer(framebuffer: WebGLFramebuffer): F[Unit] = Sync[F].delay(gl.deleteFramebuffer(framebuffer))
  def deleteProgram(program: WebGLProgram): F[Unit] = Sync[F].delay(gl.deleteProgram(program))
  def deleteRenderbuffer(renderbuffer: WebGLRenderbuffer): F[Unit] = Sync[F].delay(gl.deleteRenderbuffer(renderbuffer))
  def deleteShader(shader: WebGLShader): F[Unit] = Sync[F].delay(gl.deleteShader(shader))
  def deleteTexture(texture: WebGLTexture): F[Unit] = Sync[F].delay(gl.deleteTexture(texture))
  def depthFunc(func: Int): F[Unit] = Sync[F].delay(gl.depthFunc(func))
  def depthMask(flag: Boolean): F[Unit] = Sync[F].delay(gl.depthMask(flag))
  def depthRange(zNear: Double, zFar: Double): F[Unit] = Sync[F].delay(gl.depthRange(zNear, zFar))
  def detachShader(program: WebGLProgram, shader: WebGLShader): F[Unit] = Sync[F].delay(gl.detachShader(program, shader))  
  def disable(cap: Int): F[Unit] = Sync[F].delay(gl.disable(cap))
  def disableVertexAttribArray(index: Int): F[Unit] = Sync[F].delay(gl.disableVertexAttribArray(index))
  def drawArrays(mode: Int, first: Int, count: Int): F[Unit] = Sync[F].delay(gl.drawArrays(mode, first, count))
  def drawElements(mode: Int, count: Int, `type`: Int, offset: Int): F[Unit] = Sync[F].delay(gl.drawElements(mode, count, `type`: Int, offset))  
  def enable(cap: Int): F[Unit] = Sync[F].delay(gl.enable(cap))
  def enableVertexAttribArray(index: Int): F[Unit] = Sync[F].delay(gl.enableVertexAttribArray(index))
  def finish(): F[Unit] = Sync[F].delay(gl.finish())
  def flush(): F[Unit] = Sync[F].delay(gl.flush())
  def framebufferRenderbuffer(target: Int, attachment: Int, renderbuffertarget: Int, renderbuffer: WebGLRenderbuffer): F[Unit] = Sync[F].delay(gl.framebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer))
  def framebufferTexture2D(target: Int, attachment: Int, textarget: Int, texture: WebGLTexture, level: Int): F[Unit] = Sync[F].delay(gl.framebufferTexture2D(target, attachment, textarget, texture, level))  
  def frontFace(mode: Int): F[Unit] = Sync[F].delay(gl.frontFace(mode))
  def generateMipmap(target: Int): F[Unit] = Sync[F].delay(gl.generateMipmap(target))
  def getActiveAttrib(program: WebGLProgram, index: Int): F[WebGLActiveInfo] = Sync[F].delay(gl.getActiveAttrib(program, index))  
  def getActiveUniform(program: WebGLProgram, index: Int): F[WebGLActiveInfo] = Sync[F].delay(gl.getActiveUniform(program, index))  
  def getAttachedShaders(program: WebGLProgram): F[js.Array[WebGLShader]] = Sync[F].delay(gl.getAttachedShaders(program))
  def getAttribLocation(program: WebGLProgram, name: String): F[Int] = Sync[F].delay(gl.getAttribLocation(program, name))
  def getBufferParameter(target: Int, pname: Int): F[Int] = Sync[F].delay(gl.getBufferParameter(target, pname))
  def getParameter(pname: Int): F[js.Any] = Sync[F].delay(gl.getParameter(pname))
  def getError(): F[Int] = Sync[F].delay(gl.getError())
  def getFramebufferAttachmentParameter(target: Int, attachment: Int, pname: Int): F[js.Any] = Sync[F].delay(gl.getFramebufferAttachmentParameter(target, attachment, pname))  
  def getProgramParameter(program: WebGLProgram, pname: Int): F[js.Any] = Sync[F].delay(gl.getProgramParameter(program, pname))  
  def getProgramInfoLog(program: WebGLProgram): F[String] = Sync[F].delay(gl.getProgramInfoLog(program))
  def getRenderbufferParameter(target: Int, pname: Int): F[js.Any] = Sync[F].delay(gl.getRenderbufferParameter(target, pname))
  def getShaderParameter(shader: WebGLShader, pname: Int): F[js.Any] = Sync[F].delay(gl.getShaderParameter(shader, pname))
  def getShaderPrecisionFormat(shadertype: Int, precisiontype: Int): F[WebGLShaderPrecisionFormat] = Sync[F].delay(gl.getShaderPrecisionFormat(shadertype, precisiontype))  
  def getShaderInfoLog(shader: WebGLShader): F[String] = Sync[F].delay(gl.getShaderInfoLog(shader))
  def getShaderSource(shader: WebGLShader): F[String] = Sync[F].delay(gl.getShaderSource(shader))
  def getTexParameter(target: Int, pname: Int): F[js.Any] = Sync[F].delay(gl.getTexParameter(target, pname))
  def getUniform(program: WebGLProgram, location: WebGLUniformLocation): F[js.Any] = Sync[F].delay(gl.getUniform(program, location))  
  def getUniformLocation(program: WebGLProgram, name: String): F[WebGLUniformLocation] = Sync[F].delay(gl.getUniformLocation(program, name))  
  def getVertexAttrib(index: Int, pname: Int): F[js.Any] = Sync[F].delay(gl.getVertexAttrib(index, pname))
  def getVertexAttribOffset(index: Int, pname: Int): F[Int] = Sync[F].delay(gl.getVertexAttribOffset(index, pname))
  def hint(target: Int, mode: Int): F[Unit] = Sync[F].delay(gl.hint(target, mode))
  def isBuffer(buffer: js.Any): F[Boolean] = Sync[F].delay(gl.isBuffer(buffer))
  def isEnabled(cap: Int): F[Boolean] = Sync[F].delay(gl.isEnabled(cap))
  def isFramebuffer(framebuffer: js.Any): F[Boolean] = Sync[F].delay(gl.isFramebuffer(framebuffer))
  def isProgram(program: js.Any): F[Boolean] = Sync[F].delay(gl.isProgram(program))
  def isRenderbuffer(renderbuffer: js.Any): F[Boolean] = Sync[F].delay(gl.isRenderbuffer(renderbuffer))
  def isShader(shader: js.Any): F[Boolean] = Sync[F].delay(gl.isShader(shader))
  def isTexture(texture: js.Any): F[Boolean] = Sync[F].delay(gl.isTexture(texture))
  def lineWidth(width: Double): F[Unit] = Sync[F].delay(gl.lineWidth(width))
  def linkProgram(program: WebGLProgram): F[Unit] = Sync[F].delay(gl.linkProgram(program))
  def pixelStorei(pname: Int, param: Int): F[Unit] = Sync[F].delay(gl.pixelStorei(pname, param))
  def polygonOffset(factor: Double, units: Double): F[Unit] = Sync[F].delay(gl.polygonOffset(factor, units))
  def readPixels(x: Int, y: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: ArrayBufferView): F[Unit] = Sync[F].delay(gl.readPixels(x, y, width, height, format, `type`: Int, pixels))  
  def renderbufferStorage(target: Int, internalformat: Int, width: Int, height: Int): F[Unit] = Sync[F].delay(gl.renderbufferStorage(target, internalformat, width, height))  
  def sampleCoverage(value: Int, invert: Boolean): F[Unit] = Sync[F].delay(gl.sampleCoverage(value, invert))
  def scissor(x: Int, y: Int, width: Int, height: Int): F[Unit] = Sync[F].delay(gl.scissor(x, y, width, height))
  def shaderSource(shader: WebGLShader, source: String): F[Unit] = Sync[F].delay(gl.shaderSource(shader, source))
  def stencilFunc(func: Int, ref: Int, mask: Int): F[Unit] = Sync[F].delay(gl.stencilFunc(func, ref, mask))
  def stencilFuncSeparate(face: Int, func: Int, ref: Int, mask: Int): F[Unit] = Sync[F].delay(gl.stencilFuncSeparate(face, func, ref, mask))  
  def stencilMask(mask: Int): F[Unit] = Sync[F].delay(gl.stencilMask(mask))
  def stencilMaskSeparate(face: Int, mask: Int): F[Unit] = Sync[F].delay(gl.stencilMaskSeparate(face, mask))
  def stencilOp(fail: Int, zfail: Int, zpass: Int): F[Unit] = Sync[F].delay(gl.stencilOp(fail, zfail, zpass))
  def stencilOpSeparate(face: Int, fail: Int, zfail: Int, zpass: Int): F[Unit] = Sync[F].delay(gl.stencilOpSeparate(face, fail, zfail, zpass))  
  def texImage2D(target: Int, level: Int, internalformat: Int, width: Int, height: Int, border: Int, format: Int, `type`: Int, pixels: ArrayBufferView): F[Unit] = Sync[F].delay(gl.texImage2D(target, level, internalformat, width, height, border, format, `type`: Int, pixels))
  def texImage2D(target: Int, level: Int, internalformat: Int, format: Int, `type`: Int, pixels: ImageData): F[Unit] = Sync[F].delay(gl.texImage2D(target, level, internalformat, format, `type`: Int, pixels))  
  def texImage2D(target: Int, level: Int, internalformat: Int, format: Int, `type`: Int, pixels: HTMLImageElement): F[Unit] = Sync[F].delay(gl.texImage2D(target, level, internalformat, format, `type`: Int, pixels))  
  def texImage2D(target: Int, level: Int, internalformat: Int, format: Int, `type`: Int, pixels: HTMLCanvasElement): F[Unit] = Sync[F].delay(gl.texImage2D(target, level, internalformat, format, `type`: Int, pixels))  
  def texImage2D(target: Int, level: Int, internalformat: Int, format: Int, `type`: Int, pixels: HTMLVideoElement): F[Unit] = Sync[F].delay(gl.texImage2D(target, level, internalformat, format, `type`: Int, pixels))
  def texParameteri(target: Int, pname: Int, param: Int): F[Unit] = Sync[F].delay(gl.texParameteri(target, pname, param))
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, width: Int, height: Int, format: Int, `type`: Int, pixels: ArrayBufferView): F[Unit] = Sync[F].delay(gl.texSubImage2D(target, level, xoffset, yoffset, width, height, format, `type`: Int, pixels))
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, `type`: Int, pixels: ImageData): F[Unit] = Sync[F].delay(gl.texSubImage2D(target, level, xoffset, yoffset, format, `type`: Int, pixels))  
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, `type`: Int, pixels: HTMLImageElement): F[Unit] = Sync[F].delay(gl.texSubImage2D(target, level, xoffset, yoffset, format, `type`: Int, pixels))  
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, `type`: Int, pixels: HTMLCanvasElement): F[Unit] = Sync[F].delay(gl.texSubImage2D(target, level, xoffset, yoffset, format, `type`: Int, pixels))  
  def texSubImage2D(target: Int, level: Int, xoffset: Int, yoffset: Int, format: Int, `type`: Int, pixels: HTMLVideoElement): F[Unit] = Sync[F].delay(gl.texSubImage2D(target, level, xoffset, yoffset, format, `type`: Int, pixels))  
  def uniform1f(location: WebGLUniformLocation, x: Double): F[Unit] = Sync[F].delay(gl.uniform1f(location, x))
  def uniform1fv(location: WebGLUniformLocation, v: Float32Array): F[Unit] = Sync[F].delay(gl.uniform1fv(location, v))  
  def uniform1fv(location: WebGLUniformLocation, v: js.Array[Double]): F[Unit] = Sync[F].delay(gl.uniform1fv(location, v))  
  def uniform1i(location: WebGLUniformLocation, x: Int): F[Unit] = Sync[F].delay(gl.uniform1i(location, x))
  def uniform1iv(location: WebGLUniformLocation, v: Int32Array): F[Unit] = Sync[F].delay(gl.uniform1iv(location, v))  
  def uniform1iv(location: WebGLUniformLocation, v: js.Array[Int]): F[Unit] = Sync[F].delay(gl.uniform1iv(location, v))  
  def uniform2f(location: WebGLUniformLocation, x: Double, y: Double): F[Unit] = Sync[F].delay(gl.uniform2f(location, x, y))  
  def uniform2fv(location: WebGLUniformLocation, v: Float32Array): F[Unit] = Sync[F].delay(gl.uniform2fv(location, v))  
  def uniform2fv(location: WebGLUniformLocation, v: js.Array[Double]): F[Unit] = Sync[F].delay(gl.uniform2fv(location, v))  
  def uniform2i(location: WebGLUniformLocation, x: Int, y: Int): F[Unit] = Sync[F].delay(gl.uniform2i(location, x, y))  
  def uniform2iv(location: WebGLUniformLocation, v: Int32Array): F[Unit] = Sync[F].delay(gl.uniform2iv(location, v))  
  def uniform2iv(location: WebGLUniformLocation, v: js.Array[Int]): F[Unit] = Sync[F].delay(gl.uniform2iv(location, v))  
  def uniform3f(location: WebGLUniformLocation, x: Double, y: Double, z: Double): F[Unit] = Sync[F].delay(gl.uniform3f(location, x, y, z))  
  def uniform3fv(location: WebGLUniformLocation, v: Float32Array): F[Unit] = Sync[F].delay(gl.uniform3fv(location, v))  
  def uniform3fv(location: WebGLUniformLocation, v: js.Array[Double]): F[Unit] = Sync[F].delay(gl.uniform3fv(location, v))  
  def uniform3i(location: WebGLUniformLocation, x: Int, y: Int, z: Int): F[Unit] = Sync[F].delay(gl.uniform3i(location, x, y, z))  
  def uniform3iv(location: WebGLUniformLocation, v: Int32Array): F[Unit] = Sync[F].delay(gl.uniform3iv(location, v))  
  def uniform3iv(location: WebGLUniformLocation, v: js.Array[Int]): F[Unit] = Sync[F].delay(gl.uniform3iv(location, v))  
  def uniform4f(location: WebGLUniformLocation, x: Double, y: Double, z: Double, w: Double): F[Unit] = Sync[F].delay(gl.uniform4f(location, x, y, z, w))  
  def uniform4fv(location: WebGLUniformLocation, v: Float32Array): F[Unit] = Sync[F].delay(gl.uniform4fv(location, v))  
  def uniform4fv(location: WebGLUniformLocation, v: js.Array[Double]): F[Unit] = Sync[F].delay(gl.uniform4fv(location, v))  
  def uniform4i(location: WebGLUniformLocation, x: Int, y: Int, z: Int, w: Int): F[Unit] = Sync[F].delay(gl.uniform4i(location, x, y, z, w))  
  def uniform4iv(location: WebGLUniformLocation, v: Int32Array): F[Unit] = Sync[F].delay(gl.uniform4iv(location, v))  
  def uniform4iv(location: WebGLUniformLocation, v: js.Array[Int]): F[Unit] = Sync[F].delay(gl.uniform4iv(location, v))  
  def uniformMatrix2fv(location: WebGLUniformLocation, transpose: Boolean, value: Float32Array): F[Unit] = Sync[F].delay(gl.uniformMatrix2fv(location, transpose, value))  
  def uniformMatrix2fv(location: WebGLUniformLocation, transpose: Boolean, value: js.Array[Double]): F[Unit] = Sync[F].delay(gl.uniformMatrix2fv(location, transpose, value))  
  def uniformMatrix3fv(location: WebGLUniformLocation, transpose: Boolean, value: Float32Array): F[Unit] = Sync[F].delay(gl.uniformMatrix3fv(location, transpose, value))  
  def uniformMatrix3fv(location: WebGLUniformLocation, transpose: Boolean, value: js.Array[Double]): F[Unit] = Sync[F].delay(gl.uniformMatrix3fv(location, transpose, value))  
  def uniformMatrix4fv(location: WebGLUniformLocation, transpose: Boolean, value: Float32Array): F[Unit] = Sync[F].delay(gl.uniformMatrix4fv(location, transpose, value))  
  def uniformMatrix4fv(location: WebGLUniformLocation, transpose: Boolean, value: js.Array[Double]): F[Unit] = Sync[F].delay(gl.uniformMatrix4fv(location, transpose, value))  
  def useProgram(program: WebGLProgram): F[Unit] = Sync[F].delay(gl.useProgram(program))
  def validateProgram(program: WebGLProgram): F[Unit] = Sync[F].delay(gl.validateProgram(program))
  def vertexAttrib1f(indx: Int, x: Double): F[Unit] = Sync[F].delay(gl.vertexAttrib1f(indx, x))
  def vertexAttrib1fv(indx: Int, values: Float32Array): F[Unit] = Sync[F].delay(gl.vertexAttrib1fv(indx, values))
  def vertexAttrib1fv(indx: Int, values: js.Array[Double]): F[Unit] = Sync[F].delay(gl.vertexAttrib1fv(indx, values))
  def vertexAttrib2f(indx: Int, x: Double, y: Double): F[Unit] = Sync[F].delay(gl.vertexAttrib2f(indx, x, y))
  def vertexAttrib2fv(indx: Int, values: Float32Array): F[Unit] = Sync[F].delay(gl.vertexAttrib2fv(indx, values))
  def vertexAttrib2fv(indx: Int, values: js.Array[Double]): F[Unit] = Sync[F].delay(gl.vertexAttrib2fv(indx, values))
  def vertexAttrib3f(indx: Int, x: Double, y: Double, z: Double): F[Unit] = Sync[F].delay(gl.vertexAttrib3f(indx, x, y, z))  
  def vertexAttrib3fv(indx: Int, values: Float32Array): F[Unit] = Sync[F].delay(gl.vertexAttrib3fv(indx, values))
  def vertexAttrib3fv(indx: Int, values: js.Array[Double]): F[Unit] = Sync[F].delay(gl.vertexAttrib3fv(indx, values))
  def vertexAttrib4f(indx: Int, x: Double, y: Double, z: Double, w: Double): F[Unit] = Sync[F].delay(gl.vertexAttrib4f(indx, x, y, z, w))  
  def vertexAttrib4fv(indx: Int, values: Float32Array): F[Unit] = Sync[F].delay(gl.vertexAttrib4fv(indx, values))
  def vertexAttrib4fv(indx: Int, values: js.Array[Double]): F[Unit] = Sync[F].delay(gl.vertexAttrib4fv(indx, values))
  def vertexAttribPointer(indx: Int, size: Int, `type`: Int, normalized: Boolean, stride: Int, offset: Int): F[Unit] = Sync[F].delay(gl.vertexAttribPointer(indx, size, `type`: Int, normalized, stride, offset))  
  def viewport(x: Double, y: Double, width: Double, height: Double): F[Unit] = Sync[F].delay(gl.viewport(x, y, width, height))
  def clearStencil(s: Int): F[Unit] = Sync[F].delay(gl.clearStencil(s))
  def texParameterf(target: Int, pname: Int, param: Double): F[Unit] = Sync[F].delay(gl.texParameterf(target, pname, param))

}

class RealWebGL2Context[F[_]: Sync](gl: WebGL2RenderingContext) extends RealWebGLContext(gl) with WebGL2ContextAlgebra[F] {
    def createVertexArray: F[WebGLVertexArrayObject] = Sync[F].delay(gl.createVertexArray())
    def bindVertexArray(vertexArray: WebGLVertexArrayObject): F[Unit] = Sync[F].delay(gl.bindVertexArray(vertexArray))
    def deleteVertexArray(vertexArray: WebGLVertexArrayObject): F[Unit] = Sync[F].delay(gl.deleteVertexArray(vertexArray))
    def isVertexArray(vertexArray: WebGLVertexArrayObject): F[Boolean] = Sync[F].delay(gl.isVertexArray(vertexArray))
}
