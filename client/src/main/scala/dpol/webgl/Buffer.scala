package dpol.webgl

import cats.effect.Resource
import cats.Applicative
import org.scalajs.dom.raw.WebGLBuffer

object Buffer {
  def apply[F[_]: Applicative](implicit gl: WebGLContextAlgebra[F]): Resource[F, WebGLBuffer] = Resource.make(gl.createBuffer())(gl.deleteBuffer)
}

// object BufferSyntax {
//   implicit class BufferOps(b: WebGLBuffer) {
//     def bufferData[F[_]](implicit gl: WebGLContextAlgebra[F]) 
//   }
// }