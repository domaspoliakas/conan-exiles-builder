package dpol.webgl

import cats.effect._
import cats.syntax.flatMap._
import cats.syntax.functor._

import dpol.webgl.WebGL2ContextAlgebra
import dpol.webgl.RealWebGL2Context
import org.scalajs.dom
import org.scalajs.dom.raw.HTMLCanvasElement
import dpol.webgl.WebGL2RenderingContext
import org.scalajs.dom.raw.WebGLShader

object WebGL {
  def initialiseWebGL2[F[_]: Sync](canvas: HTMLCanvasElement): F[WebGL2ContextAlgebra[F]] =
    Sync[F]
      .delay {
        canvas.width = canvas.clientWidth
        canvas.height = canvas.clientHeight
        canvas
      }.flatMap(canvas => {
        Option(canvas.getContext("webgl2"))
          .map(_.asInstanceOf[WebGL2RenderingContext])
          .map(Sync[F].pure)
          .getOrElse(Sync[F].raiseError(new Exception("The browser does not support WebGL2")))
      })
      .map(context => new RealWebGL2Context(context))
}
