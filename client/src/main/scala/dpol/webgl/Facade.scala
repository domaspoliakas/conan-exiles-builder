package dpol. webgl

import scalajs.js
import org.scalajs.dom.raw.WebGLRenderingContext
import scala.scalajs.js.annotation.JSGlobal

@js.native
trait WebGL2RenderingContext extends WebGLRenderingContext {

    // Vertex Arrays
    def createVertexArray(): WebGLVertexArrayObject = js.native
    def bindVertexArray(vertexArray: WebGLVertexArrayObject): Unit = js.native
    def deleteVertexArray(vertexArray: WebGLVertexArrayObject): Unit = js.native
    def isVertexArray(vertexArray: WebGLVertexArrayObject): Boolean = js.native

}

// trait WebGL2RenderingContextConstants {
    // val TRANSFORM_FEEDBACK_BUFFER_MODE = ???
// }
// 
// object WebGL2RenderingContext extends WebGL2RenderingContextConstants

@js.native
@JSGlobal
class WebGLVertexArrayObject  extends js.Any 
