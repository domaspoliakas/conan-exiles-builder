package dpol.webgl

import org.scalajs.dom.raw.WebGLProgram
import org.scalajs.dom.raw.WebGLRenderingContext
import cats.MonadError
import cats.effect.Resource
import cats.syntax.flatMap._
import cats.syntax.functor._
import org.scalajs.dom.raw.WebGLShader

object Program {

  private def checkProgramLinkingStatus[F[_]: MonadErrorT](
      program: WebGLProgram
  )(implicit gl: WebGLContextAlgebra[F]): F[Unit] =
    gl.getProgramParameter(program, WebGLRenderingContext.LINK_STATUS)
      .flatMap(d =>
        (d: Any) match {
          case true =>
            MonadError[F, Throwable].unit
          case _ =>
            gl.getProgramInfoLog(program)
              .flatMap(l =>
                MonadError[F, Throwable].raiseError(new Exception(s"Could not link program: $l"))
              )
        }
      )

  def apply[F[_]: MonadErrorT](
      vertexShader: WebGLShader,
      fragmentShader: WebGLShader
  )(implicit gl: WebGLContextAlgebra[F]): Resource[F, WebGLProgram] =
    Resource.make(for {
      program <- gl.createProgram
      _       <- gl.attachShader(program, vertexShader)
      _       <- gl.attachShader(program, fragmentShader)
      _       <- gl.linkProgram(program)
      _       <- checkProgramLinkingStatus(program)
    } yield program)(gl.deleteProgram)

}