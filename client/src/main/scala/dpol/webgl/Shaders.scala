package dpol.webgl

import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.effect.Resource
import org.scalajs.dom.raw.WebGLRenderingContext
import org.scalajs.dom.raw.WebGLShader
import cats.MonadError

object VertexShader extends Shaderer {

  def apply[F[_]: MonadErrorT](
      shaderSource: String
  )(implicit gl: WebGL2ContextAlgebra[F]): Resource[F, WebGLShader] =
    newShaderResource(shaderSource, WebGLRenderingContext.VERTEX_SHADER)
}

object FragmentShader extends Shaderer {

  def apply[F[_]: MonadErrorT, U](
      shaderSource: String
  )(implicit gl: WebGL2ContextAlgebra[F]): Resource[F, WebGLShader] =
    newShaderResource(shaderSource, WebGLRenderingContext.FRAGMENT_SHADER)
}

sealed trait Shaderer {

  def newShaderResource[F[_]: MonadErrorT](
      shaderSource: String,
      shaderType: Int
  )(implicit gl: WebGL2ContextAlgebra[F]): Resource[F, WebGLShader] = {
    Resource.make(
      for {
        shader <- gl.createShader(shaderType)
        _      <- gl.shaderSource(shader, shaderSource)
        _      <- gl.compileShader(shader)
        _      <- checkShaderCompilationStatus(shader)
      } yield shader
    )(gl.deleteShader)
  }

  private[webgl] def checkShaderCompilationStatus[F[_]: MonadErrorT](
      shader: WebGLShader
  )(implicit gl: WebGL2ContextAlgebra[F]): F[Unit] =
    gl.getShaderParameter(shader, WebGLRenderingContext.COMPILE_STATUS)
      .flatMap(a =>
        (a: Any) match {
          case true => MonadError[F, Throwable].unit
          case _ =>
            gl.getShaderInfoLog(shader)
              .flatMap(l =>
                MonadError[F, Throwable]
                  .raiseError(new Exception(s"Could not compile shader: $l"))
              )
        }
      )

}
