package dpol.webgl

import cats.effect.Resource
import cats.Functor

object VertexArrayObject {
  def apply[F[_]: Functor ](implicit gl: WebGL2ContextAlgebra[F]): Resource[F, WebGLVertexArrayObject] = {
    Resource.make(gl.createVertexArray)(gl.deleteVertexArray)
  }
}