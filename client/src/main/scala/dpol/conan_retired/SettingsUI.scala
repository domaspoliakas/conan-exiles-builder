package dpol.conan_retired

import fs2._
import fs2.concurrent._
import cats.implicits._
import cats.effect._
import org.scalajs.dom.raw.Element
import io.circe._, io.circe.generic.auto._, io.circe.generic.semiauto._
import cats.effect.concurrent.Ref
import scala.concurrent.duration._
import monocle.Lens
import monocle.macros.Lenses
import monocle.macros.GenLens

object SettingsUI {

  val defaults = Settings(
    60,
    RenderSettings(90, 1, 100000),
    270,
    5
  )

  case class Settings(fps: Int, renderSettings: RenderSettings, turnSpeed: Int, movSpeed: Int)
  object Settings {
    val renderSettings: Lens[Settings, RenderSettings] = GenLens[Settings](_.renderSettings)
  }

  case class RenderSettings(fovInDegrees: Int, zNear: Int, zFar: Int) {
    lazy val fovInRads = Math.toRadians(fovInDegrees)
  }

  object RenderSettings {
    val fovInDegrees: Lens[RenderSettings, Int] = GenLens[RenderSettings](_.fovInDegrees)
    val zNear: Lens[RenderSettings, Int] = GenLens[RenderSettings](_.zNear)
    val zFar: Lens[RenderSettings, Int] = GenLens[RenderSettings](_.zFar)
  }

  def apply[F[_]: ConcurrentEffect: Timer](
      storage: Storage[F],
      parent: Element
  ): Stream[F, Settings] = {

    def settingsPotoot[A](
        p: Pipe[F, A, A],
        f: Settings => A,
        g: (Settings, A) => Settings,
        ref: Ref[F, Settings]
    ): Pipe[F, Settings, Unit] = s => {
      s.map(f).through(p).evalMap(newValue => ref.update(s => g(s, newValue)))
    }

    def settingsTomoot[A](
        p: Pipe[F, A, A],
        ref: Ref[F, Settings],
        l: Lens[Settings, A]
    ): Pipe[F, Settings, Unit] = s => {
      s.map(l.get).through(p).evalMap(newValue => ref.update(l.set(newValue)))
    }

    for {
      startingValue <- Stream.eval(storage.get[Option[Settings]]("settings"))
      ref           <- Stream.eval(SignallingRef(startingValue.getOrElse(defaults)))

      s <- ref.discrete.concurrently(
        ref.discrete.broadcastThrough(
          settingsTomoot[Int]( UI.reactive(buildFovSlider(parent)), ref, Settings.renderSettings ^|-> RenderSettings.fovInDegrees),
          settingsPotoot[Int]( UI.reactive(buildFpsSlider(parent)), _.fps, (s, n) => s.copy(fps = n), ref),
          settingsPotoot[Int]( UI.reactive(buildSlider(parent, 100, 1000, "turn_speed_slider", "Turn Speed")), _.turnSpeed, (s, n) => s.copy(turnSpeed = n), ref),
          settingsPotoot[Int]( UI.reactive(buildSlider(parent, 1, 100, "movement_speed_slider", "Movement Speed")), _.movSpeed, (s, n) => s.copy(movSpeed = n), ref),
          settingsTomoot[Int]( UI.reactive(buildInput(parent, "zNear", "zNear - Clipping Plane Near Distance")), ref, Settings.renderSettings ^|-> RenderSettings.zNear),
          settingsTomoot[Int]( UI.reactive(buildInput(parent, "zFar", "zFar - Clipping Plane Far Distance")), ref, Settings.renderSettings ^|-> RenderSettings.zFar),
          (s: Stream[F, Settings]) => s.debounce(5.seconds).evalMap(storage.set("settings", _))
        )
      )
    } yield s
  }

  import scalatags.JsDom._
  import scalatags.JsDom.all._

  def buildFovSlider[F[_]](
      parent: Element
  )(implicit F: ConcurrentEffect[F]): (Int => Unit) => Resource[F, Int => F[Unit]] =
    buildSlider(parent, 30, 120, "fov_slider", "FOV")

  def buildFpsSlider[F[_]](
      parent: Element
  )(implicit F: ConcurrentEffect[F]): (Int => Unit) => Resource[F, Int => F[Unit]] =
    buildSlider(parent, 0, 120, "fps_slider", "FPS")

  def buildInput[F[_]](
      parent: Element,
      idString: String,
      labelString: String
  )(implicit F: ConcurrentEffect[F]): (Int => Unit) => Resource[F, Int => F[Unit]] = writer => {

    for {
      input <- Resource
        .liftF(F.delay {
          input(
            `type` := "number",
            id := idString
          ).render
        })
        .map(i => {
          i.oninput = e => writer(i.valueAsNumber.toInt)
          i
        })
      div <- Resource.liftF(F.delay {
        div(
          input,
          label(`for` := idString, labelString)
        ).render
      })
      _ <- Resource.make[F, Unit](
        F.delay(parent.appendChild(div)).void
      )(_ => F.delay(parent.removeChild(div)))
    } yield (i: Int) =>
      F.delay {
        input.value = i.toString()
      }

  }

  def buildSlider[F[_]](
      parent: Element,
      minV: Int,
      maxV: Int,
      idString: String,
      labelString: String
  )(
      implicit F: ConcurrentEffect[F]
  ): (Int => Unit) => Resource[F, Int => F[Unit]] = writer => {

    for {
      input <- Resource
        .liftF(F.delay {
          input(
            `type` := "range",
            min := minV,
            max := maxV,
            id := idString
          ).render
        })
        .map(i => {
          i.oninput = e => writer(i.valueAsNumber.toInt)
          i
        })
      spanner <- Resource.liftF(F.delay { span(style := "width: 15px").render })
      div <- Resource.liftF(F.delay {
        div(
          spanner,
          input,
          label(`for` := idString, labelString)
        ).render
      })
      _ <- Resource.make[F, Unit](
        F.delay(parent.appendChild(div)).void
      )(_ => F.delay(parent.removeChild(div)))
    } yield (i: Int) =>
      F.delay {
        input.value = i.toString()
        spanner.textContent = i.toString()
      }
  }
}
