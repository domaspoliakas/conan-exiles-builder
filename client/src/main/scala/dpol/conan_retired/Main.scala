package dpol.conan_retired

// import dpol.webgl.{WebGL2RenderingContext => WebGL2}
// import dpol.webgl.AttributeLocator
// import org.scalajs.dom
// import org.scalajs.dom.raw.{WebGLRenderingContext => WebGL, HTMLCanvasElement}
// import scala.scalajs.js.typedarray.Float32Array
// import scala.scalajs.js.typedarray.ArrayBuffer
// import scala.scalajs.js.typedarray.ArrayBufferView
// import scala.scalajs.js
// import scala.scalajs.js.Date
// import scala.util.Random
// import monix.reactive.Observable
// import cats._
// import cats.effect._
// import cats.data._
// import cats.implicits._
// import scala.concurrent.duration._
// import scala.concurrent.ExecutionContext
// import cats.effect.IOApp
// import cats.effect.ExitCode
// import org.scalajs.dom.raw.WebGLShader
// import org.scalajs.dom.raw.WebGLProgram
// import org.scalajs.dom.html
// import java.{util => ju}
// import org.scalajs.dom.raw.WebGLBuffer
// import dpol.webgl.WebGL2RenderingContext
// import org.scalajs.dom.raw.WebGLUniformLocation
// import dpol.webgl.WebGLVertexArrayObject
// import dpol.webgl.RealWebGL2Context
// import dpol.webgl.WebGLContextAlgebra
// import dpol.webgl.WebGL2ContextAlgebra
// import dpol.webgl.VertexShader
// import dpol.webgl.Attribute
// import dpol.webgl.Uniform
// import dpol.webgl.FragmentShader
// import dpol.webgl.Program
// import dpol.conan.Shaders.VertexShaderAttrs
// import dpol.conan.Shaders.VertexShaderUniforms
// import dpol.webgl.VertexArrayObject

// object Main extends IOApp {
object Main {

  // Legacy main, that contains various code that was involved in me figuring out
  // how webGL works.
  // Keeping around for now in  case I want to reference it in the future

  // // import Bufferable._
  // // import Uniform._

  // type Point = (Double, Double)
  // type Line  = (Point, Point)

  // def run(args: List[String]): IO[ExitCode] =
  //   for {
  //     gl <- initialiseWebGL2[IO]
  //     glAlgebra = new RealWebGL2Context[IO](gl)
  //     // glF = new RealWebGL2(glAlgebra)
  //     drawer2 = initialiseProgram2[IO](glAlgebra)
  //     _ <- drawer2.use(d => d((0d, 0d) -> (0d, 0d)))
  //     // drawerResource = initialiseProgramF[IO](glF, glAlgebra)
  //     // _ <- drawerResource.use(d => drawSomeLinesForAWhile(d, 1, None))
  //     // _ <- drawerResource.use(d => d((0d, 0d) -> (0d, 0d)))
  //   } yield ExitCode.Success

  // def drawSomeLinesForAWhile[F[_]: Sync: Timer](
  //     drawer: Line => F[Unit],
  //     drawnSoFar: Int,
  //     original: Option[F[Unit]]
  // ): F[Unit] = {
  //   val a = (1 to drawnSoFar)
  //     .map(i => {
  //       val ii = (i.toDouble / 100)
  //       drawer((ii, ii) -> (-ii, -ii))
  //     })
  //     .toVector
  //     .sequence

  //   if (drawnSoFar == 100) {
  //     original.getOrElse(Sync[F].unit)
  //   } else
  //     for {
  //       _ <- a
  //       _ <- Timer[F].sleep(100.millis)
  //       _ <- drawSomeLinesForAWhile(
  //         drawer,
  //         drawnSoFar + 1,
  //         if (original.isDefined) original else Some(a.map(_ => ()))
  //       )
  //     } yield ()
  // }


  // def initialiseProgram2[F[_]: Sync](
  //     gl: WebGL2ContextAlgebra[F]
  // ): Resource[F, Line => F[Unit]] = {

  //   implicit val igl: WebGL2ContextAlgebra[F] = gl;
  //   implicit val a: AttributeLocator[F, VertexShaderAttrs] =
  //     Functor[({ type f[X] = AttributeLocator[F, X] })#f]
  //       .map(AttributeLocator.vec4[F]("a_position"))(VertexShaderAttrs.apply)

  //     (
  //       for {
  //         vertexShader <- VertexShader.from[F, VertexShaderAttrs, VertexShaderUniforms](
  //           Shaders.vertexShader
  //         )
  //         fragmentShader <- FragmentShader.from[F, Unit](Shaders.fragmentShader)
  //         program <- Program.from[F, VertexShaderAttrs, VertexShaderUniforms, Unit](
  //           vertexShader,
  //           fragmentShader
  //         )
  //         line = new Float32Array(
  //           js.Array[Double](
  //             -0.5, -0.5, 0, -0.5, 0, 0, -0.5, 0, 0, 0, 0, 0, 0, 0, 0, -0.5, -0.5, 0
  //           )
  //         )
  //         buffer <- Resource.make(gl.createBuffer())(gl.deleteBuffer)
  //         vao <- VertexArrayObject.from[F, VertexShaderAttrs, VertexShaderUniforms with Unit](
  //           program,
  //           _.a_position.bindToBuffer[F](buffer, 3, WebGL.FLOAT, false, 0, 0),
  //           (_, gl) => Sync[F].delay(gl.drawArrays(WebGL.LINES, 0, 6))
  //         )
  //       } yield (l: Line) => vao.render(new VertexShaderUniforms {
  //         val translationx: Uniform[Double]
  //         val translationy: Uniform[Double]
  //       }))
  //     ).evalMap(l => Sync[F].delay(println("Hello")).map(_ => l))
  // }
  // def initialiseProgramF[F[_]: Sync](gl: WebGL2F[F], gl2: WebGL2ContextAlgebra[F]): Resource[F, Line => F[Unit]] = {
  //     (for {
  //         vao1 <- gl.newVertexArrayObject
  //         vao2 <- gl.newVertexArrayObject
  //         positionBuffer1 <- gl.newBufferResource
  //         positionBuffer2 <- gl.newBufferResource
  //         vertexShader <- gl.newVertexShaderResource(Shaders.vertexShader)
  //         fragmentShader <- gl.newFragmentShaderResource(Shaders.fragmentShader)
  //         program <- gl.newProgramResource(vertexShader, fragmentShader)
  //     } yield (program, positionBuffer1, positionBuffer2, vao1, vao2)).evalMap { case (prog, pos1, pos2, vao1, vao2) =>
  //         for {
  //             positionLoc <- gl.getAttributeLocation[(Double, Double, Double)](prog, "a_position")
  //             translationxUniform <- gl.getUniformLocation[Double](prog, "translationx")
  //             translationyUniform <- gl.getUniformLocation[Double](prog, "translationy")

  //             _ <- gl2.bindVertexArray(vao1)
  //             _ <- gl2.enableVertexAttribArray(positionLoc.location)
  //             _ <- gl2.bindBuffer(WebGL.ARRAY_BUFFER, pos1)
  //             _ <- gl2.bufferData(WebGL.ARRAY_BUFFER, new Float32Array(js.Array[Double](
  //                 -0.5, -0.5, 0,
  //                 -0.5, 0, 0,
  //                 -0.5, 0, 0,
  //                 0, 0, 0,
  //                 0, 0, 0,
  //                 -0.5, -0.5, 0
  //             )), WebGL.STATIC_DRAW)
  //             _ <- gl2.vertexAttribPointer(positionLoc.location, 3, WebGL.FLOAT, false, 0, 0)
  //             _ <- gl2.bindVertexArray(vao2)
  //             _ <- gl2.enableVertexAttribArray(positionLoc.location)
  //             _ <- gl2.bindBuffer(WebGL.ARRAY_BUFFER, pos2)
  //             _ <- gl2.bufferData(WebGL.ARRAY_BUFFER, new Float32Array(js.Array(
  //                 0.5, 0.5, 0,
  //                 0.5, 0, 0,
  //                 0.5, 0, 0,
  //                 0, 0, 0,
  //                 0, 0, 0,
  //                 0, 0.5, 0,
  //                 0, 0.5, 0,
  //                 0.5, 0.5, 0
  //             )), WebGL.STATIC_DRAW)
  //             _ <- gl2.vertexAttribPointer(positionLoc.location, 3, WebGL.FLOAT, false, 0, 0)

  //         } yield (_: Line) => {
  //             for {
  //                 canvas <- gl2.canvas
  //                 _ <- gl2.viewport(0, 0, canvas.clientWidth, canvas.clientHeight)
  //                 _ <- gl2.clearColor(0.5, 0, 0, 0.1)
  //                 _ <- gl2.clear(WebGL.COLOR_BUFFER_BIT | WebGL.DEPTH_BUFFER_BIT)
  //                 _ <- gl2.enable(WebGL.CULL_FACE)
  //                 _ <- gl2.enable(WebGL.DEPTH_TEST)
  //                 _ <- gl2.useProgram(prog)
  //                 _ <- gl2.bindVertexArray(vao1)
  //                 _ <- gl2.uniform1f(translationxUniform.loc, 0d)
  //                 _ <- gl2.uniform1f(translationyUniform.loc, 0d)
  //                 _ <- gl2.drawArrays(WebGL.LINES, 0, 6)

  //                 _ <- gl2.bindVertexArray(vao2)
  //                 _ <- gl2.uniform1f(translationxUniform.loc, 0)
  //                 _ <- gl2.uniform1f(translationxUniform.loc, 0)
  //                 _ <- gl2.drawArrays(WebGL.LINES, 0, 8)

  //                 _ <- Sync[F].delay(println("Hello"))

  //             } yield ()
  //         }
  //     }
  // }

}

// object Shaders {

//   // case class VertexShaderAttrs(
//   //     a_position: Attribute[(Double, Double, Double, Double)]
//   // )

//   // trait VertexShaderUniforms {
//   //   val translationx: Uniform[Double]
//   //   val translationy: Uniform[Double]
//   // }

//   val vertexShader =
//     """#version 300 es
//     |
//     |in vec4 a_position;
//     |
//     |uniform float translationx;
//     |uniform float translationy;
//     |
//     |void main() {
//     |   gl_Position = vec4(a_position.x + translationx, a_position.y + translationy, a_position.z, a_position.w);
//     |}
//     |
//     """.stripMargin

//   val fragmentShader = """#version 300 es
//     |precision mediump float;
//     |
//     |out vec4 out_color;
//     |
//     |void main() {
//     |   out_color = vec4(0.5, 0, 0, 1);
//     |}
//     |
//     """.stripMargin

// }
