package dpol.conan_retired

import cats.Foldable
import cats.Traverse
import cats.data.Kleisli
import cats.effect._
import cats.implicits._

import dpol.webgl.WebGL2ContextAlgebra
import dpol.webgl._
import org.scalajs.dom.raw.WebGLRenderingContext
import scala.scalajs.js.typedarray.Float32Array
import scala.scalajs.js
import scala.concurrent.duration._
import dpol.math.Matrix
import dpol.math.Matrix.MatrixOps
import dpol.math.Vector._
import dpol.math.VectorOps._
import scala.scalajs.js.typedarray.Uint8Array
import fs2.concurrent.SignallingRef
import fs2.concurrent.Signal
import fs2.Stream
import org.scalajs.dom.raw.Element
import org.scalajs.dom.raw.HTMLCanvasElement
import org.scalajs.dom.raw.MouseEvent
import org.scalajs.dom.raw.Event
import scala.scalajs.js.JSON
import org.scalajs.dom.raw.WebGLUniformLocation
import cats.Monad
import dpol.math.M3x3Double
import dpol.math.Vec3Double
import fs2.Pipe
import org.scalajs.dom.raw.HTMLDivElement
import org.scalajs.dom.raw.HTMLButtonElement
import scala.concurrent.ExecutionContext
import java.util.concurrent.TimeUnit
import cats.effect.concurrent.Ref
import cats.effect.concurrent.Deferred
import fs2.concurrent.Queue
import dpol.conan_retired.SettingsUI.Settings
import cats.instances.queue
import scala.util.Random
import cats.data.OptionT
import org.scalajs.dom.raw.TouchEvent

object Firestarter {

  implicit val ioTimer            = IO.timer
  implicit val globalContextShift = IO.contextShift(ExecutionContext.Implicits.global)

  type Location = Vec3Double

  case class Camera(location: Location, direction: (Double, Double))
  case class State(camera: Camera, hoverLocation: Option[Vec3Double], squares: Vector[Vec3Double])

  run(Nil).unsafeRunAsync {
    case Left(value) =>
      println("Your program appears to have exploded")
      value.printStackTrace()
    case Right(value) => println("All is well and complete")
  }

  def testoo = {
    val s        = new Storage[IO](org.scalajs.dom.window.localStorage)
    val potootoo = Stream(1, 2, 3).evalMap(i => IO(println(i)))
    // 1.toPotato
    for {
      currentM <- s.get[List[String]]("testM").recover {
        case _ => Nil
      }
      _ <- s.set[List[String]]("testM", "Hek" :: currentM)
      _ <- IO(println(s"StoragM: $currentM"))
    } yield ()
  }

  def run(args: List[String]): IO[ExitCode] = {

    for {
      controls <- Html[IO].getElementById[HTMLDivElement]("controls")
      canvas   <- Html[IO].getElementById[HTMLCanvasElement]("canvas")
      storage  <- IO { new Storage[IO](org.scalajs.dom.window.localStorage) }
      _ <- (
        for {
          can  <- canvas
          cont <- controls
        } yield runOn[IO](can, cont, storage)
      ).left.map { e =>
        IO(println(s"Error looking up HTML bits: $e"))
      }.merge
      _ <- IO(println("Program complete"))
    } yield ExitCode.Success
  }

  def runOn[F[_]: ConcurrentEffect: Timer](
      canvas: HTMLCanvasElement,
      controls: HTMLDivElement,
      storage: Storage[F]
  ): F[Unit] = {

    val drawerResource = drawer(canvas)

    (for {
      s <- SettingsUI[F](storage, controls).holdOption
      q <- Stream.eval(Queue.synchronous[F, FiniteDuration])
      pc <- s.discrete
        .collect { case Some(s) => if (s.fps > 0) (1000 / s.fps).millis else Duration.Inf }
        .through(programClock)
        .through(q.enqueue)
        .concurrently(
          s.discrete
            .collect { case Some(s) => s }
            .through(program(controls, canvas, drawerResource, q))
        )
    } yield ()).compile.drain
  }

  def programClock[F[_]: Concurrent: Timer]: Pipe[F, Duration, FiniteDuration] =
    _.switchMap {
      case f: FiniteDuration =>
        Stream.repeatEval(Timer[F].clock.realTime(TimeUnit.NANOSECONDS)).metered(f)
      case _ =>
        Stream.empty
    }.zipWithPrevious
      .collect {
        case (Some(previous), next) => FiniteDuration(next - previous, TimeUnit.NANOSECONDS)
      }

  def program[F[_]: ConcurrentEffect: Timer](
      controlsDiv: HTMLDivElement,
      canvas: HTMLCanvasElement,
      drawerResource: Resource[F, (State, Settings) => F[Unit]],
      clock: Queue[F, FiniteDuration]
  ): Pipe[F, Settings, Unit] = settingsStream => {

    val initialState = State(Camera(Vec3Double(0, 5, 5), (0, -45)), None, Vector.empty)

    def sync[F[_]: ConcurrentEffect: Timer](
        stateRef: Ref[F, State],
        drawerResource: Resource[F, (State, Settings) => F[Unit]],
        settingsStream: Stream[F, Settings]
    ): Pipe[F, FiniteDuration, Unit] = clock => {

      val movement = Controls[F].movement
      val mousePos = Controls[F].mousePosition(canvas)
      val potoot   = Queue.unbounded[F, FiniteDuration]

      for {
        m     <- movement
        lr    <- Controls[F].leftRight("KeyQ", "KeyE")
        mouse <- mousePos
        canvasSize <- Stream
          .awakeEvery[F](1.second)
          .map(_ => (canvas.clientWidth, canvas.clientHeight))
          .hold(0, 0)
        drawer   <- Stream.resource(drawerResource)
        settings <- settingsStream.holdOption

        _ <- isoCamera(
          clock,
          m,
          settings.map(o => o.map(v => v.turnSpeed -> v.movSpeed).getOrElse(0 -> 0)),
          lr
        ).zip(mouse.continuous)
          .zip(canvasSize.continuous)
          .zipWith(settings.continuous.collect {
            case Some(s) => s
          }) {
            case (((camera, mouser), canvasSize), settings) =>
              (
                stateRef.updateAndGet(
                  _.copy(
                    camera = camera,
                    whereOnGroundPlaneAmILooking(
                      camera,
                      settings.renderSettings.fovInRads,
                      mouser,
                      canvasSize
                    )
                  )
                ),
                ConcurrentEffect[F].pure(settings)
              ).tupled
          }
          .evalMap(f => f.flatMap(drawer.tupled))

      } yield ()

    }

    def async[F[_]: Timer: ConcurrentEffect](
        stateRef: Ref[F, State],
        settingsSignal: Signal[F, Option[Settings]],
        canvas: HTMLCanvasElement
    ): F[Unit] = {

      val clicks = HtmlStreams[F]
        .eventOn[MouseEvent]("click", canvas)
        .evalMap(v =>
          (for {
            settings <- OptionT(settingsSignal.get)

            canvasWidth  <- OptionT.liftF(ConcurrentEffect[F].delay(canvas.width))
            canvasHeight <- OptionT.liftF(ConcurrentEffect[F].delay(canvas.height))

            _ <- OptionT.liftF(stateRef.update(state => {
              state.copy(
                squares = whereOnGroundPlaneAmILooking(
                  state.camera,
                  settings.renderSettings.fovInRads,
                  v.clientX.toInt -> v.clientY.toInt,
                  canvasWidth     -> canvasHeight
                ).map(_ +: state.squares).getOrElse(state.squares)
              )
            }))

          } yield ()).value.void
        )

      def filterSingleTouch(t: TouchEvent): Boolean = t.changedTouches.length == 1

      val touchMove = Stream(
        HtmlStreams[F].eventOn[TouchEvent]("touchstart", canvas).filter(filterSingleTouch).map(t => (t.changedTouches(0).clientX.toInt -> t.changedTouches(0).clientY.toInt).some),
        HtmlStreams[F].eventOn[TouchEvent]("touchmove", canvas).filter(filterSingleTouch).map(t => (t.changedTouches(0).clientX.toInt -> t.changedTouches(0).clientY.toInt).some),
        HtmlStreams[F].eventOn[TouchEvent]("touchend", canvas).filter(_.changedTouches.length == 0).evalMap(t => {ConcurrentEffect[F].delay(t.preventDefault()) >> ConcurrentEffect[F].pure(None: Option[(Int, Int)])})
      ).covary[F]
        .parJoin(3)
        .zipWithPrevious
        .collect {
          case (Some(Some((x1, y1))), Some((x2, y2))) => 
            (x2 - x1, y2 - y1)
        }
        .evalMap {
          case (xDiff, yDiff) => 
            println(xDiff, yDiff)
            stateRef.update(state => state.copy(
              camera = state.camera.copy(
                // location = state.camera.location.copy(z = state.camera.location.z + yDiff*100),
                direction = (state.camera.direction._1 + xDiff*100) -> state.camera.direction._2
              )
            )) >> stateRef.get
        }.evalMap(s => ConcurrentEffect[F].delay(println(s)))
        

      touchMove.concurrently(clicks).compile.drain

    }

    for {
      state <- Stream.eval(Ref[F].of(initialState))

      settingsSignal <- settingsStream.holdOption

      _ <- clock.dequeue
        .through(sync(state, drawerResource, settingsSignal.discrete.collect { case Some(s) => s }))
        .concurrently(Stream.eval(async(state, settingsSignal, canvas)))
    } yield ()

  }

  def isoCamera[F[_]](
      clock: Stream[F, FiniteDuration],
      movement: Signal[F, Vec3Double],
      speeds: Signal[F, (Int, Int)],
      lr: Signal[F, Int]
  ): Stream[F, Camera] =
    clock
      .zip(speeds.continuous)
      .zipWith(lr.continuous) {
        case ((d, (ts, ms)), r) =>
          val coef = d.toMillis.toDouble / 1000
          (-(ts * coef * r), d, ms)
      }
      .scan((0, Duration.Zero, 0)) {
        case ((sum, _, _), (n, d, ms)) => (((sum + n.toInt) % 360), d, ms)
      }
      .zipWith(movement.continuous) {
        case ((angle, d, ms), movement) =>
          val coef = ms * d.toMillis.toDouble / 1000
          (movement.copy(z = -movement.z) * coef) -> angle
      }
      .scan(Camera(Vec3Double(0, 5, 5), (0, -45))) {
        case (c, (mov, angle)) =>
          val m           = M3x3Double.RowMajor.yRotation(Math.toRadians(-angle))
          val modifiedMov = Matrix.multiply(mov, m)

          Camera(Matrix.add(c.location, modifiedMov), (angle, -45))
      }

  def whereOnGroundPlaneAmILooking(
      camera: Camera,
      fovRads: Double,
      mouser: (Int, Int),
      canvasSize: (Int, Int)
  ): Option[Vec3Double] = {

    if (canvasSize._1 == 0 || canvasSize._2 == 0) {
      None
    } else {

      val aspect = canvasSize._1.toDouble / canvasSize._2.toDouble

      val angle = Math.tan(0.5 * fovRads)

      val x = (2 * (mouser._1.toDouble / (canvasSize._1 - 1).toDouble) - 1) * aspect * angle
      val y = (2 * (mouser._2.toDouble / (canvasSize._2 - 1).toDouble) - 1) * -angle

      val cameraDirectionVector =
        Matrix
          .multiply(
            Vec3Double(x, y, -1),
            Matrix.multiply(
              M3x3Double.RowMajor.yRotation(Math.toRadians(camera.direction._1)),
              M3x3Double.RowMajor.xRotation(Math.toRadians(camera.direction._2))
            )
          )
          .normalised

      val testRayOrigin = camera.location
      val planeOrigin   = Vec3Double(0, 0, 0)
      val planeNormal   = Vec3Double(0, 1, 0)

      cameraDirectionVector.flatMap(testRayDirection => {
        val ttt = testRayDirection.dotProduct(planeNormal)
        if (Math.abs(ttt) < 0.0000001) {
          None
        } else {
          val t     = (Matrix.subtract(planeOrigin, testRayOrigin).dotProduct(planeNormal)) / ttt
          val point = Matrix.add(testRayOrigin, testRayDirection.*(t))
          Some(point)
        }
      })

    }

  }

  def drawer[F[_]](
      canvas: HTMLCanvasElement
  )(implicit F: Sync[F]): Resource[F, (State, Settings) => F[Unit]] = {

    val v     = ElVisual[F]
    val zNear = 0.1
    val zFar  = 100000

    Resource
      .liftF(WebGL.initialiseWebGL2[F](canvas))
      .flatMap(implicit gl => {
        for {
          program <- v.simpleProgram
          axes    <- v.drawerAxes(program)
          sphere  <- v.sphereDrawer(program)
          cube <-    v.cubeDrawer(program)

        } yield {
          case (State(c, hover, squares), settings) =>
            v.drawFrame(aspect => {
              val projectionMatrix = Matrix.perspective(
                settings.renderSettings.fovInRads,
                aspect,
                settings.renderSettings.zNear,
                settings.renderSettings.zFar
              )

              val cameraMatrix = Matrix.identity
                .translate(c.location.x, c.location.y, c.location.z)
                .yRotate(Math.toRadians(c.direction._1))
                .xRotate(Math.toRadians(c.direction._2))
              val viewMatrix = Matrix.inverse(cameraMatrix)

              val viewProjectionMatrix = Matrix.multiply(projectionMatrix, viewMatrix)

              val hoverSphere = hover
                .map { case Vec3Double(x, y, z) => sphere(viewProjectionMatrix.translate(x, y, z)) }
                .getOrElse(F.unit)

              axes(viewProjectionMatrix) >> hoverSphere >> sphere(viewProjectionMatrix) >> squares
                .map(v => cube(viewProjectionMatrix.translate(v.x, v.y, v.z)))
                .sequence
                .void

            })
        }
      })
  }

}
