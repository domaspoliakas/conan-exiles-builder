package dpol.conan_retired.graph

import cats.Functor
import dpol.conan_retired.graph.Graph.Grapherton
import cats.kernel.Eq
import cats.Traverse
import cats.Eval
import cats.Applicative
import cats.instances.vector._
import cats.instances.map._
import cats.syntax.traverse._
import cats.syntax.applicative._
import cats.syntax.functor._

sealed trait Graph[N, E]

sealed trait EdgeProjectedGraph[E, N]
object EdgeProjectedGraph {
  implicit def nodeProjectedGraphFunctor[N]
      : Functor[EdgeProjectedGraph[*, N]] =
    new Functor[EdgeProjectedGraph[*, N]] {
      override def map[A, B](
          fa: EdgeProjectedGraph[A, N]
      )(f: A => B): EdgeProjectedGraph[B, N] = fa match {
        case g @ Grapherton(nextAvailableId, nodes, edges) =>
          g.copy(
            edges = edges.map {
              case (from, to, edge) => (from, to, f(edge))
            }
          )
      }
    }
}

object Graph {

  implicit def graphTraverse[E]: Traverse[Graph[*, E]] =
    new Traverse[Graph[*, E]] {
      override def foldLeft[A, B](fa: Graph[A, E], b: B)(
          f: (B, A) => B
      ): B = fa match {
        case g: Grapherton[A, E] =>
          g.nodes
            .foldLeft(b) {
              case (b, (id, node)) => f(b, node)
            }
      }

      override def foldRight[A, B](fa: Graph[A, E], lb: Eval[B])(
          f: (A, Eval[B]) => Eval[B]
      ): Eval[B] = fa match {
        case g: Grapherton[A, E] =>
          Traverse[Vector].foldRight(
            g.nodes.map(_._2),
            lb
          )(f)
      }

      override def traverse[G[_]: Applicative, A, B](fa: Graph[A, E])(
          f: A => G[B]
      ): G[Graph[B, E]] = fa match {
        case g: Grapherton[A, E] =>
          g.nodes
            .map {
              case (id, node) => f(node).map(id -> _)
            }
            .sequence
            .map(n => g.copy(nodes = n))
      }
    }

  implicit def graphEq[N: Eq, E: Eq]: Eq[Graph[N, E]] =
    Eq.fromUniversalEquals

  private type NodeId = Int

  private[graph] case class Grapherton[N, E](
      val nextAvailableId: NodeId,
      val nodes: Vector[(NodeId, N)],        // node ID to node object map
      val edges: Vector[(NodeId, NodeId, E)] // Key = from -> to, value = edge connecting the two nodes
  ) extends Graph[N, E]
      with EdgeProjectedGraph[E, N]

  def empty[N, E]: Graph[N, E] =
    Grapherton(0, Vector.empty, Vector.empty)

  /**
    * adds a node to the graph which is not connected to anything
    */
  def add[N, E](n: N): Graph[N, E] => Graph[N, E] = {
    case g @ Grapherton(nextAvailableId, nodes, edges) =>
      g.copy(
        nextAvailableId = nextAvailableId + 1,
        nodes = (nextAvailableId -> n) +: nodes
      )
  }

  def nodeCount[N, E](g: Graph[N, E]): Int = g match {
    case g: Grapherton[N, E] => g.nodes.size
  }

  def graph[N, E](n: EdgeProjectedGraph[E, N]): Graph[N, E] =
    n match {
      case g: Grapherton[N, E] => g
    }

  def edges[N, E](n: Graph[N, E]): EdgeProjectedGraph[E, N] =
    n match {
      case g: Grapherton[N, E] => g
    }
}
