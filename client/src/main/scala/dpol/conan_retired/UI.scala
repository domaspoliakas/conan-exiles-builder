package dpol.conan_retired

import fs2._
import fs2.concurrent._
import cats.implicits._
import cats.effect._
import scala.scalajs.js

object UI {

  def reactive[F[_]: ConcurrentEffect, A](
      materialization: (A => Unit) => Resource[F, A => F[Unit]]
  ): Pipe[F, A, A] = stream => {
    for {
      events <- Stream.eval(Queue.circularBuffer[F, A](1))
      writer <- Stream.resource(materialization(i => {
        ConcurrentEffect[F].runAsync(events.enqueue1(i))(_ => IO.unit).unsafeRunSync()
      }))
      e <- events.dequeue.concurrently(stream.evalMap(writer))
    } yield e
  }

  // sealed trait PerspectiveSelection
  // object PerspectiveSelection {
  //   case object Isometric   extends PerspectiveSelection
  //   case object Perspective extends PerspectiveSelection
  // }

  // type FovDegrees = Int

  // def buildUI[F[_]: ConcurrentEffect](
  //     controlsDiv: HTMLDivElement
  // ): Stream[F, (FovDegrees, PerspectiveSelection)] = {

  //   val html     = Html[F]
  //   val controls = Controls[F]

  //   val buttonIso =
  //     for {
  //       button <- Stream
  //         .eval(html.createElement[HTMLButtonElement])
  //         .evalMap(button => Sync[F].delay {
  //           button.textContent = "Isometric"
  //           controlsDiv.appendChild(button)
  //           button
  //         })
  //       _ <- HtmlStreams[F].eventOn[MouseEvent]("click", button)
  //     } yield Isometric

  //   val buttonPerspective = for {
  //       button <- Stream
  //         .eval(html.createElement[HTMLButtonElement])
  //         .evalMap(button => Sync[F].delay {
  //           button.textContent = "Perspective"
  //           controlsDiv.appendChild(button)
  //           button
  //         })
  //       _ <- HtmlStreams[F].eventOn[MouseEvent]("click", button)
  //     } yield Perspective

  //     buildFovSlider(controlsDiv).zip(buttonIso.merge(buttonPerspective))

  // }

  // import scalatags.JsDom._
  // import scalatags.JsDom.all._

  // def buildFovSlider[F[_]: ConcurrentEffect](parent: Element): Stream[F, Int] = {

  //   val default = 60

  //   val elem = input(
  //     `type` := "range",
  //     min := 30,
  //     max := 120,
  //     id := "fov_slider",
  //     value := default
  //   ).render

  //   val divoo = 
  //   div(
  //     elem,
  //     label(`for` := "fov_slider", "FOV")
  //   ).render

  //   (for {
  //     _ <- Stream.eval(Sync[F].delay(parent.appendChild(divoo))).debug(_ => "Hek")
  //     i <- Stream(default) ++ HtmlStreams[F].eventOn[Event]("input", elem).evalMap(_ => Sync[F].delay(elem.value.toInt))
  //   } yield i)

  // }

  // def buildFpsSlider[F[_]: ConcurrentEffect](parent: Element): Stream[F, Int] = {

  //   val default = 25

  //   val elem = input(
  //     `type` := "range",
  //     min := 0,
  //     max := 120,
  //     id := "fps_slider",
  //     value := default
  //   ).render
  //   val divoo = 
  //   div(
  //     elem,
  //     label(`for` := "fps_slider", "FPS")
  //   ).render

  //   (for {
  //     _ <- Stream.eval(Sync[F].delay(parent.appendChild(divoo)))
  //     i <- Stream(default) ++ HtmlStreams[F].eventOn[Event]("input", elem).evalMap(_ => Sync[F].delay(elem.value.toInt))
  //   } yield i)

  // }

}
