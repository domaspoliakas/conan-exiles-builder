package dpol.conan_retired

import cats.syntax.all._
import dpol.math.Vector.Vec3
import dpol.math._
import fs2.Stream
import fs2.concurrent.Queue
import org.scalajs.dom.raw.Event
import org.scalajs.dom.raw.EventTarget
import cats.effect.Sync
import cats.effect.ConcurrentEffect
import cats.effect.IO
import org.scalajs.dom.raw.MouseEvent
import org.scalajs.dom.raw.KeyboardEvent
import scala.scalajs.js
import fs2.concurrent.SignallingRef
import fs2.concurrent.Signal
import org.scalajs.dom.raw.Element
import cats.effect.Concurrent

class Controls[F[_]] {

  /**
    * 0 if neutral, 1 if clockwise, -1 if counter-clockwise
    */
  def leftRight(leftCode: String, rightCode: String)(
      implicit F: ConcurrentEffect[F]
  ): Stream[F, Signal[F, Int]] = {
    HtmlStreams[F].keyboardEvents
      .map(e => e -> e.asInstanceOf[js.Dynamic].code.asInstanceOf[String])
      .filter {
        case (e, code) => !e.repeat && (code == leftCode || code == rightCode)
      }
      .map {
        case (e, code) =>
          val a = if (e.`type` == "keydown") 1 else -1
          if (code == leftCode) -a else a
      }.scan(0)(_ + _).hold(0)
  }

  def movement(
      movementKeys: Map[String, (Vec3Double => Vec3Double, Vec3Double => Vec3Double)]
  )(implicit cf: ConcurrentEffect[F]): Stream[F, Signal[F, Vec3Double]] = {
    val keys = movementKeys.keySet
    val ref  = SignallingRef(Vec3Double(0, 0, 0))

    def potat(s: SignallingRef[F, Vec3Double]): Stream[F, Unit] =
      HtmlStreams[F].keyboardEvents
        .filter(s =>
          !s.repeat && keys.contains(s.asInstanceOf[js.Dynamic].code.asInstanceOf[String])
        )
        .flatMap(event => {
          movementKeys
            .get(event.asInstanceOf[js.Dynamic].code.asInstanceOf[String])
            .map {
              case (ifDown, ifUp) =>
                val f = if (event.`type` == "keydown") ifDown else ifUp
                Stream.eval((s.update(v => {
                  f(v)
                }))): Stream[F, Unit]
            }
            .getOrElse(
              Stream.eval(
                cf.delay(
                  println(
                    "Movement key was pressed, but further inspection revealed no movement function"
                  )
                )
              )
            )
        })

    for {
      ref <- Stream.eval(SignallingRef(Vec3Double(0, 0, 0)))
      s   <- Stream(ref).concurrently(potat(ref))
    } yield s

  }

  def movement(implicit cf: ConcurrentEffect[F]): Stream[F, Signal[F, Vec3Double]] = {

    def potat(
        value: Double,
        source: Vec3Double => Double,
        copier: Double => Vec3Double => Vec3Double
    ): (Vec3Double => Vec3Double, Vec3Double => Vec3Double) = {
      val one: Vec3Double => Vec3Double = v => copier(source(v) + value)(v)
      val two: Vec3Double => Vec3Double = v => copier(source(v) - value)(v)
      one -> two
    }

    movement(
      Map(
        "KeyW"        -> potat(1, _.z, i => _.copy(z = i)),
        "KeyS"        -> potat(-1, _.z, i => _.copy(z = i)),
        "KeyA"        -> potat(-1, _.x, i => _.copy(x = i)),
        "KeyD"        -> potat(1, _.x, i => _.copy(x = i)),
        "Space"       -> potat(1, _.y, i => _.copy(y = i)),
        "ControlLeft" -> potat(-1, _.y, i => _.copy(y = i))
      )
    )
  }

  def mousePosition(
      onElement: Element
  )(implicit cf: ConcurrentEffect[F]): Stream[F, Signal[F, (Int, Int)]] = {
    HtmlStreams[F]
      .eventOn[MouseEvent]("mousemove", onElement)
      .map(e => e.clientX.toInt -> e.clientY.toInt)
      .hold(0 -> 0)
  }

  def cameraMovement(
      lockElement: Element
  )(implicit cf: ConcurrentEffect[F]): Stream[F, (Int, Int)] = {

    def pointerLockChanges =
      HtmlStreams[F].eventOn[Event]("pointerlockchange", org.scalajs.dom.document)

    def potoo: Stream[F, (Int, Int)] =
      HtmlStreams[F]
        .eventOn[MouseEvent]("click", lockElement)
        .take(1)
        .collect(PartialFunction.empty[MouseEvent, (Int, Int)]) ++
        pointerLockChanges
          .take(1)
          .concurrently(
            Stream.eval(Sync[F].delay(lockElement.asInstanceOf[js.Dynamic].requestPointerLock()))
          )
          .collect(PartialFunction.empty[Event, (Int, Int)]) ++
        HtmlStreams[F]
          .eventOn[MouseEvent]("mousemove", org.scalajs.dom.document)
          .map(v =>
            v.asInstanceOf[js.Dynamic]
              .movementX
              .asInstanceOf[Int] -> v.asInstanceOf[js.Dynamic].movementY.asInstanceOf[Int]
          )
          .interruptWhen(pointerLockChanges.map(_ => true)) ++
        potoo

    potoo

  }

  def cameraAnglesAccumulated(
      canvas: Element
  )(implicit F: ConcurrentEffect[F]): Stream[F, Signal[F, (Double, Double)]] = {
    Controls[F]
      .cameraMovement(canvas)
      .scan(0d -> 0d) {
        case ((a, b), (x, y)) => ((a + x) % 360, Math.min(Math.max(b + y, -90), 90))
      }
      .hold(0d -> 0d)
  }

  def positionChangeSignal(
      userInput: Signal[F, Vec3Double],
      cameraDirection: Signal[F, (Double, Double)]
  )(implicit f: Concurrent[F]): Signal[F, Vec3Double] = {
    (userInput, cameraDirection).mapN {
      case (u, (rx, ry)) =>
        Matrix.multiply(
          u,
          Matrix.multiply(
            M3x3Double.RowMajor.yRotation(Math.toRadians(rx)),
            M3x3Double.RowMajor.xRotation(Math.toRadians(ry))
          )
        )
    }
  }

  def positionChangeAndCamera(canvas: Element)(implicit F: ConcurrentEffect[F]) = {
    for {
      mousey    <- cameraAnglesAccumulated(canvas)
      keyboards <- movement
      potatos = positionChangeSignal(
        keyboards,
        mousey
      )
    } yield (potatos, mousey).tupled
  }
}

object Controls {
  def apply[F[_]] = new Controls[F]
}
