package dpol.conan_retired

import cats.Traverse
import cats.Eval
import cats.Applicative
import cats.data.NonEmptyList

sealed trait Square

sealed trait Axis
object Axis {
  case object X extends Axis
  case object Y extends Axis
}

case class Movement(axis: Axis, positive: Boolean) {
  def inverse = copy(positive = !positive)
}

trait SquareOps[A] {

  /**
    * New square with no neighbours
    */
  def initialise: A

  /**
    * Either a new square if add succeeded, None if there's already something in that slot
    */
  def add(direction: Movement)(origin: A): Option[A]

  /**
    * Some with the new square or None if no square exists in that direction
    */
  def move(direction: Movement)(origin: A): Option[A]

}

private[conan_retired] case class Sqoiro(
    currentNode: (Int, Int),
    nodes: Vector[(Int, Int)],
    edges: Map[(Int, Int), Vector[(Int, Int)]]
) extends Square

object SqoiroOps extends SquareOps[Sqoiro] {
  override def initialise: Sqoiro =
    Sqoiro(0 -> 0, Vector(0 -> 0), Map((0, 0) -> Vector.empty))

  private def getCoordsFor(
      node: (Int, Int),
      movement: Movement
  ): (Int, Int) = movement match {
    case Movement(Axis.X, dir) =>
      node.copy(_1 = node._1 + (if (dir) 1 else -1))
    case Movement(Axis.Y, dir) =>
      node.copy(_2 = node._2 + (if (dir) 1 else -1))
  }

  private def adjacents(node: (Int, Int)): Vector[(Int, Int)] = {
    Vector(
      node.copy(_1 = node._1 + 1),
      node.copy(_1 = node._1 - 1),
      node.copy(_2 = node._2 + 1),
      node.copy(_2 = node._2 - 1)
    )
  }

  override def add(
      direction: Movement
  )(origin: Sqoiro): Option[Sqoiro] = {
    val other = getCoordsFor(origin.currentNode, direction)
    origin.edges
      .get(origin.currentNode)
      .filterNot(_.contains(other))
      .map(_ => {

        val existingAdjacents =
          adjacents(other).filter(origin.nodes.contains)

        val withInboundToOther =
          existingAdjacents.foldLeft(origin.edges) {
            case (edges, adj) =>
              edges
                .get(adj)
                .map(adjEdges => edges + (adj -> (other +: adjEdges)))
                .getOrElse(edges + (adj -> Vector(other)))
          }

        val newEdges = withInboundToOther ++ existingAdjacents.map(adj => other -> Vector(adj))

        origin.copy(
          nodes = other +: origin.nodes,
          edges = newEdges
        )
      })
  }

  override def move(direction: Movement)(
      origin: Sqoiro
  ): Option[Sqoiro] = {
    val c = getCoordsFor(origin.currentNode, direction)

    if (origin.nodes.contains(c)) Some(origin.copy(currentNode = c)) else None
  }

}

private[conan_retired] case class Squarino(dirs: Seq[NonEmptyList[Movement]])
    extends Square

object SquarinoOps extends SquareOps[Squarino] {

  /**
    * New square with no neighbours
    */
  def initialise: Squarino = Squarino(Vector.empty)

  /**
    * Either a new square if add succeeded, None if there's already something in that slot
    */
  def add(direction: Movement)(origin: Squarino): Option[Squarino] =
    origin match {
      case Squarino(dirs) if !dirs.exists {
            case NonEmptyList(d, Nil) if d == direction => true
            case _                                      => false
          } =>
        Some(Squarino(NonEmptyList.one(direction) +: dirs))

      case _ => None
    }

  /**
    * Some with the new square or None if no square exists in that direction
    */
  def move(direction: Movement)(origin: Squarino): Option[Squarino] =
    origin match {
      case Squarino(dirs) if dirs.exists {
            case NonEmptyList(d, Nil) if d == direction => true
            case _                                      => false
          } =>
        val inverse = direction.inverse

        Some(
          Squarino(
            dirs.map {
              case NonEmptyList(head, tail) if head == direction =>
                NonEmptyList
                  .fromList(tail)
                  .getOrElse(NonEmptyList.one(direction.inverse))
              case n => inverse :: n
            }
          )
        )

      case _ => None

    }

}
