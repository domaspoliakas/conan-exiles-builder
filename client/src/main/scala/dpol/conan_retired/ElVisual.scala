package dpol.conan_retired

import cats.effect.Resource
import cats.effect.Sync

import cats._
import cats.data._
import cats.instances.function._
import cats.instances.list._
import cats.syntax.all._

import dpol.webgl.WebGLVertexArrayObject
import dpol.webgl.WebGL2ContextAlgebra
import dpol.webgl.VertexShader
import dpol.webgl.FragmentShader
import dpol.webgl.Program
import dpol.webgl.Buffer
import dpol.webgl.VertexArrayObject

import org.scalajs.dom.raw.WebGLBuffer
import org.scalajs.dom.raw.WebGLProgram
import org.scalajs.dom.raw.WebGLRenderingContext

import scala.scalajs.js
import scala.scalajs.js.JSConverters._

import scala.scalajs.js.typedarray.Float32Array
import scala.scalajs.js.typedarray.Uint8Array
import scala.collection.immutable.Nil
import dpol.math.Vec3Double
import dpol.math.Matrix
import dpol.math.M3x3Double
import scala.util.Random

class ElVisual[F[_]] {

  def simpleProgram(
      implicit gl: WebGL2ContextAlgebra[F],
      me: MonadError[F, Throwable]
  ): Resource[F, WebGLProgram] = {
    for {
      vshad <- VertexShader(ElVisual.vertexShader)
      fshad <- FragmentShader(ElVisual.fragmentShader)
      prog  <- Program(vshad, fshad)
    } yield prog
  }

  def simpleDrawer(
      posBufferContent: js.Array[Double],
      colorBufferContent: js.Array[Int],
      prog: WebGLProgram,
      drawType: Int = WebGLRenderingContext.TRIANGLES
  )(
      implicit gl: WebGL2ContextAlgebra[F],
      me: MonadError[F, Throwable]
  ): Resource[F, js.Array[Double] => F[Unit]] = {

    val posBufSize = posBufferContent.size / 3

    def potoot(arr: js.Array[Double]): Float32Array = new Float32Array(arr.map(_.toFloat))
    def potoot2(arr: js.Array[Int]): Uint8Array  = new Uint8Array(arr.map(_.toShort))

    (for {
      posBuffer   <- Buffer[F]
      colorBuffer <- Buffer[F]
      vao         <- VertexArrayObject[F]
    } yield (posBuffer, colorBuffer, vao)).evalMap {
      case (posBuffer, colorBuffer, vao) =>
        for {
          pos            <- gl.getAttribLocation(prog, "a_position")
          color          <- gl.getAttribLocation(prog, "a_color")
          transformation <- gl.getUniformLocation(prog, "u_transform")

          _ <- gl.bindVertexArray(vao)
          _ <- gl.enableVertexAttribArray(pos)
          _ <- gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, posBuffer)
          _ <- gl.bufferData(
            WebGLRenderingContext.ARRAY_BUFFER,
            potoot(
              posBufferContent
            ),
            WebGLRenderingContext.STATIC_DRAW
          )
          _ <- gl.vertexAttribPointer(pos, 3, WebGLRenderingContext.FLOAT, false, 0, 0)

          _ <- gl.enableVertexAttribArray(color)
          _ <- gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, colorBuffer)
          _ <- gl.bufferData(
            WebGLRenderingContext.ARRAY_BUFFER,
            potoot2(
              colorBufferContent
            ),
            WebGLRenderingContext.STATIC_DRAW
          )
          _ <- gl.vertexAttribPointer(color, 3, WebGLRenderingContext.UNSIGNED_BYTE, true, 0, 0)
        } yield (transfo: js.Array[Double]) =>
          for {
            _ <- gl.useProgram(prog)
            _ <- gl.bindVertexArray(vao)
            _ <- gl.uniformMatrix4fv(
              transformation,
              false,
              transfo
            )
            _ <- gl.drawArrays(drawType, 0, posBufSize)
          } yield ()
    }
  }

  def drawFrame(
      draws: Double => F[Unit]
  )(implicit gl: WebGL2ContextAlgebra[F], m: Monad[F]): F[Unit] =
    for {
      canvas <- gl.canvas
      aspect = canvas.clientWidth.toDouble / canvas.clientHeight.toDouble

      _ <- gl.viewport(0, 0, canvas.clientWidth, canvas.clientHeight)
      _ <- gl.clearColor(1, 1, 1, 0.5)
      _ <- gl.clear(
        WebGLRenderingContext.COLOR_BUFFER_BIT | WebGLRenderingContext.DEPTH_BUFFER_BIT
      )
      _ <- gl.enable(WebGLRenderingContext.CULL_FACE)
      _ <- gl.enable(WebGLRenderingContext.DEPTH_TEST)
      _ <- draws(aspect)

    } yield ()

  def cubeDrawer(prog: WebGLProgram)(implicit gl: WebGL2ContextAlgebra[F], F: Sync[F]): Resource[F, js.Array[Double] => F[Unit]] = {

    val a = (-1d, 1d, 1d)
    val b = (1d, 1d, 1d)
    val c = (-1d, 1d, -1d)
    val d = (1d, 1d, -1d)
    val e = (-1d, -1d, 1d)
    val f = (1d, -1d, 1d)
    val g = (-1d, -1d, -1d)
    val h = (1d, -1d, -1d)

    // val triangles = List(
    //   a, c, b,
    //   b, c, a,
    //   b, a, h,
    //   b, h, f,
    //   a, c, g,
    //   a, g, h,
    //   h, g, f,
    //   g, e, f,
    //   c, a, e,
    //   c, e, g,
    //   a, b, e,
    //   b, f, e,
    // ).flatMap {
    //   case (a, b, c) => List(a, b, c)
    // }.toJSArray

    val triangles = js.Array(
      -1.0d,-1.0d,-1.0d, // triangle 1 : begin
    -1.0d,-1.0d, 1.0d,
    -1.0d, 1.0d, 1.0d, // triangle 1 : end
    1.0d, 1.0d,-1.0d, // triangle 2 : begin
    -1.0d,-1.0d,-1.0d,
    -1.0d, 1.0d,-1.0d, // triangle 2 : end
    1.0d,-1.0d, 1.0d,
    -1.0d,-1.0d,-1.0d,
    1.0d,-1.0d,-1.0d,
    1.0d, 1.0d,-1.0d,
    1.0d,-1.0d,-1.0d,
    -1.0d,-1.0d,-1.0d,
    -1.0d,-1.0d,-1.0d,
    -1.0d, 1.0d, 1.0d,
    -1.0d, 1.0d,-1.0d,
    1.0d,-1.0d, 1.0d,
    -1.0d,-1.0d, 1.0d,
    -1.0d,-1.0d,-1.0d,
    -1.0d, 1.0d, 1.0d,
    -1.0d,-1.0d, 1.0d,
    1.0d,-1.0d, 1.0d,
    1.0d, 1.0d, 1.0d,
    1.0d,-1.0d,-1.0d,
    1.0d, 1.0d,-1.0d,
    1.0d,-1.0d,-1.0d,
    1.0d, 1.0d, 1.0d,
    1.0d,-1.0d, 1.0d,
    1.0d, 1.0d, 1.0d,
    1.0d, 1.0d,-1.0d,
    -1.0d, 1.0d,-1.0d,
    1.0d, 1.0d, 1.0d,
    -1.0d, 1.0d,-1.0d,
    -1.0d, 1.0d, 1.0d,
    1.0d, 1.0d, 1.0d,
    -1.0d, 1.0d, 1.0d,
    1.0d,-1.0d, 1.0d
    )

    val color = List.fill(triangles.length / 3)(List(128, 50, 50)).flatten.toJSArray

    simpleDrawer(
      triangles,
      color,
      prog
    )
  }

  def sphereDrawer(prog: WebGLProgram)(implicit gl: WebGL2ContextAlgebra[F], F: Sync[F]): Resource[F, js.Array[Double] => F[Unit]] = {

    val sphere = generateIcosphere(0.1)
    val c = Random.nextInt(255)

    val color = List.fill(sphere.length)(c).toJSArray

    simpleDrawer(
      sphere,
      color,
      prog
    )
  }

  def generateIcosphere(radius: Double): js.Array[Double] = {

    val t = (1f + Math.sqrt(5)) / 2f;

    val v = Vector(
      Vec3Double(-1, t, 0),
      Vec3Double(1, t, 0),
      Vec3Double(-1, -t, 0),
      Vec3Double(1, -t, 0),
      Vec3Double(0, -1, t),
      Vec3Double(0, 1, t),
      Vec3Double(0, -1, -t),
      Vec3Double(0, 1, -t),
      Vec3Double(t, 0, -1),
      Vec3Double(t, 0, 1),
      Vec3Double(t, 0, -1),
      Vec3Double(-t, 0, 1)
    )

    val m = M3x3Double.RowMajor.scaling(radius, radius, radius)

    Vector(
      v(0), v(11), v(5),
      v(0), v(5), v(1), 
      v(0), v(1), v(7), 
      v(0), v(7), v(10), 
      v(0), v(10), v(11),
      v(1), v(5), v(9), 
      v(5), v(11), v(4), 
      v(11), v(10), v(2), 
      v(10), v(7), v(6), 
      v(7), v(1), v(8),
      v(3), v(9), v(4), 
      v(3), v(4), v(2), 
      v(3), v(2), v(6), 
      v(3), v(6), v(8), 
      v(3), v(8), v(9),
      v(4), v(9), v(5), 
      v(2), v(4), v(11), 
      v(6), v(2), v(10), 
      v(8), v(6), v(7), 
      v(9), v(8), v(1),
    ).flatMap(v => {
      val vv = Matrix.multiply(v, m)
      Vector(vv.x, vv.y, vv.z)
    }).toJSArray

  }

  def drawerAxes(prog: WebGLProgram)(
      implicit gl: WebGL2ContextAlgebra[F],
      me: Sync[F]
  ): Resource[F, js.Array[Double] => F[Unit]] = {

    val bitLength = 10000
    val bitCount  = 100

    LazyList.from(0, bitLength).map(v => v -> -v).sliding(2, 1)

    val vectorisers = List[Int => ((Int, Int, Int), (Int, Int, Int))](
      v => ((v, 0, 0), (255, 0, 0)),
      v => ((0, v, 0), (0, 255, 0)),
      v => ((0, 0, v), (0, 0, 255))
    ).sequence

    val (i, c) = LazyList
      .from(0, bitLength)
      .map(v => v -> -v)
      .sliding(2, 1)
      .map {
        case (h1, h2) #:: (t1, t2) #:: _ => (h1, t1) -> (h2, t2)
      }
      .flatMap(v => List(v._1, v._2))
      .flatMap {
        case (start, end) => vectorisers(start).zip(vectorisers(end))
      }
      .take(bitCount)
      .foldLeft(List[(Int, Int, Int)]() -> List[(Int, Int, Int)]()) {
        case ((i, c), ((v1, c1), (v2, c2))) => (v1 :: v2 :: i) -> (c1 :: c2 :: c)
      }

    simpleDrawer(
      i.flatMap { case (x, y, z) => List(x, y, z) }.map(_.toDouble).toJSArray,
      c.flatMap { case (x, y, z) => List(x, y, z) }.toJSArray,
      prog,
      WebGLRenderingContext.LINES
    )
  }

  def drawerF(
      prog: WebGLProgram
  )(
      implicit gl: WebGL2ContextAlgebra[F],
      me: MonadError[F, Throwable]
  ): Resource[F, js.Array[Double] => F[Unit]] =
    simpleDrawer(
      js.Array[Double](
        // left column front
        0, 0, 0, 0, 150, 0, 30, 0, 0, 0, 150, 0, 30, 150, 0, 30, 0, 0,          // top rung front
        30, 0, 0, 30, 30, 0, 100, 0, 0, 30, 30, 0, 100, 30, 0, 100, 0, 0,       // middle rung front
        30, 60, 0, 30, 90, 0, 67, 60, 0, 30, 90, 0, 67, 90, 0, 67, 60, 0,       // left column back
        0, 0, 30, 30, 0, 30, 0, 150, 30, 0, 150, 30, 30, 0, 30, 30, 150, 30,    // top rung back
        30, 0, 30, 100, 0, 30, 30, 30, 30, 30, 30, 30, 100, 0, 30, 100, 30, 30, // middle rung back
        30, 60, 30, 67, 60, 30, 30, 90, 30, 30, 90, 30, 67, 60, 30, 67, 90, 30, // top
        0, 0, 0, 100, 0, 0, 100, 0, 30, 0, 0, 0, 100, 0, 30, 0, 0, 30,          // top rung right
        100, 0, 0, 100, 30, 0, 100, 30, 30, 100, 0, 0, 100, 30, 30, 100, 0, 30, // under top rung
        30, 30, 0, 30, 30, 30, 100, 30, 30, 30, 30, 0, 100, 30, 30, 100, 30, 0,
        // between top rung and middle
        30, 30, 0, 30, 60, 30, 30, 30, 30, 30, 30, 0, 30, 60, 0, 30, 60, 30,    // top of middle rung
        30, 60, 0, 67, 60, 30, 30, 60, 30, 30, 60, 0, 67, 60, 0, 67, 60, 30,    // right of middle rung
        67, 60, 0, 67, 90, 30, 67, 60, 30, 67, 60, 0, 67, 90, 0, 67, 90, 30,    // bottom of middle rung.
        30, 90, 0, 30, 90, 30, 67, 90, 30, 30, 90, 0, 67, 90, 30, 67, 90, 0,    // right of bottom
        30, 90, 0, 30, 150, 30, 30, 90, 30, 30, 90, 0, 30, 150, 0, 30, 150, 30, // bottom
        0, 150, 0, 0, 150, 30, 30, 150, 30, 0, 150, 0, 30, 150, 30, 30, 150, 0, // left side
        0, 0, 0, 0, 0, 30, 0, 150, 30, 0, 0, 0, 0, 150, 30, 0, 150, 0
      ),
      js.Array[Int](
        // left column front
        200, 70, 120, 200, 70, 120, 200, 70, 120, 200, 70, 120, 200, 70, 120, 200, 70, 120,
        // top rung front
        200, 70, 120, 200, 70, 120, 200, 70, 120, 200, 70, 120, 200, 70, 120, 200, 70, 120,
        // middle rung front
        200, 70, 120, 200, 70, 120, 200, 70, 120, 200, 70, 120, 200, 70, 120, 200, 70, 120,
        // left column back
        80, 70, 200, 80, 70, 200, 80, 70, 200, 80, 70, 200, 80, 70, 200, 80, 70, 200, // top rung back
        80, 70, 200, 80, 70, 200, 80, 70, 200, 80, 70, 200, 80, 70, 200, 80, 70, 200,
        // middle rung back
        80, 70, 200, 80, 70, 200, 80, 70, 200, 80, 70, 200, 80, 70, 200, 80, 70, 200, // top
        70, 200, 210, 70, 200, 210, 70, 200, 210, 70, 200, 210, 70, 200, 210, 70, 200, 210,
        // top rung right
        200, 200, 70, 200, 200, 70, 200, 200, 70, 200, 200, 70, 200, 200, 70, 200, 200, 70,
        // under top rung
        210, 100, 70, 210, 100, 70, 210, 100, 70, 210, 100, 70, 210, 100, 70, 210, 100, 70,
        // between top rung and middle
        210, 160, 70, 210, 160, 70, 210, 160, 70, 210, 160, 70, 210, 160, 70, 210, 160, 70,
        // top of middle rung
        70, 180, 210, 70, 180, 210, 70, 180, 210, 70, 180, 210, 70, 180, 210, 70, 180, 210,
        // right of middle rung
        100, 70, 210, 100, 70, 210, 100, 70, 210, 100, 70, 210, 100, 70, 210, 100, 70, 210,
        // bottom of middle rung.
        76, 210, 100, 76, 210, 100, 76, 210, 100, 76, 210, 100, 76, 210, 100, 76, 210, 100,
        // right of bottom
        140, 210, 80, 140, 210, 80, 140, 210, 80, 140, 210, 80, 140, 210, 80, 140, 210, 80, // bottom
        90, 130, 110, 90, 130, 110, 90, 130, 110, 90, 130, 110, 90, 130, 110, 90, 130, 110,
        // left side
        160, 160, 220, 160, 160, 220, 160, 160, 220, 160, 160, 220, 160, 160, 220, 160, 160, 220
      ),
      prog
    )

}

object ElVisual {
  def apply[F[_]] = new ElVisual[F]

  val vertexShader =
    """#version 300 es
      |
      |in vec4 a_position;
      |in vec4 a_color;
      |
      |uniform mat4 u_transform;
      |
      |out vec4 v_color;
      |
      |void main() {
      |   gl_Position = u_transform * a_position;
      |   v_color = a_color;
      |}
      |
      """.stripMargin

  val fragmentShader = """#version 300 es
      |precision mediump float;
      |
      |in vec4 v_color;
      |out vec4 outColor;
      |
      |void main() {
      |   outColor = v_color;
      |}
      |
      """.stripMargin

}
