package dpol.conan_retired

import cats.effect.ConcurrentEffect
import cats.effect.Sync
import cats.syntax.all._
import fs2.Stream
import fs2.concurrent.Queue
import org.scalajs.dom.raw.Event
import org.scalajs.dom.raw.EventTarget
import scala.scalajs.js
import scala.scalajs.js.|
import org.scalajs.dom.raw.EventListenerOptions
import cats.effect.IO
import org.scalajs.dom.raw.MouseEvent
import org.scalajs.dom.raw.KeyboardEvent

class HtmlStreams[F[_]] {
  
  type UseCapture

  def eventOn[A <: Event](eventType: String, elemenet: EventTarget, opts: Option[UseCapture | EventListenerOptions] = None)(
      implicit c: ConcurrentEffect[F]
  ): Stream[F, A] = {

    val eventQueueAcquisition: F[(Queue[F, A], js.Function1[A, Any])] =
      for {
        queue <- Queue.circularBuffer[F, A](1)
        listener <- Sync[F].delay[js.Function1[A, Any]]((e: A) =>
          ConcurrentEffect[F].runAsync(queue.enqueue1(e))(_ => IO.unit).unsafeRunSync()
        )
        _ <- Sync[F].delay {
          (opts: Any) match {
            case Some(u: Boolean) => elemenet.addEventListener(eventType, listener, u)
            case Some(e) => elemenet.addEventListener(eventType, listener, e.asInstanceOf[EventListenerOptions])
            case None => elemenet.addEventListener(eventType, listener)
          }
          
        }
      } yield queue -> listener

    val bracketedEventQueue = Stream.bracket(
      eventQueueAcquisition
    ) {
      case (queue, listener) =>
          Sync[F].delay(
            elemenet.removeEventListener(eventType, listener)
          )
    }

    for {
      queueAndListener <- bracketedEventQueue
      event            <- queueAndListener._1.dequeue
    } yield event

  }

  def clickEventsFor(id: String)(implicit s: ConcurrentEffect[F]): Stream[F, MouseEvent] =
    for {
      htmlBoi <- Stream.eval(
        Sync[F].delay {
          org.scalajs.dom.document.getElementById(id)
        }
      )
      events <- HtmlStreams[F].eventOn[MouseEvent]("click", htmlBoi)
    } yield events

  def keyboardEvents(implicit cf: ConcurrentEffect[F]): Stream[F, KeyboardEvent] =
    for {
      doc <- Stream.eval(Sync[F].delay(org.scalajs.dom.document))
      event <- Stream(
        HtmlStreams[F].eventOn[KeyboardEvent]("keydown", doc),
        HtmlStreams[F].eventOn[KeyboardEvent]("keyup", doc)
      ).parJoin(2),
    } yield event

}

object HtmlStreams {
  def apply[F[_]] = new HtmlStreams[F]
}