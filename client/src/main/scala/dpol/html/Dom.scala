package dpol.html

import org.scalajs.dom.raw.HTMLHtmlElement
import org.scalajs.dom.raw.Element
import org.scalajs.dom
import cats.effect.Sync
import cats.syntax.all._
import scala.reflect.ClassTag

import org.scalajs.dom.raw.Document
import org.scalajs.dom.raw.HTMLButtonElement
import org.scalajs.dom.raw.HTMLInputElement

import scalatags.JsDom._

// SIDE EFFECTS BOI
trait HtmlTagMaker[E <: Element] {
  def createElement(document: Document): E
}

object HtmlTagMaker {

  def unsafeMakeType[E <: Element](tagName: String) = new HtmlTagMaker[E] {
    def createElement(document: Document): E = document.createElement(tagName).asInstanceOf[E]
  }

  implicit val button = unsafeMakeType[HTMLButtonElement]("button")
  implicit val input = unsafeMakeType[HTMLInputElement]("input")
}

class Dom[F[_]] {

  import Dom._

  def getElementById[A <: Element : ClassTag](id: String)(implicit s: Sync[F]): F[Either[LookupProblem, A]] = {
    Sync[F]
      .delay {
        dom.document.getElementById(id)
      }
      .map { potentiallyCanvas =>
        Option(potentiallyCanvas) match {
          case Some(x: A) => Right(x)
          case Some(_) => Left(LookupProblem.WrongType)
          case _ => Left(LookupProblem.NoElement)
        }
      }
  }

  def createElement[E <: Element](implicit s: Sync[F], maker: HtmlTagMaker[E]): F[E] = {
    Sync[F].delay {
      maker.createElement(dom.document)
    }
  }

  def appendWhileEvaluating[A <: Element, B](appendTarget: Element, tagToAppend: TypedTag[A])(effect: A => F[B])(implicit F: Sync[F]): F[B] = 
    for {
      renderedTag <- F.delay(tagToAppend.render)
      result <- F.bracket(F.delay(appendTarget.appendChild(renderedTag)))(_ => effect(renderedTag))(_ => F.delay(appendTarget.removeChild(renderedTag)))
    } yield result

}

object Dom {
  def apply[F[_]] = new Dom[F]
  sealed trait LookupProblem
  object LookupProblem {
    case object NoElement extends LookupProblem
    case object WrongType extends LookupProblem
  }
}