package dpol.conan

import cats.effect._
import cats._
import cats.data._
import cats.effect.concurrent._
import cats.implicits._

import org.scalajs.dom.window
import org.scalajs.dom.raw.IDBFactory
import org.scalajs.dom.raw.IDBDatabase

import scala.scalajs.js
import org.scalajs.dom.raw.IDBTransaction
import scala.scalajs.js.JSON
import org.scalajs.dom.raw.IDBOpenDBRequest

class IDB[F[_]](idb: IDBFactory)(implicit F: Concurrent[F]) {

}


object IDB {

  def open[F[_]]: IDB[F] = ???

  val versionStringRegex = raw"(\d+).(\d+).(\d+)(-SNAPSHOT)?".r  

  def determineVersion(versionString: String): Option[Int] = {
    versionString match {
      case versionStringRegex(major, minor, patch, isSnapshot) => 
        for {
          maj <- major.toIntOption
          min <- minor.toIntOption
          pat <- patch.toIntOption
          bool = isSnapshot != null
        } yield maj * 100000 + min * 1000 + pat * 10 + (if (bool) 1 else 0)
    }
  }
}