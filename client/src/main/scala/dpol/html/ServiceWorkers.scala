package dpol.html

import cats.effect._
import cats.implicits._
import scala.scalajs.js
import org.scalajs.dom.raw.Window
import org.scalajs.dom.raw.Navigator
import org.scalajs.dom.experimental.serviceworkers._
import org.scalajs.dom.raw.Event
import dpol.js.Events

class ServiceWorkers[F[_]: ContextShift](window: Window)(implicit F: ConcurrentEffect[F]) {

  def register(scriptUrl: String): F[Unit] = {

    val sw = F.delay(
        window.navigator.serviceWorker
      ).ensure(
        new Exception("Service workers are not supported on your browser")
      )(v => {
        println("Hek")
      !js.isUndefined(v)
      })
    
    for {
      serviceWorkers <- sw
      _ <- Events[F].once[Event](window, "load")
      _ <- Async.fromFuture(F.delay(
        serviceWorkers.register(scriptUrl).toFuture
      ))
    } yield ()
  }

}

object ServiceWorkers {
  def apply[F[_]: ConcurrentEffect: ContextShift](window: Window) = new ServiceWorkers[F](window)
}
