package dpol.html

import cats.effect.Sync
import cats.implicits._
import java.{util => ju}
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

class Storage[F[_]](storage: org.scalajs.dom.raw.Storage)(implicit F: Sync[F]) {

  /**
    * Can be failed due to parsing failure
    */
  def get[A: Decoder](key: String): F[A] =
    F.delay(storage.getItem(key)).flatMap(v => decode[A](v).fold(
      e => F.raiseError(e),
      F.pure
    ))

  def set[A: Encoder](key: String, value: A): F[Unit] = F.delay(
    storage.setItem(key, value.asJson.noSpaces)
  )

}
