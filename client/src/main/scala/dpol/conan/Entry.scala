package dpol.conan

import cats.effect._
import cats.implicits._
import dpol.html.{Storage => DStorage, _}
import org.scalajs.dom.raw._
import org.scalajs.dom._
import scala.concurrent.ExecutionContext
import cats.effect.concurrent.Deferred
import cats.data.EitherT

object Entry {

  def main(args: Array[String]): Unit = {
    run(Nil).unsafeRunAsync {
      case Left(value) =>
        println("Your program appears to have exploded")
        value.printStackTrace()
      case Right(value) => println("All is well and complete")
    }
  }

  def run(args: List[String]): IO[ExitCode] = {

    implicit val t  = IO.timer
    implicit val cs = IO.contextShift(ExecutionContext.Implicits.global)


    for {
      idb     <- IO(window.indexedDB)
      main    <- Dom[IO].getElementById[HTMLBodyElement]("main")
      // _       <- ServiceWorkers[IO](window).register("/service-worker.js")
      storage <- IO { new DStorage[IO](org.scalajs.dom.window.localStorage) }
      _ <- main.fold(
        e => IO(println(s"Error looking up HTML bits: $e")),
        m => new God[IO]().run(m)
      )
      _ <- IO(println("Program complete"))
    } yield ExitCode.Success
  }

}
