package dpol.conan.main

import cats.effect._
import cats.implicits._

import dpol.conan.Scene
import dpol.conan.settings.Settings
import dpol.conan.settings.SettingsScene

import fs2._

import java.util.concurrent.TimeUnit

import org.scalajs.dom.raw.Element

import scala.concurrent.duration._
import scalatags.JsDom._
import scalatags.JsDom.all._
import org.scalajs.dom.raw.HTMLCanvasElement

import dpol.webgl.WebGL
import dpol.html.Dom

class TimeLoopScene[F[_]](settings: Settings, settingsScene: => SettingsScene[F])(implicit F: Async[F], timer: Timer[F]) extends Scene[F] {

  val dom = Dom[F]

  override def run(parent: Element): F[Scene[F]] = 
    dom.appendWhileEvaluating(parent, canvas(
        `class` := "main-canvas"
      ))(potoot) >> F.delay(settingsScene)

  def potoot(canvas: HTMLCanvasElement): F[Unit] = 
    for {
      gl <- WebGL.initialiseWebGL2[F](canvas)
      _ <- timer.sleep(10.seconds)
    } yield ()

  def programClock[F[_]: Concurrent: Timer]: Pipe[F, Duration, FiniteDuration] =
    _.switchMap {
      case f: FiniteDuration =>
        Stream.repeatEval(Timer[F].clock.realTime(TimeUnit.NANOSECONDS)).metered(f)
      case _ =>
        Stream.empty
    }.zipWithPrevious
      .collect {
        case (Some(previous), next) => FiniteDuration(next - previous, TimeUnit.NANOSECONDS)
      }

}