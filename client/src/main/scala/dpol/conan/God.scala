package dpol.conan

import cats.effect._
import cats.implicits._

import dpol.conan.main.TimeLoopScene
import dpol.conan.settings.Settings
import dpol.conan.settings.SettingsScene
import dpol.html._

import org.scalajs.dom.raw.HTMLBodyElement
import org.scalajs.dom.raw.Element

import scalatags.JsDom._
import scalatags.JsDom.all._

import scala.concurrent.duration._

/**
  * Decides which scene to display and controls their switching. 
  */
class God[F[_]: Timer](implicit F: ConcurrentEffect[F]) {

  def run(main: HTMLBodyElement): F[Unit] = 
    for {
      loadingDiv <- F.delay(div(`class` := "loadingScreen", "Loading"))
      scene <- Dom[F].appendWhileEvaluating(main, loadingDiv)(_ => determineStartingScene)
      _ <- playScene(scene, main)
    } yield ()
  
  def playScene(scene: Scene[F], parent: Element): F[Unit] = 
    for {
      nextScene <- scene.run(parent)
      _ <- playScene(nextScene, parent)
    } yield ()
    
  
  def determineStartingScene: F[Scene[F]] = 
    F.pure(new SettingsScene(Settings(123, 321, 131, "Hek"), s => F.pure(CountingScene(1))))

}


case class CountingScene[F[_]: Timer](number: Int)(implicit F: ConcurrentEffect[F]) extends Scene[F] {
  def run(parent: Element): F[Scene[F]] = 
    for {
      diveroo <- F.delay(div(s"Current number: $number").render)
      next <- F.bracket(F.delay(parent.appendChild(diveroo)))(_ => Timer[F].sleep(3.seconds) >> F.delay(CountingScene(number + 1)))(_ => F.delay(parent.removeChild(diveroo)))
    } yield next
}

trait Scene[F[_]] {
  /**
    * Running a scene executes its effect and returns the next scene to execute
    *
    * @param parent the UI parent that can be used to display the UI
    */
  def run(parent: Element): F[Scene[F]]
}
