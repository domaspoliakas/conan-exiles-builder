package dpol.conan.settings

import cats.effect._
import cats.effect.concurrent.Deferred
import cats.implicits._

import dpol.conan._
import dpol.html._

import fs2._

import scalatags.JsDom._
import scalatags.JsDom.all._

import org.scalajs.dom
import org.scalajs.dom._
import org.scalajs.dom.raw.HTMLInputElement
import org.scalajs.dom.raw.HTMLDivElement

import scala.concurrent.duration._
import scala.reflect.ClassTag

import shapeless.labelled._
import shapeless._

trait SettingsFormGenerator[A] {
  type Output
  def generate(a: A): Output
}

object SettingsFormGenerator {

  def apply[A](implicit a: SettingsFormGenerator[A]) = a

  type Aux[A, O] = SettingsFormGenerator[A] { type Output = O }

  implicit val stringInputGenerator =
    new SettingsFormGenerator[String] {
      type Output = ConcreteHtmlTag[HTMLInputElement]
      def generate(a: String): Output = input(
        value := a
      )
    }

  implicit val intInputGenerator = new SettingsFormGenerator[Int] {
    type Output = ConcreteHtmlTag[HTMLInputElement]
    def generate(a: Int): Output = input(
      value := a,
      `type` := "number"
    )
  }

  implicit val hnilGen = new SettingsFormGenerator[HNil] {
    type Output = Seq[ConcreteHtmlTag[HTMLDivElement]]
    def generate(a: HNil): Seq[TypedTag[HTMLDivElement]] = Seq()
  }

  implicit def hlistGen[K <: Symbol, H, T <: HList](
      implicit headGen: Lazy[SettingsFormGenerator.Aux[H, _ <: ConcreteHtmlTag[HTMLInputElement]]],
      tailGen: SettingsFormGenerator.Aux[T, Seq[ConcreteHtmlTag[HTMLDivElement]]],
      witness: Witness.Aux[K]
  ): SettingsFormGenerator.Aux[FieldType[K, H] :: T, Seq[ConcreteHtmlTag[HTMLDivElement]]] = {
    new SettingsFormGenerator[FieldType[K, H] :: T] {
      type Output = Seq[ConcreteHtmlTag[HTMLDivElement]]
      override def generate(a: FieldType[K, H] :: T): Output = {

        val idValue = witness.value.name

        val inputerino: ConcreteHtmlTag[HTMLInputElement] = headGen.value.generate(a.head)(
          id := idValue
        )

        val headDiv = div(
          `class` := "settings-group",
          label(
            `for` := idValue,
            idValue
          ),
          inputerino
        )

        headDiv +: tailGen.generate(a.tail)
      }
    }
  }

  implicit def labelledGeneric[A, H](
      implicit generic: LabelledGeneric.Aux[A, H],
      hEncoder: Lazy[SettingsFormGenerator.Aux[H, Seq[ConcreteHtmlTag[HTMLDivElement]]]],
      typeTag: ClassTag[A]
  ) = new SettingsFormGenerator[A] {
    type Output = ConcreteHtmlTag[HTMLDivElement]
    def generate(a: A): Output = {
      div(
        `class` := "settings-screen",
        h1(
          `class` := "title",
          typeTag.runtimeClass.getSimpleName()
        )
      )(hEncoder.value.generate(generic.to(a)): _*)
    }
  }

}

case class Settings(fovInDegrees: Int, zNear: Int, zFar: Int, yourName: String) {
  lazy val fovInRads = Math.toRadians(fovInDegrees)
}

class SettingsScene[F[_]](settings: Settings, nextScene: Settings => F[Scene[F]])(
    implicit F: Concurrent[F],
    timer: Timer[F]
) extends Scene[F] {

  val dom = Dom[F]

  override def run(parent: Element): F[Scene[F]] = {

    dom.appendWhileEvaluating(
      parent,
      the[SettingsFormGenerator[Settings]].generate(Settings(123, 321, 131, "Hek"))
      // div(
      //   `class` := "settings-screen",
      //   h1(
      //     `class` := "title",
      //     "Settings"
      //   ),
      //   div(
      //     `class` := "settings-group",
      //     label(
      //       `for` := "one",
      //       "One"
      //     ),
      //     input(
      //       id := "one",
      //       `type` := "number"
      //     )
      //   ),
      //   div(
      //     `class` := "settings-group",
      //     label(
      //       `for` := "two",
      //       "Two"
      //     ),
      //     input(
      //       id := "two",
      //       `type` := "number"
      //     )
      //   ),
      //   div(
      //     `class` := "settings-actions",
      //     button("Defaults"),
      //     button("Save")
      //   )
      // )
    )(_ => potoot) >> nextScene(settings)
  }

  def potoot: F[Unit] = {
    F.never.void
  }

}
