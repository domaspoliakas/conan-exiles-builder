addSbtPlugin("org.scala-js"         % "sbt-scalajs"              % "1.0.1")
addSbtPlugin("com.vmunier"          % "sbt-web-scalajs"          % "1.0.11")
addSbtPlugin("org.portable-scala"   % "sbt-scalajs-crossproject" % "1.0.0")
addSbtPlugin("io.spray"             % "sbt-revolver"             % "0.9.1")
addSbtPlugin("com.eed3si9n"         % "sbt-buildinfo"            % "0.9.0")

