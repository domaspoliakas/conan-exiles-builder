# Conan Exiles Builder

This is a web application for planning bases for the video game Conan Exiles. It is written in [scala.js](https://www.scala-js.org/) using WebGL. I am writing this application in part because the game does not have the capability to allow one to plan your bases, but also because I wanted to learn WebGL and wanted to see if I can push my knowledge of scala to make an application based on functional programming principles in a space that is not usually functionally oriented (i.e. videogames and graphics programming). 



## Goals

The goals are to allow the following

- To lay out your base spatially (as one could in the game)
- To calculate the resources necessary for the layout
- To make the designs easily sharable between people

## Feature Suggestions

- Mobile version
- Stability calculations