package dpol.conan;

import cats.effect._
import cats.implicits._
import org.http4s.headers.`Content-Type`
import fs2._
import org.http4s._
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.dsl.Http4sDsl
import org.http4s.server.middleware.Logger
import org.http4s.server.staticcontent._

object Firestarter extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    server[IO].compile.drain.as(ExitCode.Success)
  }

  def server[F[_]: ConcurrentEffect](implicit T: Timer[F], C: ContextShift[F]): Stream[F, Nothing] = {
    for {
      blocker <- Stream.resource(Blocker[F])
      exitCode <- BlazeServerBuilder[F]
        .bindHttp(12345, "0.0.0.0")
        .withHttpApp(
          Logger.httpApp(true, true)(
            (
              potoot[F](blocker).orNotFound
            )
          )
        )
        .serve
    } yield exitCode
  }.drain

  def potoot[F[_]: Sync: ContextShift](blocker: Blocker): HttpRoutes[F] = {
    val dsl = new Http4sDsl[F] {}
    import dsl._

    val resource = resourceService[F](ResourceService.Config("/public", blocker))

    def fullOptOrFastOpt(base: String, request: Request[F]): F[Response[F]] = {
      StaticFile
        .fromResource(s"/public/$base-fullopt.js", blocker, Some(request), true)
        .orElse(StaticFile.fromResource(s"/public/$base-fastopt.js", blocker, Some(request), true))
        .getOrElse(Response(status = InternalServerError))
    }

    val static = HttpRoutes.of[F] {
      case GET -> Root =>
        Ok(
          Page("/carpenter-thrall.js", "Carpenter Thrall"),
          `Content-Type`.apply(MediaType.text.html)
        )

      case r @ GET -> Root / "carpenter-thrall.js" => fullOptOrFastOpt("carpenter-thrall", r)
      case r @ GET -> Root / "service-worker.js"   => fullOptOrFastOpt("service-worker", r)
    }

    static <+> resource

  }

}
