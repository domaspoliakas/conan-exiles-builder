package dpol.conan

import scalatags.Text.all._
import java.{util => ju}
import java.nio.charset.StandardCharsets

object Page {
  def apply(scriptLoc: String, title: String): String =
    "<!DOCTYPE html>" + html(
      head(
        scalatags.Text.tags2.title(title),
        meta(name := "viewport", content := "width=device-width, initial-scale=1.0"),
        link(rel := "manifest", href := "/manifest.webmanifest"),
        link(rel := "stylesheet", `type` := "text/css", href := "css/main.css")
      ),
      body(
        id := "main",
        script(src := s"$scriptLoc")
      )
    ).render
}
