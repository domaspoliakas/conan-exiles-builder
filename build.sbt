Global / onChangedBuildSource := ReloadOnSourceChanges

val monocleVersion = "2.0.4" // depends on cats 2.x
val catsVersion    = "2.1.1" // Except for cats-effect, which case to use a different version for sjs 1 reasons

lazy val commonSettings = Seq(
  scalaVersion := "2.13.2",
  version := "0.1.0-SNAPSHOT",
  organization := "org.dpol",
  organizationName := "Domas Poliakas",
  scalacOptions ++= Seq(
    // "-Xlint",
    "-Xfatal-warnings",
    "-feature",
    "-deprecation",
    "-unchecked",
    "-encoding",
    "UTF-8",
    "-language:higherKinds",
    "-language:postfixOps"
  ),
  addCompilerPlugin(
    "org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full
  ),
  addCompilerPlugin(
    "com.olegpy" %% "better-monadic-for" % "0.3.1"
  )
)

lazy val root = (project in file("."))
  .aggregate(client, `service-worker`, server, `js-shared`)

lazy val `js-shared` = (project in file("js-shared"))
  .settings(commonSettings)
  .settings(
    name := "js-shared",
    libraryDependencies ++= Seq(
      "org.scala-js"  %%% "scalajs-dom"   % "1.0.0",
      "org.typelevel" %%% "cats-core"     % catsVersion,
      "org.typelevel" %%% "cats-effect"   % "2.1.2",
      "co.fs2"        %%% "fs2-core"      % "2.3.0",
      "io.circe"      %%% "circe-core"    % "0.13.0",
      "io.circe"      %%% "circe-generic" % "0.13.0",
      "io.circe"      %%% "circe-parser"  % "0.13.0"
    )
  )
  .enablePlugins(ScalaJSPlugin)

lazy val client = (project in file("client"))
  .settings(commonSettings)
  .settings(
    scalacOptions += "-Ymacro-annotations",
    name := "Carpenter Thrall",
    scalaJSUseMainModuleInitializer := true,
    libraryDependencies ++= Seq(
      "com.lihaoyi"                %%% "scalatags"                 % "0.8.6",
      "org.typelevel"              %%% "cats-laws"                 % catsVersion % Test,
      "com.github.julien-truffaut" %%% "monocle-core"              % monocleVersion,
      "com.github.julien-truffaut" %%% "monocle-macro"             % monocleVersion,
      "com.github.julien-truffaut" %%% "monocle-law"               % monocleVersion % "test",
      "com.chuusai"                %%% "shapeless"                 % "2.3.3",
      "org.scalacheck"             %%% "scalacheck"                % "1.14.3" % Test,
      "com.github.alexarchambault" %%% "scalacheck-shapeless_1.14" % "1.2.5" % Test
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full)
  )
  .enablePlugins(
    ScalaJSPlugin,
    ScalaJSWeb,
    BuildInfoPlugin
  )
  .dependsOn(`js-shared`)

lazy val `service-worker` =
  (project in file("service-worker"))
    .settings(commonSettings)
    .settings(
      name := "service-worker",
      scalaJSUseMainModuleInitializer := true,
      libraryDependencies ++= Seq(
        "org.scala-js" %%% "scalajs-dom" % "1.0.0"
      )
    )
    .enablePlugins(
      ScalaJSPlugin,
      ScalaJSWeb
    )
    .dependsOn(`js-shared`)

val Http4sVersion = "0.21.1"

lazy val server = (project in file("server"))
  .settings(commonSettings)
  .settings(
    name := "server",
    scalaJSProjects := Seq(
      client,
      `service-worker`
    ),
    devCommands in scalaJSPipeline += "~reStart",
    pipelineStages in Assets := Seq(scalaJSPipeline),
    // triggers scalaJSPipeline when using compile or continuous compilation
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,
    WebKeys.packagePrefix in Assets := "public/",
    managedClasspath in Runtime += (packageBin in Assets).value,
    libraryDependencies ++= Seq(
      "org.http4s"  %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s"  %% "http4s-dsl"          % Http4sVersion,
      "com.lihaoyi" %%% "scalatags"          % "0.8.6"
    ),
    buildInfoKeys ++= Seq[BuildInfoKey](
      BuildInfoKey.map(isDevMode in scalaJSPipeline) { case (k, v) => "isDevMode" -> v }
    )
  )
  .enablePlugins(SbtWeb, BuildInfoPlugin)
