package dpol

import cats.effect._
import cats.implicits._

import org.scalajs.dom.experimental.serviceworkers._
import org.scalajs.dom.experimental.Request

import scala.concurrent.ExecutionContext

import scala.scalajs.js.JSConverters._
import scala.scalajs.js
import scala.scalajs.js.|
import org.scalajs.dom.experimental.Response
import org.scalajs.dom.experimental.Fetch
import org.scalajs.dom.experimental.ResponseType
import scala.scalajs.js.JSON

object Entry {

  val cacheName = "CARPENTER-THRALL"
  val urlsToCache = List(
    "/",
    "/css/main.css",
    "/carpenter-thrall.js"
  )

  def main(args: Array[String]): Unit = {
    run(Nil).unsafeRunAsync {
      case Left(value) =>
        println("Your program appears to have exploded")
        value.printStackTrace()
        throw value
      case Right(value) =>
        println("All is well and complete")
    }
  }

  def run(args: List[String]): IO[ExitCode] = {

    implicit val t  = IO.timer
    implicit val ec = ExecutionContext.Implicits.global
    implicit val cs = IO.contextShift(ExecutionContext.Implicits.global)

    for {
      globalScope <- IO(ServiceWorkerGlobalScope.self)
      _ <- IO {
        globalScope.addEventListener("install", (event: ExtendableEvent) => {
          event.waitUntil(
            install[IO](globalScope).unsafeToFuture().toJSPromise
          )
        })
      }
      _ <- IO {
        globalScope.addEventListener("fetch", (event: FetchEvent) => {
          event.respondWith(
            fetch(globalScope, event).unsafeToFuture().toJSPromise
          )
        })
      }
    } yield ExitCode.Success
  }

  def install[F[_]: ContextShift](self: ServiceWorkerGlobalScope)(implicit F: Async[F]): F[Unit] = {
    for {
      cache <- Async.fromFuture(F.delay(self.caches.open(cacheName).toFuture))
      _ <- Async.fromFuture(
        F.delay(
          cache.addAll(urlsToCache.toJSArray.asInstanceOf[js.Array[String | Request]]).toFuture
        )
      )
      _ <- F.delay(
        println("Caching complete")
      )
    } yield ()
  }

  case class FetchError(message: String, originalResponse: Response) extends Exception

  def fetch[F[_]: ContextShift: Timer](self: ServiceWorkerGlobalScope, request: FetchEvent)(
      implicit F: Async[F]
  ): F[Response] = {

    implicit val ec = ExecutionContext.Implicits.global

    def isSuccess(r: Response): Boolean =
      !js.isUndefined(r) && r.status == 200 && r.`type` == ResponseType.basic

    def fetchAndCache(request: Request): F[Response] =
      Async
        .fromFuture(F.delay(Fetch.fetch(request).toFuture))
        .ensureOr(r => FetchError(s"Failed to fetch; response status was ${r.status}", r))(
          isSuccess
        )
        .flatMap(r => {
          for {
            _              <- F.delay(println(s"Successfully fetched ${request.url}; gonna cache"))
            clonedResponse <- F.delay(r.clone())
            cache <- Async.fromFuture(
              F.delay(
                self.caches.open(cacheName).toFuture
              )
            )
            _ <- F.delay(cache.put(request, clonedResponse))
          } yield r
        })

    def tryToFetchFromCache[A](fallback: F[Response]): F[Response] =
      Async
        .fromFuture(
          F.delay(
            self.caches.`match`(request.request).asInstanceOf[js.Promise[Response]].toFuture
          )
        )
        .redeemWith(
          _ =>
            F.delay(println("Failed to get from cache; completing with original response")) >> fallback,
          r =>
            if (!isSuccess(r)) {
              F.delay(
                println("Failed to get from cache; completing with original response")
              ) >> fallback
            } else {
              F.delay(println("Found in cache; completing")) >> F.pure(r)
            }
        )

    F.delay(println(s"Trying to fetch: ${request.request.url}")) *>
      fetchAndCache(request.request).recoverWith {
        case FetchError(message, originalResponse) =>
          F.delay(println(s"Failed to get resource due to '$message'; trying the cache")) *>
            tryToFetchFromCache(F.pure(originalResponse))
        case other =>
          F.delay(println("Failed to get resource; trying the cache")) *>
              tryToFetchFromCache(F.raiseError(other))
      }
  }

}
